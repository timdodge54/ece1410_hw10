#!/usr/bin/env python3

"""Script for compiling and running all student submitted c and c++ files.

Command line script marked as an executable so run with the following command:
    ./grading_script <folder> <input_path> 

Args:
    folder (str): Relative path to folder of cpp files to compile and run
    input_path (str): Relative Path of the input files (in the form Input***.txt)

"""
print("GRADING SCRIPT\n\n")
comp_scores = {}
run_scores = {}
correct_scores = {}

import argparse
import os
import subprocess
from pathlib import Path
import traceback
import shutil
import cv2

# Create the argument parser
parser = argparse.ArgumentParser(
    prog="grading_script",
    description="""Script for compiling 
    and running all student submitted c and c++ files.""",
    epilog="Author: Timothy Dodge",
)

# add positional command line arguments
parser.add_argument(
    "folder", help="Relative path to folder of cpp files to compile and run"
)
parser.add_argument(
    "output_filename", help="output filename"
)


# parse args and assign to variables
args = parser.parse_args()
folder = args.folder
output_filename = args.output_filename
cwd = os.getcwd()
path_to_main = f"{cwd}/main.cpp"




# Get the student files
source_student_files = Path(folder)
print(f"STUDENT FILES = {source_student_files}")
student_files = [x.name for x in source_student_files.iterdir()]
student_files.sort()
for file in student_files:
    print(file)


# Loop through the student files.
keys = []
try:
    for i, key in enumerate(student_files):
        print(
            f"|-------------------------------------------------------"
            "-------------------------------|"
        )
        student_folder = os.path.join(folder, key)
        print(os.getcwd())
        os.chdir(student_folder)

        # Compile the student file.
        print(f"\nFOLDERNAME= {student_folder}\n")
        curr_folder = os.getcwd()
        shutil.copyfile(path_to_main, curr_folder + "/main.cpp")

        if os.path.exists("test.exe"):
            os.remove("test.exe")
        curr_folder = os.getcwd()
        cpp_names = [x.name for x in Path(".").iterdir() if x.name.endswith(".cpp") or x.name.endswith(".h")]
        images = [x.name for x in Path(".").iterdir() if x.name.endswith(".pdf")]
        print("Doxegyn Contents ------------")
        comment_count = 0
        doxygen_count = 0
        for cpp_name in cpp_names:
            with open(cpp_name, "r") as f:
                doxygen = False
                for line in f.readlines():
                    if doxygen:
                        print(line)
                    line_split = list(line)
                    if len(line_split) < 2:
                        continue
                    else:
                        for i, char_ in enumerate(line_split):
                            if i != len(line_split) - 1:
                                if char_ == "/" and line_split[i+1] == "/":
                                    comment_count += 1 
                                if char_ == "*" and line_split[i+1] == "/" and doxygen:
                                    doxygen = False
                                if char_ == "/" and line_split[i+1] == "*":
                                    doxygen = True
                                    doxygen_count += 1
                                    print(f"Doxygen Header # {doxygen_count}")
                                    print(line)

                            if line_split[-2] == "/":
                                break
        if doxygen_count == 0:
            print("No doxygen comments found.")
        print(f"COMMENT COUNT: {comment_count}")
        print(f"DOXYGEN COUNT: {doxygen_count}")

        output_comp = subprocess.run(
            [
                "cl", "*.cpp",
            ],
            capture_output=True,
            shell=True,
        )
        print("COMPILATION OUTPUT ----------------------------\n")
        compilation_sucess = output_comp.returncode == 0
        if output_comp.returncode != 0:
            print(f"COMPILATION ov {student_folder} FAILED FOR {key}\n")
            print(output_comp.stderr.decode())
            print(output_comp.stdout.decode())
        else:
            print(f"COMPILATION SUCCESSFUL FOR {key}\n")
        cwd = os.getcwd()
        list_of_files = os.listdir(cwd)
        for files in list_of_files:
            if (
                files.endswith(".exe")
            ):
                print(f"executable found:") 
                run_file = files
        if not run_file:
            print("No executable file found.")
            raise
        print(f"Current Directory: {os.getcwd()}")
        print(f"RUN COMMAND: .\test.exe")
        # Print the stdout and stderr of the student file.
        try:

            output = subprocess.run(
                [
                    run_file,
                ],
                shell=True,
                capture_output=True,
                timeout=30
            )
            print("RUN OUTPUT ----------------------------\n")
            print(output.stdout.decode())
            print("Standard Error ------------------------\n")
            print(output.stderr.decode())
        except subprocess.TimeoutExpired:
            print("TIMEOUT ERROR !!!!!")
        for image in images:
           subprocess.Popen([image], shell=True)

        os.chdir("../..")
        print(os.getcwd())
        keys.append(key)
        print(f"Current Student: {student_folder}")
        comp_scores[key] = input("Compilation without errors: ")
        run_scores[key] = input("Program ran without errors: ")
        correct_scores[key] = input("Program output correct: ")
        move = input("Move folder to completed? (N)")
        if move == "":
            shutil.move(student_folder, "completed/")
        print(os.getcwd())

        # Input score for the student file.
        # print(f"Dict key: {key}")
        
        # Remove the executable file.
    print(
        f"|-------------------------------------------------------"
        "-------------------------------|\n\n"
    )
except KeyboardInterrupt:
    print("Keyboard Interrupt")
except Exception as e:
    print(e)
    print(traceback.format_exc())
    print("Error in grading script.")

print(
    f"||||||||||||||||||||||||||||||||||||||||||||||||||"
    "||||||||||||||||||||||||||||||||||||||"
)

cwd = os.getcwd()


input("Press Enter to exit...")
