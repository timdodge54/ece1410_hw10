#pragma once
#include <iostream>
using namespace std;

template <class T>
class Bubble
{
public:
	Bubble(T* array, int N);
	void print(T* array, int N);
};

/****************************************************
*	@name		Bubble
*	@brief		sort the array via bubble sorting method
*	@param		array to be sorted, size of array
*	@retval		none
****************************************************/
template <class T>
Bubble<T>::Bubble(T* array, int N)
{
	int i, j;
	T temp;
	int swapped = true;
	while (swapped)
	{
		swapped = false;
		i = 0;
		j = 1;
		while (j < N)
		{
			if (array[i] > array[j])	//swap if nums are out of order
			{
				temp = array[i];
				array[i] = array[j];
				array[j] = temp;
				swapped = true;		//loop until we don't swap
			}
			i++;
			j++;
		}
		N--;
	}
}

/****************************************************
*	@name		print
*	@brief		print the array of numbers
*	@param		array to be printed, size of array
*	@retval		none
****************************************************/
template <class T>
void Bubble<T>::print(T* array, int N)
{
	int i;
	for (i = 0; i < N; i++)
	{
		cout << array[i] << " ";
	}
}