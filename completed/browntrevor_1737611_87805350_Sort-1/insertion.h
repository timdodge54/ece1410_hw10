#pragma once
#include <iostream>
using namespace std;

template <class T>
class Insertion
{
public:
	Insertion(T* array, int N);
	void print(T* array, int N);
};

/****************************************************
*	@name		Insertion
*	@brief		sort the array via Insertion sorting method
*	@param		array to be sorted, size of array
*	@retval		none
****************************************************/
template <class T>
Insertion<T>::Insertion(T* array, int N)
{
	int i, j, k, p;
	i = 1;
	T temp;
	while (i < N)
	{
		j = i;
		k = j - 1;
		while (j > 0 && array[k] > array[j])
		{		//move new element to its place in what has been sorted so far
			temp = array[j];
			array[j] = array[k];
			array[k] = temp;
			j--;
			k--;
		}
		i++;
	}


}

/****************************************************
*	@name		print
*	@brief		print the array of numbers
*	@param		array to be printed, size of array
*	@retval		none
****************************************************/
template <class T>
void Insertion<T>::print(T* array, int N)
{
	int i;
	for (i = 0; i < N; i++)
	{
		cout << array[i] << " ";
	}
}