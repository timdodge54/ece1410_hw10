#pragma once
#include <iostream>
using namespace std;

template <class T>
class Selection
{
public:
	Selection(T* array, int N);
	void print(T* array, int N);
};

/****************************************************
*	@name		Selection
*	@brief		sort the array via Selection sorting method
*	@param		array to be sorted, size of array
*	@retval		none
****************************************************/
template <class T>
Selection<T>::Selection(T* array, int N)
{
	int i, j, iMin, start;
	T temp;
	start = 0;
	
	for (j = start; j < N - 1; j++)
	{
		iMin = j;	//pick a value as min
		for (i = j + 1; i < N; i++)
		{
			if (array[i] < array[iMin])		//if find new min, replace
			{
				iMin = i;
			}
		}
		
		if (iMin != j)
		{		//move the min to next spot in array
			temp = array[j];
			array[j] = array[iMin];
			array[iMin] = temp;
		}
		start++;		//move the start forward to reduce runtime
	}
}

/****************************************************
*	@name		print
*	@brief		print the array of numbers
*	@param		array to be printed, size of array
*	@retval		none
****************************************************/
template <class T>
void Selection<T>::print(T* array, int N)
{
	int i;
	for (i = 0; i < N; i++)
	{
		cout << array[i] << " ";
	}
}