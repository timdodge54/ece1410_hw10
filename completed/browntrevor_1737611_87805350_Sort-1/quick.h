#pragma once
#include <iostream>
using namespace std;

template <class T>
class Quick
{
private:
	void quicksort(T* array, int start, int end);
	int partition(T* array, int start, int end);
public:
	Quick(T* array, int N);
	void print(T* array, int N);
};

/****************************************************
*	@name		Quick
*	@brief		call the quicksort function
*	@param		array to be sorted, size of array
*	@retval		none
****************************************************/
template <class T>
Quick<T>::Quick(T* array, int N)
{
	quicksort(array, 0, N - 1);
}

/****************************************************
*	@name		quicksort
*	@brief		create parittions to recursively sort the entire array
*	@param		array to be sorted, start segment, end segment
*	@retval		none
****************************************************/
template<class T>
void Quick<T>::quicksort(T* array, int start, int end)
{
	int p;
	if (start < end)
	{
		p = partition(array, start, end);	//make a split to sort
		quicksort(array, start, p - 1);		//start sorting from the left
											//and pass in new bounds
		quicksort(array, p + 1, end);		//continue to the right
											//and pass in new bounds
	}
}

/****************************************************
*	@name		paritition
*	@brief		arrange the number on the correct side of the parition
*	@param		array, start segment of array, end segment of array
*	@retval		location of partition
****************************************************/
template<class T>
int Quick<T>::partition(T* array, int start, int end)
{
	int pivot, i, j;
	T temp;
	pivot = array[end];	//pick "random" number to become the pivot
	i = start - 1;
	for (j = start; j < end; j++)
	{
		if (array[j] < pivot)
		{		//if the number is less then the pivot, move it to the right
			i++;
			temp = array[j];
			array[j] = array[i];
			array[i] = temp;
		}
	}
	//swap pivot with the next num in sequence to position it correct
	i++;
	temp = array[j];
	array[j] = array[i];
	array[i] = temp;
	return i;
	
	
}

/****************************************************
*	@name		print
*	@brief		print the array of numbers
*	@param		array to be printed, size of array
*	@retval		none
****************************************************/
template <class T>
void Quick<T>::print(T* array, int N)
{
	int i;
	for (i = 0; i < N; i++)
	{
		cout << array[i] << " ";
	}
}