#pragma once
#include <algorithm>
#include <iostream>

using namespace std;

template <class T>
class Selection {
public:
	Selection(T* list, int n){
		for (int i = 0; i < n - 1; i++) {
			int iMin = i;
			for (int j = i + 1; j < n; j++) {
				if (list[j] < list[iMin]) {
					iMin = j;
				}
			}
			if (iMin != i) {
				swap(list[i], list[iMin]);
			}
		}
	}

	void print(T* list, int n) {
		for (int i = 0; i < n; i++) {
			cout << list[i] << " ";
		}
	}
};