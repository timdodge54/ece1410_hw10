#pragma once
#include <algorithm>
#include <iostream>

using namespace std;

template <class T>
class Bubble {
public:
	Bubble(T* list, int n) {
		int swapped = 1;
		while (swapped != 0) {
			swapped = 0;
			for (int i = 1; i < n; i++) {
				if (list[i - 1] > list[i]) {
					swap(list[i - 1], list[i]);
					swapped = 1;
				}
			}
		}
	}
	void print(T* list, int n) {
		for (int i = 0; i < n; i++) {
			cout << list[i] << " ";
		}
	}
};
