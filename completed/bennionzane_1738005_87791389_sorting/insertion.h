#pragma once
#include <algorithm>
#include <iostream>

using namespace std;

template <class T>
class Insertion {
public:
	Insertion(T* list, int n){
		int i = 1;
		while (i < n) {
			int j = i;
			while (j > 0 && list[j - 1] > list[j]) {
				swap(list[j], list[j - 1]);
				j = j - 1;
			}
			i++;
		}
	}

	void print(T* list, int n) {
		for (int i = 0; i < n; i++) {
			cout << list[i] << " ";
		}
	}
};