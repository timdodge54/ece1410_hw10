#pragma once
#include <algorithm>
#include <iostream>

using namespace std;

template <class T>
class Quick {
public:
	Quick(T* list, int n){
		quicksort(list, 0, n-1);
	}

	void print(T* list, int n) {
		for (int i = 0; i < n; i++) {
			cout << list[i] << " ";
		}
	}

private:
	void quicksort(T* list, int lo, int hi) {
		if (lo < hi) {
			int p = partition(list, lo, hi);
			quicksort(list, lo, p - 1);
			quicksort(list, p + 1, hi);
		}
	}

	int partition(T* list, int lo, int hi) {
		int pivot = list[hi];
		int i = lo - 1;
		//maybe hi - 1
		for (int j = lo; j < hi; j++) {
			if (list[j] < pivot) {
				i++;
				swap(list[i], list[j]);
			}
		}
		swap(list[i + 1], list[hi]);
		return i + 1;
	}
};