#ifndef _SELECTION_
#define _SELECTION_

#include <iostream>
#include <algorithm>

template <class T>
class Selection {
public:
	Selection (T* arr, int size)
	{
		int i, j, iMin;
		
		for (j = 0; j < size - 1; j++)
		{
			iMin = j;
			for (i = j + 1; i < size; i++)
			{
				if (arr[i] < arr[iMin])
				{
					iMin = i;
				}
			}	
			if (iMin != j)
			{
				std::swap(arr[j], arr[iMin]);
			}
		}
	}
	
	void print (T* arr, int size)
	{
		for (int i = 0; i < size; i++)
		{
			std::cout << arr[i] << " ";
		}
		std::cout << std::endl << "Selection Sort" << std::endl;
	}
};


#endif