#ifndef _BUBBLE_
#define _BUBBLE_

#include <iostream>
#include <algorithm>

template <class T>
class Bubble {
public:
	Bubble (T* arr, int size)
	{
		bool swapped = true;
		while(swapped) {
			swapped = false;
			for (int i = 1; i < size; i++) {
				if (arr[i-1] > arr[i]) {
					std::swap(arr[i-1], arr[i]);
					swapped = true;
				}
			}
			size--;
		}
	}
	
	void print (T* arr, int size)
	{
		for (int i = 0; i < size; i++)
		{
			std::cout << arr[i] << " ";
		}
		std::cout << std::endl << "Bubble sort" << std::endl;
	}
};

#endif