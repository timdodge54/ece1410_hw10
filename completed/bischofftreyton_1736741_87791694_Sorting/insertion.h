#ifndef _INSERTION_
#define _INSERTION_

#include <iostream>
#include <algorithm>

template <class T>
class Insertion {
public:
	Insertion (T* arr, int size)
	{
		for (int i = 1; i < size; i++)
		{
			for (int j = i; j > 0 && arr[j-1] > arr[j]; j--)
			{
				std::swap(arr[j], arr[j-1]);
			}
		}
	}
	
	void print (T* arr, int size)
	{
		for (int i = 0; i < size; i++)
		{
			std::cout << arr[i] << " ";
		}
		std::cout << std::endl << "Instertion Sort" << std::endl;
	}
};


#endif