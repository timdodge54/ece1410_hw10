#ifndef _QUICK_
#define _QUICK_

#include <iostream>
#include <algorithm>

template <class T>
class Quick {
public:
	Quick (T* arr, int size)
	{
		int low = 0, high = size - 1;
		
		quicksort(arr, low, high);
	}
	
	void quicksort(T* arr, int lo, int high)
	{
		if (lo < high)
		{
			T p = partition(arr, lo, high);
			quicksort(arr, lo, p - 1);
			quicksort(arr, p + 1, high);
		}
	}
	
	int partition (T* arr, int lo, int high)
	{
		T pivot = arr[high];
		int i = lo - 1;
		int j = lo;
		while (j <= high - 1)
		{
			if (arr[j] < pivot)
			{
				i++;
				std::swap(arr[i], arr[j]);
			}
			j++;
		}
		std::swap(arr[i+1], arr[high]);
		return i + 1;
	}
	
	void print (T* arr, int size)
	{
		for (int i = 0; i < size; i++)
		{
			std::cout << arr[i] << " ";
		}
		std::cout << std::endl << "Quick Sort" << std::endl;
	}
};

#endif
