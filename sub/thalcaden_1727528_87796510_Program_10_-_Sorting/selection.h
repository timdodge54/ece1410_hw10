#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>
using namespace std;

template <class T>
class Selection {
public:

/******************************************************************************
* @name Selection
* @brief sorts the numbers in the array using the selection sort algorithm
* @param none
* @retval none
******************************************************************************/

    Selection(T *arr, int n) 
	{
		//create variables
        int i, j, min_idx;
		//for every element in the array
        for (i = 0; i < n-1; i++) 
		{
			//set min_idx to i
            min_idx = i;
			//for every element in the array
            for (j = i+1; j < n; j++)
			{
				//if current element is less than min_idx
                if (arr[j] < arr[min_idx]) 
				{
					//set min_idx to j
                    min_idx = j;
                }
            }
			//create a temporary variable
            T temp = arr[min_idx];
			//set min_idx to i
            arr[min_idx] = arr[i];
			//set i to temp
            arr[i] = temp;
        }
    }
	
/******************************************************************************
* @name print
* @brief prints the numbers in the array to the screen
* @param none
* @retval none
******************************************************************************/

    void print(T *arr, int n) {
		//for every element in the array
        for (int i = 0; i < n; i++) 
		{
			//print current number and then a space
            cout << arr[i] << " ";
        }
        cout << endl;
    }
};

#endif