#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>
using namespace std;

template<class T>
class Bubble {
public:

/******************************************************************************
* @name Bubble
* @brief sorts the numbers in the array using the bubble sort algorithm
* @param none
* @retval none
******************************************************************************/

    Bubble(T *arr, int n) {
		//for every element in the array
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
				//if j is greater than j + 1
                if (arr[j] > arr[j+1]) 
				{
					//create a temporary variable
                    T temp = arr[j];
					//set j equal to j + 1
                    arr[j] = arr[j+1];
					//set j + 1 equal to temp
                    arr[j+1] = temp;
                }
            }
        }
    }
	
/******************************************************************************
* @name print
* @brief prints the numbers in the array to the screen
* @param none
* @retval none
******************************************************************************/

    void print(T *arr, int n) {
		//for every element in the array
        for (int i = 0; i < n; i++) 
		{
			//print current number and then a space
            cout << arr[i] << " ";
        }
        cout << endl;
    }
};

#endif