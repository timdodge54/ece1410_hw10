#ifndef INSERTION_H
#define INSERTION_H
#include <iostream>
using namespace std;

template <class T>
class Insertion {
public:

/******************************************************************************
* @name Insertion
* @brief sorts the numbers in the array using the insertion sort algorithm
* @param none
* @retval none
******************************************************************************/

    Insertion(T *arr, int n) {
		//create variables
        int i, j;
        T key;
		//for every element in the array
        for (i = 1; i < n; i++) 
		{
			//set key equal to current element
            key = arr[i];
			//set j to i - 1;
            j = i - 1;
			//while j is less than or equal to 0
			//and while current element is less than key
            while (j >= 0 && arr[j] > key) 
			{
				//set the j + 1 equal to j
                arr[j+1] = arr[j];
				//set j equal to j - 1;
                j = j - 1;
            }
			//set j + 1 equal to key
            arr[j+1] = key;
        }
    }
	
/******************************************************************************
* @name print
* @brief prints the numbers in the array to the screen
* @param none
* @retval none
******************************************************************************/

    void print(T *arr, int n) {
		//for every element in the array
        for (int i = 0; i < n; i++) 
		{
			//print current number and then a space
            cout << arr[i] << " ";
        }
        cout << endl;
    }
};


#endif