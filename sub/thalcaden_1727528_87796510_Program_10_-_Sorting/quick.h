#ifndef QUICK_H
#define QUICK_H
#include <iostream>
using namespace std;

template <class T>
class Quick {
public:

/******************************************************************************
* @name Quick
* @brief sorts the numbers in the array using the quick sort algorithm
* @param none
* @retval none
******************************************************************************/

    Quick(T *arr, int n) {
        quicksort(arr, 0, n-1);
    }
	
/******************************************************************************
* @name print
* @brief prints the numbers in the array to the screen
* @param none
* @retval none
******************************************************************************/

    void print(T *arr, int n) {
		//for every element in the array
        for (int i = 0; i < n; i++) 
		{
			//print current number and then a space
            cout << arr[i] << " ";
        }
        cout << endl;
    }
private:

/******************************************************************************
* @name quicksort
* @brief sorts the numbers in the array
* @param none
* @retval none
******************************************************************************/

    void quicksort(T *arr, int low, int high) {
		//if low is less than high
        if (low < high) 
		{
			//create a new partition element 
            int pi = partition(arr, low, high);
			//quicksort upper half
            quicksort(arr, low, pi-1);
			//quicksort lower half
            quicksort(arr, pi+1, high);
        }
    }
	
	/******************************************************************************
* @name partition
* @brief partitions the elements in the array that have not been sorted
* @param none
* @retval none
******************************************************************************/

    int partition(T *arr, int low, int high) {
		//set the pivot to be the highest element in the array
        T pivot = arr[high];
		//set i equal to low - 1
        int i = low - 1;
        for (int j = low; j <= high-1; j++) 
		{
			//if current element is less than the pivot
            if (arr[j] < pivot) 
			{
				//increment i
                i++;
				//sqap the current element with the other element
                swap(arr[i], arr[j]);
            }
        }
		//swap the next element with the high element in the array
        swap(arr[i+1], arr[high]);
		//return i + 1
        return (i+1);
    }
	
	/******************************************************************************
* @name swap
* @brief swaps 2 given T's
* @param none
* @retval none
******************************************************************************/

    void swap(T& a, T& b) {
		//set temp to a
        T temp = a;
		//set a to b
        a = b;
		//set b to temp
        b = temp;
    }
};

#endif