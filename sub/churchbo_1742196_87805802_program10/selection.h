/*************************************************************************************************************************************
* Class: Selection
*
* Summary: template class that will apply the selection algorithm to sort an array passed into the constructor then output
*          the sorted data with the print function.
**************************************************************************************************************************************/


#pragma once
#include <iostream>
#include <iomanip>
#include <algorithm>

using namespace std;

template <class T>
class Selection {
public:
	Selection(T* array, int n)
	{
		for (int j = 0; j < n - 1; j++) {
			int iMin = j;
			for (int i = j + 1; i < n; i++) {
				if (array[i] < array[iMin]) {
					iMin = i;
				}
			}
			if (iMin != j) {
				swap(array[j], array[iMin]);
			}
		}
	}

	void print(T* a, int n)
	{
		for (int i = 0; i < n; i++)
		{
			cout << " " << setw(3) << a[i];
		}
	}
};