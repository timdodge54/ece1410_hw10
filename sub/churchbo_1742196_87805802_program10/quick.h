/***********************************************************************************************************************************
* Class: Quick
*
* Summary: Class template Quick applies the Quick sorting algorithm to sort an array passed into the constructor and outputs
*          the sorted data to the user with a print function.
************************************************************************************************************************************/
#pragma once
#include <iostream>
#include <iomanip>
#include <algorithm>

using namespace std;

template <class T>
class Quick
{
public:
	Quick(T* A, int n)
	{
		int lo = 0;
		int hi = n - 1;

		quicksort(A, lo, hi);
	};

	void print(T* a, int n)
	{
		for (int i = 0; i < n; i++)
		{
			cout << " " << setw(3) << a[i];
		};
	};
private:
	void quicksort(T* A, int lo, int hi) {
			if (lo < hi) {
				int pivot_index = partition(A, lo,  hi);
				quicksort(A, lo, pivot_index - 1);
				quicksort(A, pivot_index + 1, hi);
			}
		}

		int partition(T* A, int lo, int hi) {
			T pivot = A[hi];
			int i = lo - 1;
			for (int j = lo; j < hi; j++) {
				if (A[j] <= pivot) {
					i++;
					swap(A[i], A[j]);
				}
			}
			swap(A[i + 1], A[hi]);
			return i + 1;
		}
	};
