/**************************************************************************************************************************************
* Class: Insertion
*
* Summary: Class template Insertion applies the Insertion algorithm to sort an array passed into the constructor then prints
*          the data to the user using a print function
****************************************************************************************************************************************/


#pragma once
#include <iostream>
#include <iomanip>
#include <algorithm>

using namespace std;

template <class T>
class Insertion {
public:
	Insertion(T* Myarray, int n)
	{
		int i = 1;
			while (i < n)
			{
				int j = i;
					while ((j > 0) && (Myarray[j - 1] > Myarray[j]))
					{
						swap(Myarray[j], Myarray[j - 1]);
						j = j - 1;
					}
					i = i + 1;
			}

	}

	void print(T* a, int n)
	{
		for (int i = 0; i < n; i++)
		{
			cout << " " << setw(3) << a[i];


		}

	}
};
