/**************************************************************************************************************************************
* Class: Bubble
*
* Summary: Class template bubble applies the bubble sort algorithm to sort an array that is passed into the constructor
*          then prints the sorted data with a print function.
***************************************************************************************************************************************/

#pragma once
#include <iostream>
#include <algorithm>
#include <iomanip>


using namespace std;

template <class T> class Bubble {
public:

	Bubble(T* array, int n)
	{

		for (T i = 0; i < n ; i++)
		{
			for (T j = i + 1; j < n ; j++)
			{
				if (array[j] < array[i])
					swap(array[j], array[i]);
			}
		}
	
			

	}

	void print(T* a, int n)
	{
		for (T i = 0; i < n; i++)
		{
			cout << " " << setw(3) << a[i] ;
		}
	}
};