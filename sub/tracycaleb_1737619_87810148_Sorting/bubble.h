#ifndef BUBBLE_H
#define BUBBLE_H

#include <iostream>

template <class T>
class Bubble {
public:
	Bubble(T * arr, int size);
	void print(T * arr, int size);
};


/**
@name		Bubble
@brief		sorts array with bubble method
@param		array head and size
@retval		none
*/
template <class T>
Bubble<T>::Bubble(T * arr, int size) {
	
	T temp;
	
	// each loop the next largest term is moved to top of array
	for (int i = 0; i < size -1; i++) {
		for (int j = 0; j < size - i - 1; j++) {
			
			// swapping terms if right adjacent is larger
			if (arr[j + 1] < arr[j]) {
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}


/**
@name		print
@brief		prints the array
@param		array head and size
@retval		none
*/
template <class T>
void Bubble<T>::print(T * arr, int size) {
	for (int i = 0; i < size; i++) {
		std::cout << arr[i] << ' ';
	}
}

#endif