#ifndef SELECTION_H
#define SELECTION_H

#include <iostream>

template <class T>
class Selection {
public:
	Selection(T * arr, int size);
	void print(T * arr, int sizs);
};


/**
@name		Selection
@brief		sorts the array using selection sort
@param		array head and size
@retval		none
*/
template <class T>
Selection<T>::Selection(T * arr, int size) {
	
	int minterm, j;
	T temp;
	
	// finds the smallest value term
	for (int i = 0; i < size; i++) {
		minterm = i;
		for (j = i + 1; j < size; j ++) {
			if (arr[minterm] > arr[j])
				minterm = j;
		}
		
		// moves minterm to the bottom of array
		if (minterm != i) {
			temp = arr[i];
			arr[i] = arr[minterm];
			arr[minterm] = temp;
		}
	}
}


/**
@name		print
@brief		prints the array
@param		array head and size
@retval		none
*/
template <class T>
void Selection<T>::print(T * arr, int size) {
	for (int i = 0; i < size; i++) {
		std::cout << arr[i] << ' ';
	}
}

#endif