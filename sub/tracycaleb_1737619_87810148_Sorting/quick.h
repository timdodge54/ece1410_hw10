#ifndef QUICK_H
#define QUICK_H

#include <iostream>

template <class T>
class Quick {
public:
	Quick(T * arr, int size);
	T partition(T * arr, int low, int high);
	void quickSort(T * arr, int low, int high);
	void print(T * arr, int size);
};


/**
@name		Quick
@brief		calls the quicksort function
@param		array size and array head
@retval		none
*/
template <class T>
Quick<T>::Quick(T * arr, int size) {
	quickSort(arr, 0, size - 1);
}


/**
@name		partition
@brief		creates a partition within the array
			and sorts that subarray
@param		array head, lower and upper bounds of partition
@retval		index of the partitions midpoint
*/
template <class T>
T Quick<T>::partition(T * arr, int low, int high) {
	
	// first index
	T pivot = arr[low];
	
	// finding midpoint relative to start
	int count = 0;
	for (int i = low + 1; i <= high; i++) {
		if (arr[i] <= pivot)
			count++;
	}
	
	// swapping midpoint value with first's value
	int pivotIndex = low + count;
	T temp = arr[low];
	arr[low] = arr[pivotIndex];
	arr[pivotIndex] = temp;
	
	int i = low;
	int j = high;
	
	// ensures increments don't go past midpoint
	while (i < pivotIndex && j > pivotIndex) {
		while (arr[i] <= pivot) {
			i++;
		}
		while (arr[j] > pivot) {
			j--;
		}
		
		// swapping higher and lower values
		if (i < pivotIndex && j > pivotIndex) {
			temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
			i++; j--;
		}
	}
	
	return pivotIndex;
}


/**
@name		quickSort
@brief		recursively calls itself and the partition function
@param		array head, lower and upper bounds of partition
@retval		none
*/
template <class T>
void Quick<T>::quickSort(T * arr, int low, int high) {
	
	// base case
	if (low >= high)
		return;;
	
	int part = partition(arr, low, high);
	
	// sorts lower partition
	quickSort(arr, low, part - 1);
	
	// sorts higher partition
	quickSort(arr, part + 1, high);
}


/**
@name		print
@brief		prints the array
@param		array head and size
@retval		none
*/
template <class T>
void Quick<T>::print(T * arr, int size) {
	for (int i = 0; i < size; i++) {
		std::cout << arr[i] << ' ';
	}
}	

#endif