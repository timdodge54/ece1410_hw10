#ifndef INSERTION_H
#define INSERTION_H

#include <iostream>

template <class T>
class Insertion {
public:
	Insertion(T * arr, int size);
	void print(T * arr, int size);
};


/**
@name		Insertion
@brief		sorts array using insertion sort
@param		array head and size
@retval		none
*/
template <class T>
Insertion<T>::Insertion(T * arr, int size) {
	
	T comparand;
	int j;
	
	//  looping through adjacent indices
	for (int i = 1; i < size; i++) {
		comparand = arr[i];
		j = i - 1;
		
		// moves smaller terms to bottom of array
		while (j >= 0 && arr[j] > comparand) {
			arr[j + 1] = arr[j];
			j--;
		}
		arr[j + 1] = comparand;
	}
}


/**
@name		print
@brief		prints array
@param		array head and size
@retval		none
*/
template <class T>
void Insertion<T>::print(T * arr, int size) {
	for (int i = 0; i < size; i++) {
		std::cout << arr[i] << ' ';
	}
}
#endif