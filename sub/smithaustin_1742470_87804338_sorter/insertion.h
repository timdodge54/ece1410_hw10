#ifndef INSERTION_H
#define INSERTION_H

#include <iostream>
using namespace std;

template<typename T>
class Insertion {
public:
/********************************************
	* @name Insertion sort
	* @brief Sorts using the inerton method
	* @param element, number
	* @retval none
	********************************************/
    Insertion(T *arr, int n) {
        int j;
        T temp;
        for (int i = 1; i < n; i++) {
            j = i - 1;
            temp = arr[i];
            while (j >= 0 && arr[j] > temp) {
                arr[j+1] = arr[j];
                j--;
            }
            arr[j + 1] = temp;
        }
    }
	/********************************************
	* @name print
	* @brief prints sorted array
	* @param Element number of elemnts
	* @retval none
	********************************************/
    void print(T *arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }

        
};

#endif
