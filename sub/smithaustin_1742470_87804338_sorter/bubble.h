#ifndef BUBBLE_H
#define BUBBLE_H

#include <iostream>
using namespace std;


template<typename T>
class Bubble {
public:
	/********************************************
	* @name Bubble_Sort
	* @brief Sorts using the bubble method
	* @param element, number
	* @retval none
	********************************************/
    Bubble(T *arr, int n) {
		bool swapped;
        for (int i = 0; i<n-1; i++) {
            swapped = false;
            for (int j = 0; j<n-i-1; j++) {
                if (arr[j] > arr[j+1]) {
                    swap(arr[j], arr[j+1]);
                    swapped = true;
                }
            }
            if (swapped == false) {
                break;
            }
        }
    }
 
	/********************************************
	* @name print
	* @brief prints sorted array
	* @param Element number of elemnts
	* @retval none
	********************************************/
    void print(T *arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }
        
};

#endif
