#ifndef SELECTION_H
#define SELECTION_H

#include <iostream>
using namespace std;

template<typename T>
class Selection {
public:
	/********************************************
	* @name Selection_Sort
	* @brief Sorts using the selection method
	* @param element, number
	* @retval none
	********************************************/
    Selection(T *arr, int n) {
       int minIndex;
        for (int i = 0; i < n - 1; i++) {
            minIndex = i;
            for (int j = i + 1; j < n; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            swap(arr[i], arr[minIndex]);
        }
    }
    /********************************************
	* @name print
	* @brief prints sorted array
	* @param Element number of elemnts
	* @retval none
	********************************************/
    void print(T *arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }
  
};

#endif
