#ifndef QUICK_H
#define QUICK_H

#include <iostream>
using namespace std;

template<typename T>
class Quick {
public:
/********************************************
	* @name Quick sort
	* @brief Sorts using the quick method
	* @param element, number
	* @retval none
	********************************************/
    Quick(T *arr, int n) {
        quickSort(arr, 0, n - 1);
    }
	
	/********************************************
	* @name print
	* @brief prints sorted array
	* @param Element number of elemnts
	* @retval none
	********************************************/
    void print(T *arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }
private:
/********************************************
	* @name quick sort
	* @brief quick sorts by selctin pivot and low, high starts recursion
	* @param element, low, high
	* @retval none
	********************************************/
    void quickSort(T *arr, int low, int high) {
        if (low < high) {
            int pivotIndex = partition(arr, low, high);
            quickSort(arr, low, pivotIndex - 1);
            quickSort(arr, pivotIndex + 1, high);
        }
    }
	/********************************************
	* @name partition
	* @brief swaps the pointers
	* @param element, low, high
	* @retval int
	********************************************/
    int partition(T *arr, int low, int high) {
        T pivot = arr[high];
        int i = low - 1;
        for (int j = low; j <= high - 1; j++) {
            if (arr[j] < pivot) {
                i++;
                swap(arr[i], arr[j]);
            }
        }
       swap(arr[i + 1], arr[high]);
        return i + 1;
    }
};

#endif
