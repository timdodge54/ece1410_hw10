#pragma once
#include <iostream>
using namespace std;

//Selection Sort class template
template <class T>
class Selection {
public:
    Selection(T*, int);
    void print(T*, int);
};

/*************************************************
* @name Selection
* @brief Constructor for an array sorted with the selection method
* @param Pointer to beggining spot of array, and size of array to sort
* @retval None
*************************************************/
template <class T>
Selection<T>::Selection(T* A, int length) {
    int n = length;
    T temp;
    int i, j, iMin;
    //selection algorithm
    for (j = 0; j < n - 1; j++) {
        iMin = j;
        //loop to find min
        for (i = j + 1; i < n; i++) {
            if (A[i] < A[iMin]) {
                iMin = i;
            }
        }
        //swap
        if (iMin != j) {
            temp = A[j];
            A[j] = A[iMin];
            A[iMin] = temp;
            //swap(a[j], a[iMin]);
        }
    }
}

/*************************************************
* @name print
* @brief prints the values in the array
* @param Pointer to beggining spot of array, and size of array to
* @retval None
*************************************************/
template <class T>
void Selection <T>::print(T* A, int n)
{
    int i; 
    for (i = 0; i < n; i++) {
        cout << A[i] << " ";
    }
}