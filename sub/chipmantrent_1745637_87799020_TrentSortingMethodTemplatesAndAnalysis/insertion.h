#pragma once
#include <iostream>
using namespace std;

//Insertion Sort class template
template <class T>
class Insertion {
public:
    Insertion(T*, int);
    void print(T*, int);
};

/*************************************************
* @name Insertion
* @brief Constructor for an array sorted with the insertion method
* @param Pointer to beggining spot of array, and size of array to sort
* @retval None
*************************************************/
template <class T>
Insertion<T>::Insertion(T* A, int length) {
    int n = length;
    T temp;
    int i = 1, j;
    while (i < n) {
        j = i;
        //loop through
            while ((j > 0) && (A[j - 1] > A[j])) {
                //swap A[j] and A[j - 1]
                temp = A[j - 1];
                A[j - 1] = A[j];
                A[j] = temp;
                j = j - 1;
            }
        i++;
    }
}

/*************************************************
* @name print
* @brief prints the values in the array
* @param Pointer to beggining spot of array, and size of array
* @retval None
*************************************************/
template <class T>
void Insertion <T>::print(T* A, int n)
{
    int i;
    for (i = 0; i < n; i++) {
        cout << A[i] << " ";
    }
}