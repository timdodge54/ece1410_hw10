#pragma once
#include <iostream>
using namespace std;

// Bubble Sort class template
template <class T>
class Bubble {
public:
    Bubble(T*, int);
    void print(T*, int);
};

/*************************************************
* @name Bubble
* @brief Constructor for an array sorted with the bubble method
* @param Pointer to beggining spot of array, and size of array to sort
* @retval None
*************************************************/
template <class T>
Bubble<T>::Bubble(T* A, int length) {
    int n = length;
    T temp;
    int i;
    int swapped = 1;
    while (swapped) {
        //keep looping untill nothing swapps
        swapped = 0;
        i = 1;
        do {
            //loop through each item
            if (A[i - 1] > A[i]) {
                //swap when greater
                temp = A[i - 1];
                A[i - 1] = A[i];
                A[i] = temp;
                swapped = true;
                
            }
            i++;
        } while (i < n);
    }
}

/*************************************************
* @name print
* @brief prints the values in the array
* @param Pointer to beggining spot of array, and size of array
* @retval None
*************************************************/
template <class T>
void Bubble <T>::print(T* A, int n)
{
    int i;
    for (i = 0; i < n; i++) {
        cout << A[i] << " ";
    }
}