#pragma once
#include <iostream>
using namespace std;

//Quick Sort class template
template <class T>
class Quick {
    void quicksort(T*, int, int);
    int partition(T*, int, int);
public:
    Quick(T*, int);
    void print(T*, int);
};

/*************************************************
* @name Quick
* @brief Constructor for an array sorted with the quick method
* @param Pointer to beggining spot of array, and size of array to sort
* @retval None
*************************************************/
template <class T>
Quick<T>::Quick(T* A, int length) {
    quicksort(A, 0, length-1);
}

/*************************************************
* @name quicksort
* @brief to be called recursively, splits an array
* @param Pointer to beggining spot of array, bottom, and top spots
* @retval None
*************************************************/
template <class T>
void Quick <T>::quicksort(T* A, int lo, int high)
{
    int p;
    if (lo < high) {
        //implement quicksort method of spliting and swapping recursively
        p = partition(A, lo, high);
        quicksort(A, lo, p - 1);
        quicksort(A, p + 1, high);
    }
}

/*************************************************
* @name partition
* @brief swaps spots in an array, used with quicksort
* @param Pointer to beggining spot of array, bottom, and top spots
* @retval Amount of pivots
*************************************************/
template <class T>
int Quick <T>::partition(T* A, int lo, int high)
{
    T temp;
    int i = lo - 1, j;
    T pivot = A[high];
    for (j = lo; j < high; j++) {
        if (A[j] < pivot) {
            i = i + 1;
            //swap A[i] with A[j]
            temp = A[i];
            A[i] = A[j];
            A[j] = temp;
        }
    }
    //swap A[i + 1] with A[hi]
    temp = A[i + 1];
    A[i + 1] = A[high];
    A[high] = temp;
    return i + 1;
}

/*************************************************
* @name print
* @brief prints the values in the array
* @param Pointer to beggining spot of array, and size of array
* @retval None
*************************************************/
template <class T>
void Quick <T>::print(T* A, int n)
{
    int i;
    for (i = 0; i < n; i++) {
        cout << A[i] << " ";
    }
}