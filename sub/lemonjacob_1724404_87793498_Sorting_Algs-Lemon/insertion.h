#ifndef insertion_h
#define insertion_h

#include <iostream>

template <class T>
class Insertion {
	public:
		Insertion(T* nums, int N);
		void print(T* numbers, int N);
};

/*************************************************************************
* @name Insertion
* @brief this is the constructor for the Insertion class. This sorts a 
* given list
* @param pointer to a list and the size of the list
* @retval none
*************************************************************************/
template <class T>
Insertion<T>::Insertion(T* nums, int N)
{
	int i=0, j=0;
	T swapper;
	i=1;
	//loop through the list n times
	while (i<N)
	{
		j=i;
		//while j>0 and our current element is smaller than the last
		while(j>0 && nums[j-1] > nums[j])
		{
			//swap nums[j], and nums[j-1]
			swapper = nums[j];
			nums[j] = nums[j-1];
			nums[j-1] = swapper;
			//decrement j
			j--;
		}
		//increment i
		i = i + 1;
	}
}

/*************************************************************************
* @name print
* @brief this prints a list of elements
* @param pointer to the list and size of the list
* @retval none
*************************************************************************/
template <class T>
void Insertion<T>::print(T* numbers, int N)
{
	
	int i = 0;
	
	for (i=0; i<N; i++)
	{
		//print each element seperated by a space
		std::cout << numbers[i] << " ";
		
	}
	//print additional useful information
	std::cout << std::endl << "just did insertion sort with " 
		<< N << " items" << std::endl;
}

#endif