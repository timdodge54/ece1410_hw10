#ifndef quick_h
#define quick_h

#include <iostream>

template <class T>
class Quick {
	public:
		Quick(T* nums, int N);
		void quicksort(T* nums, int low, int high);
		int partition(T* numbers, int low, int high);
		void print(T* numbers, int N);
};

/*************************************************************************
* @name Quick
* @brief this is the constructor for the Quick class. This sorts a list by
* calling the quicksort algorithm
* @param pointer to a list and the size of the list
* @retval none
*************************************************************************/
template <class T>
Quick<T>::Quick(T* nums, int N)
{
	//call the quicksort function
	quicksort(nums, 0, N-1);
}

/*************************************************************************
* @name Quicksort
* @brief this is the quicksort algorithm function. It calls the partition 
* function and recursively calls itself until the list is sorted
* @param pointer to a list, lower index, and upper index
* @retval none
*************************************************************************/
template <class T>
void Quick<T>::quicksort(T* nums, int low, int high)
{
	int p=0; 
	if (low < high)
	{
		p = partition(nums, low, high);
		quicksort(nums, low, p-1);
		quicksort(nums, p+1, high);
	}
}

/*************************************************************************
* @name partition
* @brief this partitions a list and swaps numbers in the manner of the 
* all powerful and all fast quick sort algorithm
* @param pointer to a list, lower index, and upper index of the sub-array
* @retval integer to the partition index
*************************************************************************/
template <class T>
int Quick<T>::partition(T* numbers, int low, int high)
{
	int pivot = numbers[high];
	int i = (low - 1);
	T swapper;
	//loop through the sub-array
	for (int j = low; j < high; j++)
	{
		//if the number is smaller than the pivot,
		if (numbers[j] < pivot)
		{
			//increment i
			i++;
			//swap numbers[i], numbers [j]
			swapper = numbers[j];
			numbers[j] = numbers[i];
			numbers[i] = swapper;
		}
	}
	//swap numbers[i+1] with a[high]
	swapper = numbers[i+1];
	numbers[i+1] = numbers[high];
	numbers[high] = swapper;
	return (i+1);
}

/*************************************************************************
* @name print
* @brief this prints a list of elements
* @param pointer to the list and size of the list
* @retval none
*************************************************************************/
template <class T>
void Quick<T>::print(T* numbers, int N)
{
	
	//print and stuff
	int i = 0;
	
	for (i=0; i<N; i++)
	{
		//print each element seperated by a space
		std::cout << numbers[i] << " ";
		
	}
	std::cout << std::endl << "just did quick sort with " 
		<< N << " items" << std::endl;
}

#endif