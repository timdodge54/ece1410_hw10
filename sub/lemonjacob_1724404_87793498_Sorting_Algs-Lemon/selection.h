#ifndef selection_h
#define selection_h

#include <iostream>

template <class T>
class Selection {
	public:
		Selection(T* nums, int N);
		void print(T* numbers, int N);
};

/*************************************************************************
* @name Selection
* @brief this is the constructor for the Selection class. This sorts a 
* given list
* @param pointer to a list and the size of the list
* @retval none
*************************************************************************/
template <class T>
Selection<T>::Selection(T* nums, int N)
{
	int i=0, j=0, imin=0;
	T swapper;
	//loop
	for (j=0; j < N-1; j++)
	{
		imin = j;
		//loop
		for (i = j+1; i<N; i++)
		{
			//find the smallest index
			if (nums[i] < nums[imin])
			{
				imin = i;
			}
		}
		if (imin != j)
		{
			//swap a[j], a[imin]
			swapper = nums[j];
			nums[j] = nums[imin];
			nums[imin] = swapper;
		}
	}
}

/*************************************************************************
* @name print
* @brief this prints a list of elements
* @param pointer to the list and size of the list
* @retval none
*************************************************************************/
template <class T>
void Selection<T>::print(T* numbers, int N)
{
	//print and stuff
	int i = 0;
	for (i=0; i<N; i++)
	{
		std::cout << numbers[i] << " ";
	}
	//print additional information
	std::cout << std::endl << "just did selection sort with " 
		<< N << " items" << std::endl;

}

#endif