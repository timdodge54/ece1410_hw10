#ifndef bubble_h
#define bubble_h

#include <iostream>

template <class T>
class Bubble {
	public:
		Bubble(T* nums, int N);
		void print(T* numbers, int N);
};

/*************************************************************************
* @name Bubble
* @brief this is the constructor for the Bubble class. This sorts a given
* list
* @param pointer to a list and the size of the list
* @retval none
*************************************************************************/
template <class T>
Bubble<T>::Bubble(T* nums, int N)
{
	int swapped = 0;
	int i = 0;
	T swapper;
	//loop until swapped is false
	do
	{
		//set swapped to false
		swapped = 0;
		//iterate through the list
		for (i=1; i < N; i++)
		{
			//if the numbers are out of order
			if (nums[i-1] > nums[i])
			{
				//swap nums[i-1] and nums[i]
				swapper = nums[i-1];
				nums[i-1] = nums[i];
				nums[i] = swapper;
				//set swapped to true
				swapped = 1;
			}
		}
	} while(swapped);
	
}

/*************************************************************************
* @name print
* @brief this prints a list of elements
* @param pointer to the list and size of the list
* @retval none
*************************************************************************/
template <class T>
void Bubble<T>::print(T* numbers, int N)
{
	
	int i = 0;	
	for (i=0; i<N; i++)
	{
		//print each element seperated by a space
		std::cout << numbers[i] << " ";
	}
	//print additional information
	std::cout << std::endl << "just did bubble sort with " 
		<< N << " items" << std::endl;
}

#endif