 #include <iostream>

using namespace std;

template <class T>
class Quick {
public:
	Quick(T* A, int length);
	void print(T* A, int length);
	void Sort(T* A, int left, int right);
};
/******************************************************************************
@name	Quick
@brief	consructor, that inputs A to a sorter
@praam	A and length	
@retval	a sorted A
******************************************************************************/
template <class T>
Quick<T>::Quick(T* A, int length){
	int left;		//creates left variable
	left = 0;
	Sort(A, left, length-1);//when calling sort function, creates right
}

/******************************************************************************
@name	Sort
@brief	assists in the sorting from the function
@praam	A, left, and right variable	
@retval	none
******************************************************************************/
template <class T>
void Quick<T>::Sort(T* A, int left, int right) {
	int i = left, j = right;	//left and right assigned to I&J
	int pivot = A[(left + right) / 2];	//creates the pivot point

	while (i <= j) {			//while on the left
		while (A[i] < pivot)	//and its lower than pivot value
			i++;				//increment i
		while (A[j] > pivot)//vise versa
				j--;
		if (i <= j) {			//if "I" i less or = to "J" 
			swap (A[i], A[j]);	//swap values
			i++;
			j--;
		}
	};
	if (left < j)			//if left value is lower 
		Sort(A, left, j);	//then it will stay the same when thrown into the
							//next function
	if (i < right)			//but if i is lower, you'll keep the right value
		Sort(A, i, right);	// the same
}
/******************************************************************************
@name	print
@brief	prints values
@praam	array and length	
@retval	none
******************************************************************************/
template <class T>
void Quick<T>::print(T * A, int length){
	for (int i=0; i<length; i++)
	{
		cout<<A[i]<<"   ";
	}
}

