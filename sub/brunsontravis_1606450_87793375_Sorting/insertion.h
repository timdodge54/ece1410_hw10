#include <iostream>
using namespace std;

template <class T>
class Insertion {
	public:
		Insertion(T * A, int length);
		void print(T * A, int length);
};
/******************************************************************************
@name	Insertion
@brief	a constructor that will sort the given array
@praam	array and length	
@retval	a sorted list
******************************************************************************/
template <class T>
Insertion<T>::Insertion(T * A, int length)
{
	T temp;	//create variables
	int j;
    for(int i = 1; i <= length - 1; i++)	//grow loop
    {
        temp = A[i];			//keep position
        for(j = i; j > 0; j--)	
            if(temp < A[j - 1])	//if value is greater than previous
               A[j] = A[j - 1];	//take value
            else break;
        A[j] =  temp;	//move A[j]
    }
}

/******************************************************************************
@name	print
@brief	prints values
@praam	array and length	
@retval	none
******************************************************************************/
template <class T>
void Insertion<T>::print(T * A, int length)
{
	for (int i=0; i<length; i++)
	{
		cout<<A[i]<<"   ";
	}
}

