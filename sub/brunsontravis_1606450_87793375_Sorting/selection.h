#include <iostream>
using namespace std;

template <class T>
class Selection {
	public:
		Selection(T * A, int length);
		void print(T * A, int length);
};
/******************************************************************************
@name	Insertion
@brief	a constructor that will sort the given array
@praam	array and length	
@retval	a sorted list
******************************************************************************/
template <class T>
Selection<T>::Selection(T * A, int length)
{
	int i;
	int j;
	for (i=0; i< length; i++)
	{
		j = i;
		while (j > 0 && (A [j-1] > A [j]))	//while greater than zer0 and the
		{									//values are out of order
			swap(A[j], A[j-1]);				//swap values
			j--;
		}
	}
}
/******************************************************************************
@name	print
@brief	prints values
@praam	array and length	
@retval	none
******************************************************************************/
template <class T>
void Selection<T>::print(T * A, int length)
{
	for (int i=0; i<length; i++)
	{
		cout<<A[i]<<"   ";
	}
}