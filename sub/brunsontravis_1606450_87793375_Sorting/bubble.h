#include <iostream>
using namespace std;
template <class T>
class Bubble {
	public:
		Bubble(T * A, int length);
		void print(T * A, int length);
};
/******************************************************************************
@name	Bubble
@brief	a constructor that will sort the given array
@praam	array and length	
@retval	a sorted list
******************************************************************************/
template <class T>
Bubble<T>::Bubble(T * A, int length)
{
	for (int i = 1; i <= length; i++)	//simple for loop
    {
        for (int j = 0; j < length; j++)//simple for loop
        {
            if (A[j] < A[j - 1])//if current value is less than previous
            {
                swap(A[j], A[j - 1]);//swap values
            }
        }
    }
}
/******************************************************************************
@name	print
@brief	prints values
@praam	array and length	
@retval	none
******************************************************************************/
template <class T>
void Bubble<T>::print(T * A, int length)
{
	for (int i=0; i<length; i++)
	{
		cout<<A[i]<<"   ";
	}
}
	
