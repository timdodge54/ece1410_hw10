#ifndef QUICK_H
#define QUICK_H

template <class T>
class Quick
{
	private:
	void swap(T *a, T *b);
	int partition(T *nums,int N);
	
	public:
	Quick(T *nums, int N);
	void print (T *nums, int N);
};

/******************************************************************************
* @name		Quick	
* @brief	Sorts an array of numbers using quick sort.
* @param	Pointer to head of an array, and number of elements in the array.
* @retval	None.
******************************************************************************/

template <class T>
Quick<T>::Quick(T *nums, int N)
{
	int p;
	
	//if there are elements in the array
	if(N>0)
	{
		//Then find the partition and call quick recursively.
		p = partition(nums, N);
		Quick(nums, p-1);
		Quick(nums+p+1, N-p);
	}
}	

/******************************************************************************
* @name		Insertion	
* @brief	Sorts an array of numbers using insertion sort.
* @param	Pointer to head of an array, and number of elements in the array.
* @retval	None.
******************************************************************************/

template <class T>
int Quick<T>::partition(T *nums,int N)
{
	//innitialize variables.
	T pivot = *(nums+N-1);
	int i = 0;
	int j = 0;
	
	//Loop while i is not at the end of the array.
	while( i <= N-1)
	{
	
		//if current element is smaller than pivot incriment i.
		if(*(nums+i) < pivot)
		{
			i++;
		}
		//else swap i and j then incriment them both.
		else
		{
			swap(nums+i, nums+j);
			i++;
			j++;
		}
		
	}
	
	//return the partition.
	return j-1;
}	

/******************************************************************************
* @name		swap	
* @brief	swaps the content of two elements.
* @param	Two pointers to elements.
* @retval	None
******************************************************************************/

template <class T>
void Quick<T>::swap(T *a, T *b)
{
	T temp;
	
	//swap the contents from element a and element b.
	temp = *a;
	*a = *b;
	*b = temp;
}

/******************************************************************************
* @name		print	
* @brief	Prints all elements in an array.
* @param	Pointer to head of the array, elements in the array.
* @retval	None
******************************************************************************/

template <class T>
void Quick<T>::print(T *nums, int N)
{
	int i;
	
	//Loop until all elements have been printed.
	for(i=0;i<N;i++)
	{
		//Print contents of the array.
		std::cout<<*(nums+i)<<" ";
	}
}




#endif