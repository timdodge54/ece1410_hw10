#ifndef INSERTION_H
#define INSERTION_H

template <class T>
class Insertion
{
	private:
	void swap(T *a, T *b);
	
	public:
	Insertion(T *nums, int N);
	void print (T *nums, int N);
};

/******************************************************************************
* @name		Insertion	
* @brief	Sorts an array of numbers using insertion sort.
* @param	Pointer to head of an array, and number of elements in the array.
* @retval	None.
******************************************************************************/

template <class T>
Insertion<T>::Insertion(T *nums, int N)
{
	int i, j;
	
	//Loop until all elements are sorted
	for(i=1; i<N; i++)
	{
		//Loop as long as elements need to be moved.
		for(j=i; j>0 && (*(nums+j-1)>*(nums+j)); j--)
		{
			//swap the elements that need to be moved.
			swap(nums+j, nums+j-1);
		}
	}
}

/******************************************************************************
* @name		swap	
* @brief	swaps the content of two elements.
* @param	Two pointers to elements.
* @retval	None
******************************************************************************/

template <class T>
void Insertion<T>::swap(T *a, T *b)
{
	T temp;
	
	//swap the contents from element a and element b.
	temp = *a;
	*a = *b;
	*b = temp;
}

/******************************************************************************
* @name		print	
* @brief	Prints all elements in an array.
* @param	Pointer to head of the array, elements in the array.
* @retval	None
******************************************************************************/

template <class T>
void Insertion<T>::print(T *nums, int N)
{
	int i;
	
	//Loop until all elements have been printed.
	for(i=0;i<N;i++)
	{
		//Print contents of the array.
		std::cout<<*(nums+i)<<" ";
	}
}


#endif