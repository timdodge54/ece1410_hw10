#ifndef SELECTION_H
#define SELECTION_H

template <class T>
class Selection
{
	private:
	void swap(T *a, T *b);
	
	public:
	Selection(T *nums, int N);
	void print (T *nums, int N);
};

/******************************************************************************
* @name		Insertion	
* @brief	Sorts an array of numbers using insertion sort.
* @param	Pointer to head of an array, and number of elements in the array.
* @retval	None.
******************************************************************************/

template <class T>
Selection<T>::Selection(T *nums, int N)
{
	int i, j, k;
	
	//Loop until all elements are sorted.
	for (j = 0; j < N-1; j++) 
	{
		k = j;
		
		//Loop through all elements in the array.
		for (i = j+1; i < N; i++)
		{
			//if current element is less than k, set that to the lowest element.
			if (*(nums+i) < *(nums+k)) 
			{
				k = i;
			}
		}
		
		//if k is no longer j swap j and k.
		if (k != j) 
		{		
			swap((nums+j), (nums+k));
		}
	}
}

/******************************************************************************
* @name		swap	
* @brief	swaps the content of two elements.
* @param	Two pointers to elements.
* @retval	None
******************************************************************************/

template <class T>
void Selection<T>::swap(T *a, T *b)
{
	T temp;
	
	//swap the contents from element a and element b.
	temp = *a;
	*a = *b;
	*b = temp;
}

/******************************************************************************
* @name		print	
* @brief	Prints all elements in an array.
* @param	Pointer to head of the array, elements in the array.
* @retval	None
******************************************************************************/

template <class T>
void Selection<T>::print(T *nums, int N)
{
	int i;
	
	//Loop until all elements have been printed.
	for(i=0;i<N;i++)
	{
		//Print contents of the array.
		std::cout<<*(nums+i)<<" ";
	}
}


#endif