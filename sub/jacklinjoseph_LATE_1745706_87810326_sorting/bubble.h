#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>

template <class T>
class Bubble
{
	private:
	void swap(T *a, T *b);
	
	public:
	Bubble(T *nums, int N);
	void print (T *nums, int N);
};

/******************************************************************************
* @name		Bubble	
* @brief	Sorts an array of numbers using bubble sort.
* @param	Pointer to head of an array, and number of elements in the array.
* @retval	None.
******************************************************************************/

template <class T>
Bubble<T>::Bubble(T *nums, int N)
{
	int swapped, i;
	T *j, *k;
	
	//Loop
	do
	{
		swapped = 0;
		
		//Loop until the last element is in its place.
		for(i=1; i<=N-1; i++)
		{
			j = (nums + (i-1));
			k = (nums + i);
			
			//If j is bigger than k then swap them.
			if(*j > *k)
			{
				swap(j, k);
				swapped = 1;
			}
		}
		
		//Shorten the viewed poriton of the array by 1.
		N--;
		
	} 
	//EndLoop when the array is sorted. 
	while(swapped!=0);
}

/******************************************************************************
* @name		swap	
* @brief	swaps the content of two elements.
* @param	Two pointers to elements.
* @retval	None
******************************************************************************/

template <class T>
void Bubble<T>::swap(T *a, T *b)
{
	T temp;
	
	//swap the contents from element a and element b.
	temp = *a;
	*a = *b;
	*b = temp;
}

/******************************************************************************
* @name		print	
* @brief	Prints all elements in an array.
* @param	Pointer to head of the array, elements in the array.
* @retval	None
******************************************************************************/

template <class T>
void Bubble<T>::print(T *nums, int N)
{
	int i;
	
	//Loop until all elements have been printed.
	for(i=0;i<N;i++)
	{
		//Print contents of the array.
		std::cout<<*(nums+i)<<" ";
	}
}

#endif