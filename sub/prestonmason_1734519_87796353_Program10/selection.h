#pragma once
#include <iostream>
#include <algorithm>
template <class T>
class Selection {
	void sort(T* A, int N);
public:
	Selection(T* nums, int N);
	void print(T* nums, int N);
};

/*-----------------------------------------------------------------------------
@name Selection
@breif constructor of selection sort. calls the sort function
@param address of data set, and number of data elements
@retval none
-----------------------------------------------------------------------------*/

template <class T>
Selection<T>::Selection(T* nums, int N) {
	//call sort
	sort(nums, N);
}

/*-----------------------------------------------------------------------------
@name print
@breif prints a data set
@param address of data set, and number of data elements
@retval none
-----------------------------------------------------------------------------*/

template<class T>
void Selection<T>::print(T* nums, int N) {
	//output each element of the array
	for (int i = 0; i < N; i++) {
		std::cout << nums[i] << " ";
	}
}

/*-----------------------------------------------------------------------------
@name sort
@breif sorts a data set using the bubble sort method
@param address of data set, and number of data elements
@retval none
-----------------------------------------------------------------------------*/

template <class T>
void Selection<T>::sort(T* A, int N) {
	//for each element in the array
	for (int j = 0; j < N; j++) {
		int min = j;
		//for each unsorted element
		for (int i = j + 1; i < N; i++) {
			//compare elements
			if (A[i] < A[min]) {
				min = i;
			}
		}
		//swap elements
		if (min != j) {
			std::swap(A[j], A[min]);
		}
	}
}