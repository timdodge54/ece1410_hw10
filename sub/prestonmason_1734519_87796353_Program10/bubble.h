#pragma once
#include <iostream>
#include <algorithm>
template <class T>
class Bubble {
	void sort(T* A, int N);
public:
	Bubble(T* nums, int N);
	void print(T* nums, int N);
};

/*-----------------------------------------------------------------------------
@name Bubble
@breif constructor of bubble sort. calls the sort function
@param address of data set, and number of data elements
@retval none
-----------------------------------------------------------------------------*/

template <class T>
Bubble<T>::Bubble(T* nums, int N) {
	//call sort
	sort(nums, N);
}

/*-----------------------------------------------------------------------------
@name print
@breif prints a data set
@param address of data set, and number of data elements
@retval none
-----------------------------------------------------------------------------*/

template <class T>
void Bubble<T>::print(T* nums, int N) {
	//output each element of the array
	for (int i = 0; i < N; i++) {
		std::cout << nums[i] << " ";
	}
}

/*-----------------------------------------------------------------------------
@name sort
@breif sorts a data set using the bubble sort method
@param address of data set, and number of data elements
@retval none
-----------------------------------------------------------------------------*/

template <class T>
void Bubble<T>::sort(T* A, int N) {
	int swaped = 1;
	//while an element has been swaped
	while (swaped == 1) {
		swaped = 0;
		//for each elements in the array
		for (int i = 1; i < N; i++) {
			//compare and swap elements
			if (A[i - 1] > A[i]) {
				std::swap(A[i - 1], A[i]);
				swaped = 1;
			}
		}
	}
}
