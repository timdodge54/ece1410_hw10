#pragma once
#include <iostream>
#include <algorithm>
template <class T>
class Quick {
	void sort(T* A, int lo, int hi);
	int partition(T* A, int lo, int hi);
public:
	Quick(T* nums, int N);
	void print(T* nums, int N);
};

/*-----------------------------------------------------------------------------
@name Quick
@breif constructor of quick sort. calls the sort function
@param none
@retval none
-----------------------------------------------------------------------------*/

template <class T>
Quick<T>::Quick(T* nums, int N) {
	//call sort
	sort(nums, 0, (N - 1));
}

/*-----------------------------------------------------------------------------
@name print
@breif prints a data set
@param address of data set, and number of data elements
@retval none
-----------------------------------------------------------------------------*/

template <class T>
void Quick<T>::print(T* nums, int N) {
	//output each element of the array
	for (int i = 0; i < N; i++) {
		std::cout << nums[i] << " ";
	}
}

/*-----------------------------------------------------------------------------
@name sort
@breif sorts a data set using the bubble sort method
@param address of data set, and number of data elements
@retval none
-----------------------------------------------------------------------------*/

template <class T>
void Quick<T>::sort(T* A, int lo, int hi) {
	//if not done sorting
	if (lo < hi) {
		//get partition
		int p = partition(A, lo, hi);
		//sort 2 more partitions
		sort(A, lo, p - 1);
		sort(A, p + 1, hi);
	}
}

/*-----------------------------------------------------------------------------
@name partition
@breif makes an image of a mandelbrot set with one of 3 possible color schemes
@param none
@retval none
-----------------------------------------------------------------------------*/

template <class T>
int Quick<T>::partition(T* A, int lo, int hi) {
	//get pivot
	T piv = A[hi];
	int i = lo - 1;
	//for each element of the partition
	for (int j = lo; j < hi; j++) {
		//if the element is lower than the pivot
		if (A[j] < piv) {
			//swap elements
			++i; std::swap(A[i], A[j]);
		}
	}
	//swap elements and return the partition
	std::swap(A[i + 1], A[hi]);
	return i + 1;
}
