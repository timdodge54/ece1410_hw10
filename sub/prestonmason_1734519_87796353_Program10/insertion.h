#pragma once
#include <iostream>
#include <algorithm>
template <class T>
class Insertion {
	void sort(T* A, int N);
public:
	Insertion(T* nums, int N);
	void print(T* nums, int N);
};

/*-----------------------------------------------------------------------------
@name Insertion
@breif constructor of insertion sort. calls the sort function
@param address of data set, and number of data elements
@retval none
-----------------------------------------------------------------------------*/

template <class T>
Insertion<T>::Insertion(T* nums, int N) {
	//call sort
	sort(nums, N);
}

/*-----------------------------------------------------------------------------
@name print
@breif prints a data set
@param address of data set, and number of data elements
@retval none
-----------------------------------------------------------------------------*/

template <class T>
void Insertion<T>::print(T* nums, int N) {
	//output each element of the array
	for (int i = 0; i < N; i++) {
		std::cout << nums[i] << " ";
	}
}

/*-----------------------------------------------------------------------------
@name sort
@breif sorts a data set using the bubble sort method
@param address of data set, and number of data elements
@retval none
-----------------------------------------------------------------------------*/

template <class T>
void Insertion<T>::sort(T* A, int N) {
	int i = 1, j;
	//for each element in the array
	while (i < N) {
		j = i;
		//while somethings has been swapped compare elements
		while (j > 0 && A[j - 1] > A[j]) {
			std::swap(A[j], A[j - 1]);
			j--;
		}
		i++;
	}
}