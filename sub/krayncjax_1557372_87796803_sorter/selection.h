#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>
using namespace std;

template <typename T>
class Selection
{
	public:
		Selection(T * , int);
		void print(T *, int);
};		

/****************************************************************************
* @name     Selection
* @brief    constructor for selection sort class
* @param	none
* @retval   none
*****************************************************************************/
template <typename T>
Selection<T>::Selection(T * arr, int n)
{
	int i,j,imin;

	for(j=0; j<n-1; j++)
	{
		imin = j;
		for(i=j+1; i<n; i++)
		{
			if(arr[i] < arr[imin])
			{
				imin = i;
			}
		}

		if(imin != j)
		{
			swap(arr[j], arr[imin]);						
		}	
	}

}	
/****************************************************************************
* @name     print
* @brief    print function for sorters
* @param	none
* @retval   none
*****************************************************************************/
template <typename T>
void Selection<T>::print(T* arr, int n)
{
    for(int i=0; i<n; i++){
        cout << arr[i] << " ";
    }
    cout <<endl;
}








#endif