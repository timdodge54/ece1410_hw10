#ifndef QUICK_H
#define QUICK_H
#include <iostream>
using namespace std;

template <typename T>
class Quick
{
	public:
		Quick(T * , int);
		void print(T *, int);
		void quicksort(T* arr, int lo, int high);
		int partition(T* arr, int lo, int high);		
};


/****************************************************************************
* @name     partition
* @brief    part of quick sort algorithm
* @param	none
* @retval   none
*****************************************************************************/
template <typename T>
int Quick<T>::partition(T* arr, int lo, int high)
{
    T pivot = arr[high]; 
    int i = (lo - 1); 
 
    for (int j = lo; j <= high - 1; j++)
    {
        
        if (arr[j] < pivot)
        {
            i++; 
            swap(arr[i], arr[j]);
        }
    }
    swap(arr[i + 1], arr[high]);
    return (i + 1);
}

/****************************************************************************
* @name     quicksort
* @brief    part of quicksort algorithm
* @param	none
* @retval   none
*****************************************************************************/
template <typename T>
void Quick<T>::quicksort(T* arr, int lo, int high)
{
     if (lo < high)
    {
        
        int p = partition(arr, lo, high);
        quicksort(arr, lo, p - 1);
        quicksort(arr, p + 1, high);
    }
}
/****************************************************************************
* @name     Quick
* @brief    constructor for Quick class
* @param	none
* @retval   none
*****************************************************************************/
template <typename T>
Quick<T>::Quick(T* arr, int n)
{
	quicksort( arr, 0, n-1);
}

/****************************************************************************
* @name     print
* @brief    print function for sorters
* @param	none
* @retval   none
*****************************************************************************/
template <typename T>
void Quick<T>::print(T* arr, int n)
{
    for(int i=0; i<n; i++){
        cout << arr[i] << " ";
    }
    cout <<endl;
}

#endif