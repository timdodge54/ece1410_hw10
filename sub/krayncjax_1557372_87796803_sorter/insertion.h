#ifndef INSERTION_H
#define INSERTION_H
#include <iostream>
using namespace std;

template <typename T>
class Insertion
{
	public:
		Insertion(T * , int);
		void print(T *, int);
};
/****************************************************************************
* @name     Insertion
* @brief    Constructor for insertion class
* @param	none
* @retval   none
*****************************************************************************/
template <typename T>
 Insertion<T>::Insertion(T* arr, int n)
 {
	int i,j,k;
	for(i = 1; i < n; i++)
	{
		k = arr[i];
		j = i - 1;
		while (j >= 0 && arr[j] > k)
		{
			arr[j + 1]=arr[j];
			j = j - 1;								
		}
		arr[j + 1]=k;						
	}
 }

/****************************************************************************
* @name     print
* @brief    print function for sorters
* @param	none
* @retval   none
*****************************************************************************/
template <typename T>
void Insertion<T>::print(T* arr, int n)
{
    for(int i=0; i<n; i++){
        cout << arr[i] << " ";
    }
    cout <<endl;
}

#endif