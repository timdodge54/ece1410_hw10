#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>
using namespace std;

template <typename T>
class Bubble
{
	public:
		Bubble(T * , int);
		void print(T *, int);
};

/****************************************************************************
* @name     Bubble
* @brief    constructor for bubble class
* @param	none
* @retval   none
*****************************************************************************/
template <typename T>
Bubble<T>::Bubble(T* arr, int n)
{
    int i, j;
    for (i = 0; i < n - 1; i++)
		for (j = 0; j < n - i - 1; j++)
            if (arr[j] > arr[j + 1])
                swap(arr[j], arr[j + 1]);
}


/****************************************************************************
* @name     print
* @brief    print function for sorters
* @param	none
* @retval   none
*****************************************************************************/
template <typename T>
void Bubble<T>::print(T* arr, int n)
{
    for(int i=0; i<n; i++){
        cout << arr[i] << " ";
    }
    cout <<endl;
}





#endif