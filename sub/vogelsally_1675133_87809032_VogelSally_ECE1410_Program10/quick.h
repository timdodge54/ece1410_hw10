#include <iostream>

using namespace std;

#ifndef QUICK_H
#define QUICK_H

template <class T>
class Quick
{
	public:
		Quick(T *, int);
		void print(T *, int);
	private:
		void quicksort(T *, int low, int high);
		T partition(T *, int low, int high);
		T pivot;
		int i;
		int j;
		T high;
		T low;
		T temp;
};

#endif

/****************************************************************************** 
* @name Quick
* @brief Constructor quick sorts list of N random integers
* @param T * nums, int N
* @retval none
******************************************************************************/ 

template <class T>
Quick<T>::Quick(T * nums, int N)
{
	// Set low and high variables to the first and last elements of the list
	low = 0;
	high = N - 1;
	
	// call the quicksort function
	quicksort(nums, low, high);
}

/****************************************************************************** 
* @name quicksort
* @brief quicksort iterates until both sides of the array are sorted
* @param T * nums, int low, int high
* @retval none
******************************************************************************/
	
template <class T>
void Quick<T>::quicksort(T * nums, int low, int high)
{	
	T p;
	
	// If the value of low is still less than the value of high
	if(low < high)
	// Then
	{
		// Call the partition function
		p = partition(nums, low, high);
		
		// Recursively call the quicksort function twice
		// This allows both sides of the pivot element to be sorted
		quicksort(nums, low, p - 1);
		quicksort(nums, p + 1, high);
	}
	// EndIf
	
}

/****************************************************************************** 
* @name partition
* @brief partition halves working array and puts lower values on one side and 
* higher values on the other side of the pivot value
* @param T * nums, int low, int high
* @retval new pivot element
******************************************************************************/

template <class T>
T Quick<T>::partition(T * nums, int low, int high)
{
	int i;
	
	// Set the pivot element to the last element in the array
	pivot = nums[high];
	// Set variable i to one less than variable low
	i = low - 1;
	
	// For j = low to j = high - 1
	for (j = low; j <= high - 1; j = j + 1)
	{
		// If nums[j] < pivot
		if (nums[j] < pivot)
		// Then
		{
			// Increment variable i
			i = i + 1;
			
			// Swap values at nums[i] and nums[j]
			temp = nums[i];
			nums[i] = nums[j];
			nums[j] = temp;
		}
		// EndIf
	}
	// EndLoop
	
	// Swap values of nums[high] and nums[i + 1]
	temp = nums[high];
	nums[high] = nums[i + 1];
	nums[i + 1] = temp;
	
	// Return variable i plus 1
	// This will be the new pivot element
	return i + 1;
}	
	
/****************************************************************************** 
* @name print
* @brief Prints quick-sorted list of N integers
* @param T *, int
* @retval none
******************************************************************************/ 

template <class T>
void Quick<T>::print(T * nums, int N)
{
	// For the size of the array
	for (i = 0; i < N; i = i + 1)
	{
		// Print the value of the array element to screen
		cout << nums[i] << " ";
	}
	// EndLoop
}

