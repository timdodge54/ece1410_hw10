#include <iostream>

using namespace std;

#ifndef SELECTION_H
#define SELECTION_H

template <class T>
class Selection
{
	public:
		Selection(T *, int);
		void print(T *, int);
	private:
		int i;
		int j;
		T iMin;
		T temp;
};

#endif

/****************************************************************************** 
* @name Selection
* @brief Constructor selection sorts list of N random integers
* @param T * nums, int N
* @retval none
******************************************************************************/ 

template <class T>
Selection<T>::Selection(T * nums, int N)
{
	// For the size of the array
	for(j = 0; j < N; j = j + 1)
	{
		// Set iMin equal to j
		iMin = j;
		
		// For the difference between array size and j
		for(i = j + 1; i < N; i = i + 1)
		{
			// If the value of element i is less than element iMin
			if(nums[i] < nums[iMin])
			{
				// Set iMin equal to i
				iMin = i;
			}
		}
		// EndLoop
		
		// If iMin is not still equal to j
		if(iMin != j)
		// Then
		{
			// Swap the elements so the lowest value goes to element j
			temp = nums[j];
			nums[j] = nums[iMin];
			nums[iMin] = temp;
		}
		// EndIf
		
	}
	// EndLoop
	
}

/****************************************************************************** 
* @name print
* @brief Prints selection-sorted list of N integers
* @param T * nums, int N
* @retval none
******************************************************************************/ 

template <class T>
void Selection<T>::print(T * nums, int N)
{
	// For the size of the array
	for (i = 0; i < N; i = i + 1)
	{
		// Print the value of the array element to screen
		cout << nums[i] << " ";
	}
	// EndLoop
}