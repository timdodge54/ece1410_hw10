#include <iostream>

using namespace std;

#ifndef BUBBLE_H
#define BUBBLE_H

template <class T>
class Bubble
{
	public:
		Bubble(T *, int);
		void print(T *, int);
	private:
		int swapped;
		int i;
		T temp;
};

#endif

/****************************************************************************** 
* @name Bubble
* @brief Constructor bubble sorts list of N random integers
* @param T * nums, int N
* @retval none
******************************************************************************/ 

template <class T>
Bubble<T>::Bubble(T * nums, int N)
{
	// Set variable swapped equal to one
	swapped = 1;
	
	// While swapped is equal to one
	while (swapped == 1)
	{
		// Set swapped equal to zero
		swapped = 0;
		
		// For the size of the array
		for (i = 1; i < N; i = i + 1)
		{ 
			// If the value of the first element is greater than the value
			// of the second element
			if (nums[i - 1] > nums[i])
			// Then
			{
				// Swap the elements
				temp = nums[i];
				nums[i] = nums[i - 1];
				nums[i - 1] = temp;
				// Set swapped to one
				swapped = 1;
			}
			// EndIf
		}
		// EndLoop
		
		// Shrink the size of the for loop by one each time
		N  = N - 1;
	}
	// EndLoop	 
}

/****************************************************************************** 
* @name print
* @brief Prints bubble-sorted list of N integers
* @param T * nums, int N
* @retval none
******************************************************************************/ 

template <class T>
void Bubble<T>::print(T * nums, int N)
{
	// For the size of the array
	for (i = 0; i < N; i = i + 1)
	{
		// Print the value of the array element to screen
		cout << nums[i] << " ";
	}
	// EndLoop
}