#include <iostream>

using namespace std;

#ifndef INSERTION_H
#define INSERTION_H

template <class T>
class Insertion
{
	public:
		Insertion(T *, int);
		void print(T *, int);
	private:
		int j;
		int i;
		T temp;
};

#endif

/****************************************************************************** 
* @name Insertion
* @brief Constructor insertion sorts list of N random integers
* @param T * nums, int N
* @retval none
******************************************************************************/ 

template <class T>
Insertion<T>::Insertion(T * nums, int N)
{
	// Set variable i equal to 1
	i = 1;
	
	// While i is less than size of array
	while(i < N)
	{
		// Set variable j equal to variable i
		j = i;
		
		// While j is greater than 0
		// and element j - 1 is greater than element j
		while ((j > 0) && (nums[j - 1] > nums[j]))
		{
			// Swap values of elements j and j - 1
			temp = nums[j];
			nums[j] = nums[j-1];
			nums[j-1] = temp;
			
			// Subtract one from j
			j = j - 1;
		}
		// EndLoop
		
		// Add one to variable i
		i = i + 1;
	}
	// EndLoop
}

/****************************************************************************** 
* @name print
* @brief Prints insertion-sorted list of N integers
* @param T * nums, int N
* @retval none
******************************************************************************/ 

template <class T>
void Insertion<T>::print(T * nums, int N)
{
	// For the size of the array
	for (i = 0; i < N; i = i + 1)
	{
		// Print the value of the array element to screen
		cout << nums[i] << " ";
	}
	// EndLoop
}