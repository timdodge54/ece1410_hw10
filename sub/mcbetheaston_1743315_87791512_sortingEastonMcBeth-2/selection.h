#include <iostream>
using namespace std;

//selection sort class
template <class T>
class Selection
{
public:
	//rubric functions
	Selection(T*, int);
	void print(T*, int);
};


/*****************************************************************************
* @name Selection
* @brief constructor that sorts a given array using selection sort algorithm
* @param a pointer to the head of the array, and how long the array is
* @retval none
*****************************************************************************/
template<class T>
Selection<T>::Selection(T* sel, int n)
{
	T tempvar;
	int iMin;
	int i, j;
	for (j = 0; j < n - 1; j++) {
		iMin = j;
		for (i = j + 1; i < n; i++) {
			if (sel[i] < sel[iMin]) {
				iMin = i;
			}
		}
		if (iMin != j) {
			tempvar = sel[iMin];
			sel[iMin] = sel[j];
			sel[j] = tempvar;
		}
	}

}


/*****************************************************************************
* @name print
* @brief prints the array all at once to the screen using stringstream
* @param a pointer to the head of the array, and how long the array is
* @retval none
*****************************************************************************/
template<class T>
void Selection<T>::print(T* sel, int n)
{
	//using stringstream makes the function more efficient
	stringstream allatonce;
	
	//combines whole array into one string then prints the string
	for (int i = 0; i < n; i++) {
		allatonce << sel[i] << " ";
	}
	cout << allatonce.str();
}