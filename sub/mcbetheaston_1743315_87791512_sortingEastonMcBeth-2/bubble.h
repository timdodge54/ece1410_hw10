#include <sstream>
#include <iostream>
using namespace std;

//bubblesort class
template <class T>
class Bubble
{
public:
	//rubric functions
	Bubble(T*, int);
	void print(T*, int);
};


/*****************************************************************************
* @name Bubble
* @brief constructor that sorts a given array using the bubblesort algorithm
* @param a pointer to the head of the array, and how long the array is
* @retval none
*****************************************************************************/
template<class T>
Bubble<T>::Bubble(T* bub, int n)
{
	int swapping = 1;
	int swapped;
	
	//temporary variable to be able to swap elements of array
	T tempvar;
	
	//given algorithm for bubble sort
	while (swapping == 1) {
		swapped = 0;
		for (int i = 1; i < n; i++) {
			if (bub[i - 1] > bub[i]) {
				
				//this swaps them
				tempvar = bub[i];
				bub[i] = bub[i - 1];
				bub[i - 1] = tempvar;
				
				swapped = 1;
			}
		}
		if (swapped == 0) {
			swapping = 0;
		}
	}

}

/*****************************************************************************
* @name print
* @brief prints the array all at once to the screen using stringstream
* @param a pointer to the head of the array, and how long the array is
* @retval none
*****************************************************************************/
template<class T>
void Bubble<T>::print(T* bub, int n)
{
	//using stringstream makes the function more efficient
	stringstream allatonce;
	
	//combines whole array into one string then prints the string
	for (int i = 0; i < n; i++) {
		allatonce << bub[i] << " ";
	}
	cout << allatonce.str();
}