#include <iostream>
using namespace std;

//insertion sort class
template <class T>
class Insertion
{
public:
	//rubric functions
	Insertion(T*, int);
	void print(T*, int);
};


/*****************************************************************************
* @name Insertino
* @brief constructor that sorts a given array using insertion sort algorithm
* @param a pointer to the head of the array, and how long the array is
* @retval none
*****************************************************************************/
template<class T>
Insertion<T>::Insertion(T* ins, int n)
{
	T tempvar;
	int i = 1, j;
	while (i < n) {
		j = i;
		while (j > 0 && ins[j - 1] > ins[j]) {
			tempvar = ins[j];
			ins[j] = ins[j - 1];
			ins[j - 1] = tempvar;
			j -= 1;
		}
		i += 1;
	}
}


/*****************************************************************************
* @name print
* @brief prints the array all at once to the screen using stringstream
* @param a pointer to the head of the array, and how long the array is
* @retval none
*****************************************************************************/
template<class T>
void Insertion<T>::print(T* ins, int n)
{
	//using stringstream makes the function more efficient
	stringstream allatonce;
	
	//combines whole array into one string then prints the string
	for (int i = 0; i < n; i++) {
		allatonce << ins[i] << " ";
	}
	cout << allatonce.str();
}