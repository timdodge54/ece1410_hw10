#include <iostream>
using namespace std;

//quicksort class
template <class T>
class Quick
{
public:
	//rubric functions
    Quick(T*, int);
    void print(T*, int);
};


/*****************************************************************************
* @name Quick
* @brief constructor that sorts a given array using the quick sort algorithm
* @param a pointer to the head of the array, and how long the array is
* @retval none
*****************************************************************************/
template<class T>
Quick<T>::Quick(T* quicksort, int n)
{
    if (n <= 1) {
        return;
    }
    T pivot = quicksort[n - 1];
    int i = -1;
    for (int j = 0; j < n - 1; j++) {
        if (quicksort[j] <= pivot) {
            i++;
            T temp = quicksort[i];
            quicksort[i] = quicksort[j];
            quicksort[j] = temp;
        }
    }
    T temp = quicksort[i + 1];
    quicksort[i + 1] = quicksort[n - 1];
    quicksort[n - 1] = temp;
    Quick(quicksort, i + 1);
    Quick(quicksort + i + 2, n - i - 2);
}


/*****************************************************************************
* @name print
* @brief prints the array all at once to the screen using stringstream
* @param a pointer to the head of the array, and how long the array is
* @retval none
*****************************************************************************/
template<class T>
void Quick<T>::print(T* quicksort, int n)
{
	//using stringstream makes the function more efficient
    stringstream allatonce;
	
	//combines whole array into one string then prints the string
    for (int i = 0; i < n; i++) {
        allatonce << quicksort[i] << " ";
    }
    cout << allatonce.str();
}

