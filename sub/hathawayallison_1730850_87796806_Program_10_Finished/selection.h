#include <iostream>
#include <string>
#include <algorithm>

#ifndef SELECTION_H
#define SELECTION_H

using namespace std;

/*****************************************************************************
* @name Selection
* @brief Uses selection sort algorithm to sort through array
* @param A, Z
* @retval none
*****************************************************************************/
template <class T>
class Selection
{
public:
	Selection(T *A, int Z)
	{
		for (int j = 0; j < Z - 1; j++)
		//loop
		{
			int iMin = j;
			//iMin = j
			for (int i = j + 1; i < Z; i++)
			//loop through array
			{
				if (A[i] < A[iMin])
				//if A[i] < A[iMin]
				{
					iMin = i;
					//iMin = i
				}
			}
			
			if (iMin != j)
			//if iMin != j
			{
			swap(A[j], A[iMin]);
			//swap values
			}
		}
	};
/*****************************************************************************
* @name print
* @brief prints elements of array
* @param A, Z
* @retval none
*****************************************************************************/
	void print(T *A, int Z)
	{
		int i;
		for (i = 1; i < Z - 1; i++)
		{
			cout << A[i] << " ";
		}
		//print values to screen
	};
};

#endif