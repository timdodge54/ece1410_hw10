#include <iostream>
#include <string>
#include <algorithm>

#ifndef BUBBLE_H
#define BUBBLE_H

using namespace std;

/*****************************************************************************
* @name Bubble
* @brief Uses bubble sort algorithm to sort through array
* @param A, Z
* @retval none
*****************************************************************************/
template <class T>
class Bubble
{
public:
	Bubble(T *A, int Z) 
	{
		int n, swapped = true, i;
		T temp;
		while (swapped == true)
		//while swapped is true
		{
			swapped = false;
			//swapped is false
			for (i = 1; i < Z - 1; i++)
			//loop through array
			{
				if (A[i - 1] > A[i])
				//if A[i - 1] > A[i]
				{
					temp = A[i - 1];
					A[i - 1] = A[i];
					A[i] = temp;
					//swap places
					swapped = true;
					//swapped is true
				}
			}
			Z = Z - 1;
			//decrement Z
		}
	};
/*****************************************************************************
* @name print
* @brief prints elements of array
* @param A, Z
* @retval none
*****************************************************************************/
	void print(T *A, int Z)
	{
		int i;
		for (i = 1; i < Z - 1; i++)
		{
			cout << A[i] << " ";
		}
		//print values to screen
	};
};

#endif