#include <iostream>
#include <string>
#include <algorithm>

#ifndef INSERTION_H
#define INSERTION_H

using namespace std;

/*****************************************************************************
* @name Insertion
* @brief Uses insertion sort algorithm to sort through array
* @param A, Z
* @retval none
*****************************************************************************/
template <class T>
class Insertion
{
public:
	Insertion(T *A, int Z)
	{
		int i, j;
		T temp;
		i = 0;
		j = 0;
		while (i < Z)
		//while i < Z
		{
			j = i;
			//j = i
			while (j > 0 && A[j - 1] > A[j])
			//while j > 0 and A[j - 1] > A[j]
			{
				temp = A[j - 1];
				A[j - 1] = A[j];
				A[j] = temp;
				//swap values
				j--;
				//decrement j
			}
			i++;
			//increment i
		}
	};
/*****************************************************************************
* @name print
* @brief prints elements of array
* @param A, Z
* @retval none
*****************************************************************************/
	void print(T *A, int Z)
	{
		int i;
		for (i = 1; i < Z - 1; i++)
		{
			cout << A[i] << " ";
		}
		//print values to screen
	};
};

#endif