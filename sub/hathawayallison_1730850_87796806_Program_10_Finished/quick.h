#include <iostream>
#include <string>
#include <algorithm>
#include <cmath>

#ifndef QUICK_H
#define QUICK_H

using namespace std;


/*****************************************************************************
* @name Quick
* @brief Uses quick sort algorithm to sort through array
* @param A, Z
* @retval none
*****************************************************************************/
template <class T>
class Quick
{
public:
	Quick(T *A, int Z)
	{
		quickSort(A, 0, Z - 1);
		//call quick sort function
	};
	
/*****************************************************************************
* @name quickSort
* @brief Uses quick sort algorithm to sort through array
* @param A, lo, hi
* @retval none
*****************************************************************************/
	void quickSort(T *A, int lo, int hi)
	{
		if (lo < hi)
		//if lo < hi
		{
			int pi = partition(A, lo, hi);
			//pi = partition(A, lo, hi)
			quickSort(A, lo, pi - 1);
			quickSort(A, pi + 1, hi);
			//recursively call quickSort functions
		}
	}
	
/*****************************************************************************
* @name partition
* @brief Uses quick sort algorithm to sort through array
* @param A[], lo, hi
* @retval (i + 1)
*****************************************************************************/
	int partition(int A[], int lo, int hi)
	{
		T pivot = A[hi];
		//pivot = A[hi]
		int i = (lo - 1);
		//i = (lo - 1)
		
		for (int j = lo; j <= hi - 1; j++)
		//loop
		{
			if (A[j] <= pivot)
			//if A[j] <= pivot
			{
				i++;
				//increment i
				swap(A[i], A[j]);
				//swap values
			}
		}
		swap(A[i + 1], A[hi]);
		//swap values
		return (i + 1);
		//return (i + 1)
	};
/*****************************************************************************
* @name print
* @brief prints elements of array
* @param A, Z
* @retval none
*****************************************************************************/
	void print(T *A, int Z)
	{
		int i;
		for (i = 1; i < Z - 1; i++)
		{
			cout << A[i] << " ";
		}
	};
	//print values to screen
};

#endif