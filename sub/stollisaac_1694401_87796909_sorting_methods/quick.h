#pragma once
#include <iostream>
#include <algorithm>

using namespace std;

template <class T>
class Quick {
public:
	Quick(T* list, int size);
	int partition(T* list, int lo, int hi);
	void sort(T* list, int lo, int hi);
	void print(T* list, int size);
};

/******************************************************************************
* @name 	Quick
* @brief 	initilized class and calls on sort
* @param 	T* list, int size
* @retval 	none
******************************************************************************/
template <class T>
Quick<T>::Quick(T* list, int size)
{
	sort(list, 0, size - 1);
}

/******************************************************************************
* @name 	sort
* @brief 	uses itself recursively to sort lust
* @param 	T* list, int lo, int hi
* @retval 	none
******************************************************************************/
template <class T>
void Quick<T>::sort(T* list, int lo, int hi)
{
	if (lo < hi)
	{
		int p = partition(list, lo, hi);
		sort(list, lo, p - 1);
		sort(list, p + 1, hi);
	}
}

/******************************************************************************
* @name 	partition
* @brief 	creates pivot to help sort
* @param 	T* list, int lo, int hi
* @retval 	int
******************************************************************************/
template <class T>
int Quick<T>::partition(T* list, int lo, int hi)
{
	T pivot = list[hi];
	int i = lo - 1;
	for (int j = lo; j < hi; j++)
	{
		if (list[j] < pivot)
		{
			i = i + 1;
			swap(list[i], list[j]);
		}
	}
	swap(list[i + 1], list[hi]);
	return i + 1;
}

/******************************************************************************
* @name 	print
* @brief 	prints the sorted list
* @param 	T* list, int size
* @retval 	none
******************************************************************************/
template <class T>
void Quick<T>::print(T* list, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << list[i] << " ";
	}
}