#pragma once
#include <iostream>
#include <algorithm>

using namespace std;

template <class T>
class Selection {
public:
	Selection(T* list, int size);
	void print(T* list, int size);
};

/******************************************************************************
* @name 	selection
* @brief 	sorts list by selection method
* @param 	T* list, int size
* @retval 	none
******************************************************************************/
template <class T>
Selection<T>::Selection(T* list, int size)
{
	//slection algorithim
	for (int j = 0; j < size - 1; j++)
	{
		T iMin = j;
		for (int i = j + 1; i < size; i++)
		{
			if (list[i] < list[iMin])
			{
				iMin = i;
			}
		}
		if (iMin != j)
		{
			swap(list[j], list[iMin]);
		}
	}
}

/******************************************************************************
* @name 	print
* @brief 	prints the sorted list
* @param 	T* list, int size
* @retval 	none
******************************************************************************/
template <class T>
void Selection<T>::print(T* list, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << list[i] << " ";
	}
}