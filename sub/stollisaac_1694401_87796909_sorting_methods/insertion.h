#pragma once
#include <iostream>
#include <algorithm>

using namespace std;

template <class T>
class Insertion {
public:
	Insertion(T* list, int size);
	void print(T* list, int size);
};

/******************************************************************************
* @name 	Insertion
* @brief 	sorts list by insertion method
* @param 	T* list, int size
* @retval 	none
******************************************************************************/
template <class T>
Insertion<T>::Insertion(T* list, int size)
{
	//insertion algorthim
	int i = 1;
	while (i < size)
	{
		int j = i;
		while (j > 0 && (list[j - 1] > list[j]))
		{
			swap(list[j], list[j - 1]);
			j = j - 1;
		}
		i = i + 1;
	}
}

/******************************************************************************
* @name 	print
* @brief 	prints the sorted list
* @param 	T* list, int size
* @retval 	none
******************************************************************************/
template <class T>
void Insertion<T>::print(T* list, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << list[i] << " ";
	}
}