#pragma once
#include <iostream>
#include <algorithm>

using namespace std;

template <class T>
class Bubble {
public:
	Bubble (T * list, int size);
	void print(T * list, int size);
};

/******************************************************************************
* @name 	Bubble
* @brief 	sorts list by bubble method
* @param 	T* list, int size
* @retval 	none
******************************************************************************/
template <class T>
Bubble<T>::Bubble(T* list, int size)
{
	int swapped = true;
	int i;
	
	//loop while true
	while (swapped == true)
	{
		swapped = false;
		//bubble algorithim
		for (i = 1; i <= size - 1; i++)
		{
			if (list[i - 1] > list[i])
			{
				swap(list[i - 1], list[i]);
				swapped = true;
			}
		}
		size = size - 1;
	}
}

/******************************************************************************
* @name 	print
* @brief 	prints the sorted list
* @param 	T* list, int size
* @retval 	none
******************************************************************************/
template <class T>
void Bubble<T>::print(T* list, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << list[i] << " ";
	}
}