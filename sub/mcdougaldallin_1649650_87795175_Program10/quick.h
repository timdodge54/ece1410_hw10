#ifndef QUICK_H
#define QUICK_H

using namespace std;

template <class T>
class Quick {
/*****************************************************************************
* @name quicksort
* @brief Sorts an array using Quick sort algorithm.
* @param T* A, T lo, T hi
* @retval none
*****************************************************************************/
	void quicksort(T* A, T lo, T hi) {
		// If lo < hi
		if (lo < hi) {
			// partion using hi as pivot
			int p = partition(A, lo, hi);
			// quicksort partition below p
			quicksort(A, lo, p - 1);
			// quicksort partition above p
			quicksort(A, p + 1, hi);
		}
	}
/*****************************************************************************
* @name partition
* @brief Sorts a partition of the quick sort algorithm.
* @param T* A, T lo, T hi
* @retval int p
*****************************************************************************/
	int partition(T* A, T lo, T hi) {
		// pivot using value of hi
		T pivot = A[hi];
		int i = lo - 1;
		int j;
		// Loop through partition
		for (j = lo; j < hi; j++) {
			// if current element is less than pivot
			if (A[j] < pivot) {
				i++;
				// swap with i
				swap(A[i], A[j]);
			}
		}
		// Insert pivot value into pivot point
		swap(A[i + 1], A[hi]);
		return i + 1;
	}
public:
/*****************************************************************************
* @name Quick
* @brief Constructer for Quick. Sorts an array using Quick sort algorithm.
* @param T* head, int n
* @retval none
*****************************************************************************/
	Quick(T* head, int n) {
		quicksort(head, 0, n - 1);
	}
/*****************************************************************************
* @name print
* @brief Prints the sorted array seperated by 0
* @param T* head, int n
* @retval none
*****************************************************************************/
	void print(T* head, int n) {
		int i;
		// Loop through array
		for (i = 0; i < n; i++) {
			// Print Element
			cout << *(head + i) << " ";
		}
	}
};
#endif