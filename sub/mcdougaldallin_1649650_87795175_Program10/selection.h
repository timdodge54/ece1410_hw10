#ifndef SELECTION_H
#define SELECTION_H

using namespace std;

template <class T>
class Selection {
public:
/*****************************************************************************
* @name Selection
* @brief Constructer for Selection. Sorts an array using Selection
sort algorithm.
* @param T* head, int n
* @retval none
*****************************************************************************/
	Selection(T* head, int n) {
		int i, j, iMin;
		
		// Loop through array
		for (j = 0; j < n - 1; j++) {
			iMin = j;
			// Loop through unsorted portion of array
			for (i = j + 1; i < n; i++) {
				// Find min value in unsorted portion
				if (head[i] < head[iMin]) {
					iMin = i;
				}
			}
			// Swap min value with next element in array
			if (iMin != j) {
				swap(head[j], head[iMin]);
			}
		}
		

	}
/*****************************************************************************
* @name print
* @brief Prints the sorted array seperated by 0
* @param T* head, int n
* @retval none
*****************************************************************************/
	void print(T* head, int n) {
		int i;
		// Loop through array
		for (i = 0; i < n; i++) {
			// Print Element
			cout << *(head + i) << " ";
		}
	}
};

#endif