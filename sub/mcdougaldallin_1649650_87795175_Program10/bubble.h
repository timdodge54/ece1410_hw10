#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>

using namespace std;

template <class T>
class Bubble {
public:
/*****************************************************************************
* @name Bubble
* @brief Constructer for Bubble. Sorts an array using Bubble sort algorithm.
* @param T* head, int n
* @retval none
*****************************************************************************/
	Bubble(T* head, int n) {
		bool swapped = true;
		int i;

		// Loop until gone through array without swap
		while (swapped) {
			swapped = false;
			// Loop through array
			for (i = 1; i < n; i++) {
				// Check if current element is greater than previous
				if (head[i-1] > head[i]) {
					// Swap current and previous elements
					swap(head[i], head[i-1]);
					swapped = true;
				}
			}
		}
	}
/*****************************************************************************
* @name print
* @brief Prints the sorted array seperated by 0
* @param T*, int n
* @retval none
*****************************************************************************/
	void print(T* head, int n) {
		int i;
		// Loop through array
		for (i = 0; i < n; i++) {
			// Print Element
			cout << *(head + i) << " ";
		}
	}
};
#endif