#ifndef INSERTION_H
#define INSERTION_H

using namespace std;

template <class T>
class Insertion {
public:
/*****************************************************************************
* @name Insertion
* @brief Constructer for Insertion. Sorts an array using Insertion
sort algorithm.
* @param T* head, int n
* @retval none
*****************************************************************************/
	Insertion(T* head, int n) {
		int i = 1, j;
		while (i < n) {
			j = i;
			while ((j > 0) && (head[j - 1] > head[j])) {
				swap(head[j], head[j - 1]);
				j--;
			}
			i++;
		}
	}
/*****************************************************************************
* @name print
* @brief Prints the sorted array seperated by 0.
* @param T* head, int n
* @retval none
*****************************************************************************/
	void print(T* head, int n) {
		int i;
		// Loop through array
		for (i = 0; i < n; i++) {
			// Print Element
			cout << *(head + i) << " ";
		}
	}
};
#endif