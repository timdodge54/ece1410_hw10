#ifndef INSERTION_H
#define INSERTION_H
#include <iostream>
using namespace std;

template <class T>
class Insertion {
	private:
	public:
		Insertion(T *A, int N){
			int i = 1, j;
			while (i < N){
				j = i;
				while (j > 0 && A[j-1] > A[j]){
					swap(A[j], A[j-1]);
					j = j-1;
				}
				i++;
			}
		}
		void print(T *A, int N){
			int i;
			for (i = 0; i < N; i++){		// walk through array
				cout << A[i] << " ";		// print to screen
			}
		}
};



/*******************************************************************************
@name		insertion
@brief		Constructor for class insertion
@param		pointer to Type T, int size array
@retval 	none
*******************************************************************************/


/*******************************************************************************
@name		print
@brief		print for class insertion
@param		pointer to Type T, int size array
@retval 	none
*******************************************************************************/


#endif