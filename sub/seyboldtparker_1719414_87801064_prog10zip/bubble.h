#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>
using namespace std;

template <class T>
class Bubble {
	private:
	public:
		Bubble(T *A, int N) {
			int i, swapped;	
			do{
				swapped = false;
				for (i = 1; i < N; i++){ 	// for i = 1; i = n-1; i++
					if (A[i-1] > A[i]){		// if first greater than next
						swap(A[i], A[i-1]);	// swap
						swapped = true;		// swapped = true
					}						//endif
				}							//endfor	//endloop
			}
			while(swapped == true);		//do while loop - do 
		}
		
		void print(T *A, int N){
			int i;
			for (i = 0; i < N; i++){
				cout << A[i] << " ";
			}
		}
		
};


/*******************************************************************************
@name		bubble
@brief		Constructor for class bubble
@param		pointer to Type T, int size array
@retval 	none
*******************************************************************************/


/*******************************************************************************
@name		print
@brief		print for class bubble
@param		pointer to Type T, int size array
@retval 	none
*******************************************************************************/

#endif