#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>
using namespace std;

template <class T>
class Selection {
	private:
	public:
		Selection(T *A, int N){
			int i, j, iMin;					// declare variables
			for(j = 0; j < N-1; j++){		// for loop
				iMin = j;
				for(i = j+1; i < N; i++){	// nested for loop
					if (A[i] < A[iMin]){	// if new variable is greater, set 
						iMin = i;
					}
				}
				if(iMin != j){				// if new variable is less, swap
					swap(A[j], A[iMin]);
				}
			}
		}
		void print(T *A, int N){
			int i;
			for (i = 0; i < N; i++){		// walk through array
				cout << A[i] << " ";		// print to screen
			}
		}
};


/*******************************************************************************
@name		selection
@brief		Constructor for class selection
@param		pointer to Type T, int size array
@retval 	none
*******************************************************************************/


/*******************************************************************************
@name		print
@brief		print for class selection
@param		pointer to Type T, int size array
@retval 	none
*******************************************************************************/



#endif