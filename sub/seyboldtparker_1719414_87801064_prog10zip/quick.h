#ifndef QUICK_H
#define QUICK_H
#include <iostream>
using namespace std;

template <class T>
class Quick {
	private:
	public:
		Quick(T *A, int N) {
        quickSort(A, 0, N - 1);
    }
		
		int partition(T *A, int lo, int h) {
        int pivot_index = lo + (h - lo) / 2;
        T pivot = A[pivot_index];
        int i = lo;
        int j = h;
        while (i <= j) {
            while (A[i] < pivot) {
                i++;
            }
            while (A[j] > pivot) {
                j--;
            }
            if (i <= j) {
                swap(A[i], A[j]);
                i++;
                j--;
            }
        }
        return i;
    }
		
		void print(T *A, int N){
			int i;
			for (i = 0; i < N; i++){		// walk through array
				cout << A[i] << " ";		// print to screen
			}
		}
		
		void quickSort(T *A, int low, int hi) {
        if (low < hi) {
            int p = partition(A, low, hi);
            quickSort(A, low, p - 1);
            quickSort(A, p + 1, hi);
        }
    }
};



/*******************************************************************************
@name		quick
@brief		Constructor for class quick
@param		pointer to Type T, int size array
@retval 	none
*******************************************************************************/


/*******************************************************************************
@name		print
@brief		print for class quick
@param		pointer to Type T, int size array
@retval 	none
*******************************************************************************/


#endif