#ifndef QUICK_H
#define QUICK_H

/*********************************************************************
* @name Quick
* @brief performs a quick sort algorithm.
* @param none
* @retval none
*********************************************************************/

#include <iostream>

using namespace std;

template <class T>
class Quick {
public:
    Quick(T* arr, int size) {
        quickSort(arr, 0, size - 1);
    }

    void print(T* arr, int size) {
        for (int i = 0; i < size; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }

private:
    void quickSort(T* arr, int low, int high) {
        if (low < high) {
            int pi = partition(arr, low, high);
            quickSort(arr, low, pi - 1);
            quickSort(arr, pi + 1, high);
        }
    }

    int partition(T* arr, int low, int high) {
        T pivot = arr[high];
        int i = low - 1;

        for (int j = low; j < high; j++) {
            if (arr[j] < pivot) {
                i++;
                swap(arr[i], arr[j]);
            }
        }

        swap(arr[i + 1], arr[high]);
        return i + 1;
    }
};

#endif

