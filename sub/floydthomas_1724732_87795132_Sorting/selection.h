#ifndef SELECTION_H
#define SELECTION_H

/*********************************************************************
* @name Selection
* @brief performs a selection sort algorithm.
* @param none
* @retval none
*********************************************************************/

#include <iostream>
using namespace std;

template<class T>
class Selection {
public:
    Selection(T* arr, int n) {
        for (int i = 0; i < n - 1; i++) {
            int min_idx = i;
            for (int j = i + 1; j < n; j++) {
                if (arr[j] < arr[min_idx]) {
                    min_idx = j;
                }
            }
            T temp = arr[i];
            arr[i] = arr[min_idx];
            arr[min_idx] = temp;
        }
    }

    void print(T* arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }
};

#endif