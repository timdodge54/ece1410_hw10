#ifndef BUBBLE_H
#define BUBBLE_H

/*********************************************************************
* @name Bubble
* @brief performs a bubble sort algorithm.
* @param none
* @retval none
*********************************************************************/

#include <iostream>
using namespace std;

template<class T>
class Bubble {
public:
    Bubble(T* arr, int n) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    T temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    void print(T* arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }
};
#endif