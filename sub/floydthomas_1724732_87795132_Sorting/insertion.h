#ifndef INSERTION_H
#define INSERTION_H

/*********************************************************************
* @name Insertion
* @brief performs an insertion sort algorithm.
* @param none
* @retval none
*********************************************************************/

#include <iostream>
using namespace std;

template<class T>
class Insertion {
public:
    Insertion(T* arr, int n) {
        for (int i = 1; i < n; i++) {
            T key = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = key;
        }
    }

    void print(T* arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }
};

#endif