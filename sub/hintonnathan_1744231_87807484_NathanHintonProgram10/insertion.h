
#pragma once
#ifndef INSERTION_H
#define INSERTION_H

//Nathan Hinton
//Simple insertion sorting

#include <iostream>

template <typename T> class Insertion {
    public:
        Insertion(T*, int input_list_length);
        void print(T*, int input_list_length);
};

template <typename T> Insertion<T>::Insertion (T* input_list, int input_list_length) {
    int i = 1;
    while (i < input_list_length) {
        T x = input_list[i];
        int j = i-1;
        while (j >=0 & input_list[j] > x) {
            input_list[j+1] = input_list[j];
            j --;
        }
        input_list[j+1] = x;
        i ++;
    }
}

template <typename T> void Insertion<T>::print(T* input_list, int input_list_length) {
    for (int ii = 0; ii < input_list_length; ii++) {
        std::cout << input_list[ii] << " ";
    }
}

#endif