#pragma once
#ifndef SELECTION_H
#define SELECTION_H

//Nathan Hinton
//Simple selection sorting

#include <iostream>

template <typename T> class Selection {
    public:
        Selection(T*, int input_list_length);
        void print(T*, int input_list_length);
};

template <typename T> Selection<T>::Selection (T* input_list, int input_list_length) {
    for (int i = 0; i < input_list_length-1; i++) {
        int jMin = i;
        for (int j = i+1; j < input_list_length; j++) {
            if (input_list[j] < input_list[jMin])
            {
                jMin = j;
            }
        }
        if (jMin != i) {
            T tmp = input_list[i];
            input_list[i] = input_list[jMin];
            input_list[jMin] = tmp;
            // swap(a[i], a[jMin]);
        }
    }
}

template <typename T> void Selection<T>::print(T* input_list, int input_list_length) {
    for (int ii = 0; ii < input_list_length; ii++) {
        std::cout << input_list[ii] << " ";
    }
}

#endif