#pragma once
#ifndef BUBBLE_H
#define BUBBLE_H

//Nathan Hinton
//Simple bubble sorting

#include <iostream>

template <typename T> class Bubble {
    public:
        Bubble(T*, int input_list_length);
        void print(T*, int input_list_length);
};

template <typename T> Bubble<T>::Bubble (T* input_list, int input_list_length) {
    bool swapped = false;
    int loop_count = 0;
    do {
        swapped = false;
        for (int ii = 1; ii < input_list_length-loop_count; ii ++) {
            if (input_list[ii-1] > input_list[ii]) {
                T tmp = input_list[ii-1];
                input_list[ii-1] = input_list[ii];
                input_list[ii] = tmp;
                swapped = true;
            }
        }
        loop_count ++;
    }
    while (swapped == true);
}

template <typename T> void Bubble<T>::print(T* input_list, int input_list_length) {
    for (int ii = 0; ii < input_list_length; ii++) {
        std::cout << input_list[ii] << " ";
    }
}

#endif