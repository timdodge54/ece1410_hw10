#pragma once
#ifndef QUICK_H
#define QUICK_H

//Nathan Hinton
//Simple quick sort sorting

#include <iostream>

template <typename T> class Quick {
    public:
        Quick(T*, int input_list_length);
        void quicksort(T* input_list, int low, int high);
        int partition(T* input_list, int low, int high);
        void print(T*, int input_list_length);
};

template <typename T> Quick<T>::Quick (T* input_list, int input_list_length) {
    quicksort(input_list, 0, input_list_length-1);
}

template <typename T> void Quick<T>::quicksort (T* input_list, int low, int high) {
    if (low < high) {
        int p = partition(input_list, low, high);
        // print(input_list, 10);
        quicksort(input_list, low, p-1);
        quicksort(input_list, p+1, high);
    }
}
template <typename T> int Quick<T>::partition (T* input_list, int low, int high) {
    T pivot = input_list[high];
    std::cout << "Pivot: " << pivot << std::endl;
    int i = low-1;
    for (int jj = low; jj != high; jj++) {
        if (input_list[jj] <= pivot) {
            i ++;
            T tmp = input_list[i];
            input_list[i] = input_list[jj];
            input_list[jj] = tmp; 
        }
        // print(input_list, 10);
    }
    i ++;
    T tmp2 = input_list[i];
    input_list[i] = input_list[high];
    input_list[high] = tmp2; 
    return i;
}

template <typename T> void Quick<T>::print(T* input_list, int input_list_length) {
    for (int ii = 0; ii < input_list_length; ii++) {
        std::cout << input_list[ii] << " ";
    }
    std::cout << std::endl;
}

#endif