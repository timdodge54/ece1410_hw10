#pragma once
#include <iostream>
using namespace std;

template <typename T>
class Bubble {
public:
	Bubble(T*, int);
	void print(T*, int);
};


/*****************************************************************************
* @name     Bubble
* @brief    Sorts array using bubble sort techique
* @param	T* listIn, int size
* @retval	none
*****************************************************************************/
template <typename T>
Bubble<T>::Bubble(T* listIn, int size) {
    T* const listHead = listIn;
    T* const listNeck = ++listIn;
	T* curIndex = listHead;
    T* prevIndex = curIndex;
    T temp;
    bool swapped = false;
    int i;

    //Bubble sort
    do {
        swapped = false;
        //Start at beginning each time, assume we dont need to check the end
        for (i = 0, curIndex = listNeck, prevIndex = listHead;
            i < size - 1; i++, prevIndex = curIndex, curIndex++) {
            //If n-1 > n, swap
            if (*prevIndex > *curIndex) {
                temp = *curIndex;
                *curIndex = *prevIndex;
                *prevIndex = temp;
                swapped = true;
            }
        }

        size--;
    } while (swapped == true);
    return;
}

/*****************************************************************************
* @name     print
* @brief    Prints contents of array given by "list"
* @param	T* list, int size
* @retval	none
*****************************************************************************/
template <typename T>
void Bubble<T>::print(T* list, int size) {
    int i;
    T* ptr = list;
    for (i = 0; i < size; i++, ptr++) {
        cout << *ptr << " ";
    }
    return;
}