#pragma once
#include <iostream>
using namespace std;

template <typename T>
class Quick {
public:
	Quick(T*, int);
	void print(T*, int);
};

/*****************************************************************************
* @name     Quick
* @brief    DID NOT FINISH, COPIED FROM INSERTION. Sorts list given by listIn
* @param	T* listIn, int size
* @retval	none
*****************************************************************************/
template <typename T> 
Quick<T>::Quick(T* listIn, int size) {
    T* completed = listIn; //Keep track of progress
    int dist = 0;
    T* listNeck = listIn; //Index right after beginning
    listNeck++;
    T* cur = NULL; //pointer to sift through built list
    T* prev = NULL; //Follows cur
    T plucked; //Number we pulled out of stack to compare
    bool placed = false;

    do {
        placed = false;
        plucked = *completed;
        cur = completed;
        for (; placed == false;) {
            //Case where we reached the beginning
            if (cur == listIn) {
                *cur = plucked;
                placed = true;
            }
            //Scooch
            prev = cur;
            cur--;
            if (plucked < *cur) {
                *prev = *cur;
            }
            else {
                *prev = plucked;
                placed = true;
            }
        }

        dist++;
        completed++;
    } while (dist != size);

    return;
}


/*****************************************************************************
* @name     print
* @brief    Prints contents of array given by "list"
* @param	T* list, int size
* @retval	none
*****************************************************************************/
template <typename T>
void Quick<T>::print(T* list, int size) {
    int i;
    T* ptr = list;
    for (i = 0; i < size; i++, ptr++) {
        cout << *ptr << " ";
    }
    return;
}