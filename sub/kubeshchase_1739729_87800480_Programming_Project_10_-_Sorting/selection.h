#pragma once
#include <iostream>
using namespace std;

template <typename T>
class Selection {
public:
	Selection(T*, int);
	void print(T*, int);
};

/*****************************************************************************
* @name     Selection
* @brief    Sorts array using selection sort techique
* @param	T* listIn, int size
* @retval	none
*****************************************************************************/
template <typename T>
Selection<T>::Selection(T* listIn, int size) {
    T* cur = listIn;
    T* unsortHead = listIn;
    T* marker = NULL;
    T temp = (T)0;
    int i = 0;

    do {
        
        //Loop through list, beginning at the first unsorted. Mark each time we find one lower
        for (cur = unsortHead, marker = NULL, i = 0;
            i < size; cur++, i++) {

            //If we find a smaller one, mark it
            //No marker
            if ((*cur < *unsortHead) && (marker == NULL)) {
                marker = cur;
            }
            //Smaller than marker
            else if ((marker != NULL) && (*cur < *marker)) {
                marker = cur;
            }

        }
        //If a smaller number than the Head is found, swap
        if (marker != NULL) {
            
            temp = *marker;
            *marker = *unsortHead;
            *unsortHead = temp;
        }
        --size;
        unsortHead++;

    //End the sort if weve made it to the end, or if we dont find anything
        //to swap
    } while ((size > 0) || (marker != NULL));

}

/*****************************************************************************
* @name     print
* @brief    Prints contents of array given by "list"
* @param	T* list, int size
* @retval	none
*****************************************************************************/
template <typename T>
void Selection<T>::print(T* list, int size) {
    int i;
    T* ptr = list;
    for (i = 0; i < size; i++, ptr++) {
        cout << *ptr << " ";
    }
    return;
}