#include <iostream>
using namespace std;

template <class T>
class Quick {
	public:
		Quick(T *, int);
		void print(T * , int);
		void quicksort(T *, int, int);
		int partition(T *, int, int);
		
};

/******************************************************************************
@name	Quick
@breif	Default constructor for class Quick - sorts the inputted array
		using the quick sort method
@param	T * input, int length
@retval	none
******************************************************************************/
template <class T>
Quick<T>::Quick(T * input, int length) {
	//calls quicksort function from 0 to N-1
	quicksort(input, 0, length-1);
	
}

/******************************************************************************
@name	print
@breif	prints every element in inputted array
@param	T * input, int length
@retval	none
******************************************************************************/
template <class T>
void Quick<T>::print(T * input, int length) {
	int i;
	//loop through every element in array
	for (i=0;i<length;i++) {
		//print element
		cout << input[i] << " ";
	}
}

/******************************************************************************
@name	quicksort
@breif	Function that assigns a partition by calling the function partition,
		then recursively calls quicksort from 0 to p-1, and p+1 to N, which
		sorts the entire array piece by piece
@param	T * input, int lo, int hi
@retval	none
******************************************************************************/
template <class T>
void Quick<T>::quicksort(T * input, int lo, int hi) {
	if (lo >= hi || lo < 0) return;
	
	int p = partition(input, lo, hi);
	
	quicksort(input, lo, p-1);
	quicksort(input, p+1, hi);
	
	
}

/******************************************************************************
@name	partition
@breif	Assigns a 'hole' in the given range, and makes sure that all values
		smaller than that element are sorted to the left, and all values
		greater are sorted to the right
@param	T * input, int lo, int hi
@retval	int
******************************************************************************/
template <class T>
int Quick<T>::partition(T * input, int lo, int hi) {
	T temp;
	//assign hole to the rightmost value
	T pivot = input[hi];
	int i = lo-1;
	int j;
	//iterate for every value in range
	for (j=lo;j<hi;j++) {
		//if element is less than pivot element
		if (input[j] <= pivot) {
			i++;
			//swap elements
			temp = input[i];
			input[i] = input[j];
			input[j] = temp;
		}
	}
	i++;
	//place original value back in hole
	temp = input[i];
	input[i] = input[hi];
	input[hi] = temp;
	//returns location of sorted pivot element
	return i;
}