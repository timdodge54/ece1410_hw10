#include <iostream>
using namespace std;

template <class T>
class Bubble {
	public:
		Bubble(T *, int);
		void print(T *, int);
		
};

/******************************************************************************
@name	Bubble
@breif	Default constructor for class Bubble - sorts the inputted array using
		the bubble sort method
@param	T * input, int length
@retval	none
******************************************************************************/
template <class T>
Bubble<T>::Bubble(T * input, int length) {
	
	T temp;
	int i;
	int swapped = 1;
	//repeat until there is a run through of the array with no swaps
	while (swapped) {
		swapped = 0;
		//increment through the array, starting at element 1
		for (i=1;i<length;i++) {
			//if the previous element in array is greater than the current
			if (input[i-1]>input[i]) {
				//swap the values
				temp = input[i];
				input[i] = input[i-1];
				input[i-1] = temp;
				//set swapped flag to 1
				swapped = 1;
			}
		}
	}
	
}

/******************************************************************************
@name	print
@breif	prints every element in inputted array
@param	T * input, int length
@retval	none
******************************************************************************/
template <class T>
void Bubble<T>::print(T * input, int length) {
	int i;
	//loop through every element in array
	for (i=0;i<length;i++) {
		//print element
		cout << input[i] << " ";
	}
}