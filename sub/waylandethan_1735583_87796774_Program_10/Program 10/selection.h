#include <iostream>
using namespace std;

template <class T>
class Selection {
	public:
		Selection(T *, int);
		void print(T *, int);
		
};


/******************************************************************************
@name	Selection
@breif	Default constructor for class Selection - sorts the inputted array
		using the selection sort method
@param	T * input, int length
@retval	none
******************************************************************************/
template <class T>
Selection<T>::Selection(T * input, int length) {
	
	T temp;
	int i, j, min;
	
	//iterate N times
	for (j=0;j<length-1;j++) {
		min = j;
		//find smallest value in array
		for (i=j+1;i<length;i++) {
			if (input[i] < input[min]) {
				min = i;
			}
		}
		//place smallest value at front of array
		if (min != j) {
			temp = input[j];
			input[j] = input[min];
			input[min] = temp;
		}
	}
	
}

/******************************************************************************
@name	print
@breif	prints every element in inputted array
@param	T * input, int length
@retval	none
******************************************************************************/
template <class T>
void Selection<T>::print(T * input, int length) {
	int i;
	//loop through every element in array
	for (i=0;i<length;i++) {
		//print element
		cout << input[i] << " ";
	}
}