#include <iostream>
using namespace std;

template <class T>
class Insertion {
	public:
		Insertion(T *, int);
		void print(T *, int);
		
};

/******************************************************************************
@name	Selection
@breif	Default constructor for class Insertion - sorts the inputted array
		using the insertion sort method
@param	T * input, int length
@retval	none
******************************************************************************/
template <class T>
Insertion<T>::Insertion(T * input, int length) {
	
	T temp;
	int i, j;
	
	i = 1;
	//iterate for each element of array - starting at the left
	while (i<length) {
		j = i;
		//move item in array to the left until
		//everything to the right is greater than 
		//the item
		while (j>0 && (input[j-1] > input[j])) {
			//swap item with the one on its left
			temp = input[j];
			input[j] = input[j-1];
			input[j-1] = temp;
			//move right
			j--;
		}
		i++;
	}
	
}

/******************************************************************************
@name	print
@breif	prints every element in inputted array
@param	T * input, int length
@retval	none
******************************************************************************/
template <class T>
void Insertion<T>::print(T * input, int length) {
	int i;
	//loop through every element in array
	for (i=0;i<length;i++) {
		//print element
		cout << input[i] << " ";
	}
}