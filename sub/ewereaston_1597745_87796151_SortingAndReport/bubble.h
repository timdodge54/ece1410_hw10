#pragma once
#include <iostream>

using namespace std;

template <class T>
class Bubble {
public:
	Bubble(T*, int);
	void print(T*, int);
};



/*************************************************************************
* @name Bubble
* @brief Constructor; sorts using the Bubble algorithm
* @param pointer to the head of an array, int specifying size of array
* @retval none
*************************************************************************/
template <class T>
Bubble<T>::Bubble(T *nums, int n)
{ 
	T temp; 
	int i, swapped;

	do
	{ 
		swapped = 0; 					//set swapped flag to zero
		for (i = 1; i <= (n - 1); i++)	//loop the array
		{
			if (nums[i - 1] > nums[i])	//compare side by side elements
			{
				temp = nums[i - 1];		//switch elemtents 
				nums[i - 1] = nums[i];
				nums[i] = temp;
				swapped++;				//flag swapped variable
			}
		}
	} while (swapped > 0);				//loop while swaps happen
}



/*************************************************************************
* @name print
* @brief prints the array
* @param pointer to the head of an array, int specifying size of array
* @retval none
*************************************************************************/
template <class T>
void Bubble<T>::print(T*nums, int n) 
{

	for (int i = 0; i < n; i++)	//loop the array
	{
		cout << nums[i] << " ";	//print each element
	}
}
