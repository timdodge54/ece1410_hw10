#pragma once
#include <iostream>

using namespace std;

template <class T>
class Selection {
public:
	Selection(T*, int);
	void print(T*, int);
};



/*************************************************************************
* @name Selection
* @brief Constructor; sorts using the selection algorithm
* @param pointer to the head of an array, int specifying size of array
* @retval none
*************************************************************************/
template <class T>
Selection<T>::Selection(T *nums, int n)
{
	int j, i, iMin;
	T temp; 

	for (j = 0; j < n - 1; j++) //loop through the array
	{
		iMin = j;					//set j to known minimum
		for (i = j + 1; i < n; i++)	//loop until number smaller than minimum
		{
			if (nums[i] < nums[iMin])
			{
				iMin = i;		//reset minimum
			}
		}
		if (iMin != j)		//swap elements
		{
			temp = nums[j];
			nums[j] = nums[iMin];
			nums[iMin] = temp;
		}
	}
}



/*************************************************************************
* @name print
* @brief prints the array
* @param pointer to the head of an array, int specifying size of array
* @retval none
*************************************************************************/
template <class T>
void Selection<T>::print(T* nums, int n)
{

	for (int i = 0; i < n; i++) //loop the array
	{
		cout << nums[i] << " "; //print each element
	}
}