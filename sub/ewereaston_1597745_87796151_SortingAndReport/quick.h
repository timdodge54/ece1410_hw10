#pragma once
#include <iostream>

using namespace std;

template <class T>
class Quick {
public:
	Quick(T*, int);
	void print(T*, int);
	void quicksort(T*, int, int);
	int partition(T*, int, int);
};

/*************************************************************************
* @name Quick
* @brief Constructor; sorts by calling the quicksort algorithm
* @param pointer to the head of an array, int specifying size of array
* @retval none
*************************************************************************/
template <class T>
Quick<T>::Quick(T* nums, int n)
{
	quicksort(nums, 0, n-1); //call quicksort algorithm
}



/*************************************************************************
* @name print
* @brief prints the array
* @param pointer to the head of an array, int specifying size of array
* @retval none
*************************************************************************/
template <class T>
void Quick<T>::print(T* nums, int n)
{

	for (int i = 0; i < n; i++) //loop array
	{
		cout << nums[i] << " "; //print each element
	}
}




/*************************************************************************
* @name quicksort
* @brief calls partition recursively
* @param pointer to the head of an array, index of first and last in array
* @retval none
*************************************************************************/
template <class T>
void Quick<T>::quicksort(T* A, int low, int hi)
{
	int p;
	
	if (low < hi) //check if low less than high
	{
		p = partition(A, low, hi); //call partition to find pivot
	
		quicksort(A, low, p - 1 ); //quick sort subarray
		quicksort(A, p + 1, hi);	//quick sort subarray
	}
}



/*************************************************************************
* @name partition
* @brief sorts subarrays around a pivot
* @param pointer to the head of a sub array, index of array first and last 
* @retval pivot
*************************************************************************/
template <class T>
int Quick<T>::partition(T* A, int low, int hi)
{
	T pivot, temp;
	int i, j;
	
	pivot = A[hi]; //set pivot to the high element
	i = low - 1;    
	for (j = low; j <= hi - 1; j++)//traverse array
	{
		if (A[j] < pivot) //if we find an number less than the pivot..
		{
			i = i + 1;
			temp = A[i]; //swap A[i] w/ A[j]
			A[i] = A[j];
			A[j] = temp;
		}
	      
	}
	    
	temp = A[i+1]; //swap A[i+1] with A[hi]
	A[i+1] = A[hi];
	A[hi] = temp;
			
	  return i + 1; //return i index plus 1;
}

