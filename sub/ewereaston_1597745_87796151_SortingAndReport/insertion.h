#pragma once
#include <iostream>

using namespace std;

template <class T>
class Insertion {
public:
	Insertion(T*, int);
	void print(T*, int);
};



/*************************************************************************
* @name Insertion
* @brief Constructor; sorts using the insertion algorithm
* @param pointer to the head of an array, int specifying size of array
* @retval none
*************************************************************************/
template <class T>
Insertion<T>::Insertion(T *nums, int n)
{
	int i = 1, j = 1;
	T temp;

	while (i < n) //loop the array
	{
		j = i;
		while ((j > 0) && nums[j - 1] > nums[j]) //compare elements
		{
			temp = nums[j];
			nums[j] = nums[j - 1]; //swap elements
			nums[j - 1] = temp;
			j--;
		}
		i++; //increment starting position in the array
	}
}



/*************************************************************************
* @name print
* @brief prints the array
* @param pointer to the head of an array, int specifying size of array
* @retval none
*************************************************************************/
template <class T>
void Insertion<T>::print(T* nums, int n)
{

	for (int i = 0; i < n; i++)	//loop the array
	{
		cout << nums[i] << " "; //print each element
	}
}
