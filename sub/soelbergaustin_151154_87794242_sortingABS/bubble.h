#pragma once
#include <iostream>


using namespace std;

//takes in pointer to head of an array and an integer size of that array
template<class T>
class Bubble {
private:
public:
	Bubble(T* h, int n);
	void print(T* h, int n);
};

/*****************************************************************************
* @name		Bubble
* @brief	constructor for Bubble class, sorts an array using the bubble sort
*			method
* 
*			method: starting at beginning of array, compares each value to its
*			neighbors. if the first value is larger than the next, they are 
*			swapped. this loops until no swaps happen
* @param	pointer to the head of an array, integer value for size of that 
*			array
* @retval	none
*****************************************************************************/

template <class T>
Bubble<T>::Bubble(T* h, int n)
{
	int swap = 0;
	int i = 0;
	T temp;

	
	do //do the bubble sort until no swaps are made
	{
		//reset swap
		swap = 0;
		//for i = 1 to n-1 inclusive
		for (i = 1; i < n; i++)
		{
			//if array[i-1] > array[i] then swap and set swap to true
			if (h[i-1] > h[i])
			{
				temp = h[i - 1];
				h[i-1] = h[i];
				h[i] = temp;
				swap = 1;
			}
		}
	} while (swap == 1);
}

/*****************************************************************************
* @name		print
* @brief	prints a bubble sorted array
* @param	pointer to the head of an array, integer value for size of that
*			array
* @retval	none
*****************************************************************************/

template <class T>
void Bubble<T>::print(T* h, int n)
{
	int i;

	for (i = 0; i < n; i++)
	{
		cout << h[i] << " ";
		if (i % 10 == 0)
		{
			cout << endl;
		}
	}
}