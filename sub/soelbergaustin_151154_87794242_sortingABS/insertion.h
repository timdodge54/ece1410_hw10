#pragma once


using namespace std;

template <class T>
class Insertion {
private:
public:
	Insertion(T* h, int n);
	void print(T* h, int n);
};

/*****************************************************************************
* @name		Insertion
* @brief	constructor for Insertion class, sorts an array using the
*			insertion sort method
*
*
* @param	pointer to the head of an array, integer value for size of that
*			array
* @retval	none
*****************************************************************************/

template<class T>
Insertion<T>::Insertion(T* h, int n)
{
	T temp;
	int i = 0, j;

	while (i < n)
	{
		j = i;
		while (j > 0 && h[j - 1] > h[j])
		{
			temp = h[j];
			h[j] = h[j - 1];
			h[j - 1] = temp;
			j--;
		}
		i++;
	}
}

/*****************************************************************************
* @name		print
* @brief	prints an insertion sorted array
* @param	pointer to the head of an array, integer value for size of that
*			array
* @retval	none
*****************************************************************************/

template <class T>
void Insertion<T>::print(T* h, int n)
{
	int i;

	for (i = 0; i < n; i++)
	{
		cout << h[i] << " ";
		if (i % 10 == 0)
		{
			cout << endl;
		}
	}
}


