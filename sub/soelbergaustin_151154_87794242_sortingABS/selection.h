#pragma once
#include <iostream>


using namespace std;

template <class T>
class Selection {
private:
public:
	Selection(T* h, int n);
	void print(T* h, int n);
};

/*****************************************************************************
* @name		Selection
* @brief	constructor for Selection class, sorts an array using the
*			selection sort method
*
*			
* @param	pointer to the head of an array, integer value for size of that
*			array
* @retval	none
*****************************************************************************/

template <class T>
Selection<T>::Selection(T* h, int n)
{
	T temp;
	int i, j, iMin;

	for (j = 0; j < n - 1; j++)
	{
		iMin = j;
		for (i = j + 1; i < n; i++)
		{
			if (h[i] < h[iMin])
			{
				iMin = i;
			}

		}

		if (iMin != j)
		{
			//swap h[j] and h[iMin]
			temp = h[j];
			h[j] = h[iMin];
			h[iMin] = temp;
		}
	}
}

/*****************************************************************************
* @name		print
* @brief	prints a selection sorted array
* @param	pointer to the head of an array, integer value for size of that
*			array
* @retval	none
*****************************************************************************/

template <class T>
void Selection<T>::print(T* h, int n)
{
	int i;

	for (i = 0; i < n; i++)
	{
		cout << h[i] << " ";
		if (i % 10 == 0)
		{
			cout << endl;
		}
	}
}
