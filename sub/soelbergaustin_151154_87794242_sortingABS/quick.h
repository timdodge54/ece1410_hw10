#pragma once


using namespace std;

template <class T>
class Quick {
private:
public:
	Quick(T* h, int n);
	void quicksort(T* h, int low, int hi);
	int partition(T* h, int low, int hi);
	void print(T* h, int n);
};

/*****************************************************************************
* @name		Quick
* @brief	constructor for Quick class, establishes initial hi and low and
*			calls quicksort
*
*
* @param	pointer to the head of an array, integer value for size of that
*			array
* @retval	none
*****************************************************************************/

template <class T>
Quick<T>::Quick(T* h, int n)
{
	int lo = 0;
	int hi = n - 1;

	
	quicksort(h, lo, hi);

}

/*****************************************************************************
* @name		quicksort
* @brief	sorts an array using the quick sort method
*
*
* @param	pointer to the head of an array, integer values for low and hi
*			indices
* @retval	none
*****************************************************************************/

template <class T>
void Quick<T>::quicksort(T* h, int lo, int hi)
{
	int p;

	if (lo < hi)
	{
		p = partition(h, lo, hi);
		quicksort(h, lo, p - 1);
		quicksort(h, p + 1, hi);
	}
}

/*****************************************************************************
* @name		partition
* @brief	partitions array into chunks used by the quicksort method
*
*
* @param	pointer to the head of an array, integer values for lo and hi
*			indices 
* @retval	none
*****************************************************************************/

template <class T>
int Quick<T>::partition(T* h, int lo, int hi)
{
	int i, j;
	T pivot = h[hi];
	T temp;

	i = lo - 1;

	for (j = lo; j < hi; j++)
	{
		if (j < 0)
		{
			j = 0;
		}
		if (h[j] < pivot)
		{
			i = i + 1;
			//swap h[i] with h[j]
			temp = h[i];
			h[i] = h[j];
			h[j] = temp;
		}
	}

	//swap h[i+1] with h[hi]
	temp = h[hi];
	h[hi] = h[i + 1];
	h[i + 1] = temp;

	return i + 1;
}

/*****************************************************************************
* @name		print
* @brief	prints a quick sorted array
* @param	pointer to the head of an array, integer value for size of that
*			array
* @retval	none
*****************************************************************************/

template <class T>
void Quick<T>::print(T* h, int n)
{
	int i;

	for (i = 0; i < n; i++)
	{
		cout << h[i] << " ";
		if (i % 10 == 0)
		{
			cout << endl;
		}
	}
}
