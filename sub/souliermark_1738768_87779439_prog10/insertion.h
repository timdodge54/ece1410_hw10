/*
Create a template class to Insertion sort
*/

#ifndef INSERTION_H
#define INSERTION_H
#include <iostream>
using namespace std;

template <class T>
class Insertion {
  public:
    Insertion (T* nums, int N);
    void print (T* numarray, int Number);

};
/*******************************************************************
 * @name Insertion constructor
 * @brief This function takes in the array and sorts it from least to greatest
 * @param an array and the number of items in the list
 * @retval the array altered to a sorted array
*******************************************************************/
template <class T>
Insertion<T>::Insertion (T* nums, int N)
{
    T temp;
    int i, j;
    int swapped;

    //sort the array
    i = 0;
    while(i < N){
        j = i;
        while((j > 0) && (nums[j-1] > nums[j])){
            //swap the items
            temp = nums[j-1];
            nums[j-1] = nums[j];
            nums[j] = temp;            
            j--;
        }
        i++;
    }

}

/*******************************************************************
 * @name print
 * @brief This function prints the items in the array
 * @param an array and the number of items in the list
 * @retval none
*******************************************************************/
template <class T>
void Insertion<T>::print (T* numarray, int Number)
{
    int i;
    for(i = 0; i < Number; i++){
        cout << numarray[i] << endl;
    }
}

#endif