/*
Create a template class to Quick sort
*/

#ifndef QUICK_H
#define QUICK_H
#include <iostream>
using namespace std;

template <class T>
class Quick {
  public:
    Quick (T* nums, int N);
    void quicksort(T* numarray, int lo, int hi);
    int partition(T* numarray, int lo, int hi);
    void print (T* numarray, int Number);

};

/*******************************************************************
 * @name quicksort
 * @brief This function takes in the array and uses recursion to sort sections
 * by high and low values
 * @param an array and the low and high of the sort
 * @retval the array altered to a sorted array
*******************************************************************/
template <class T>
void Quick<T>::quicksort (T* numarray, int lo, int hi)
{
    int p;
    if(lo < hi){
        p = partition(numarray, lo, hi);
        quicksort(numarray, lo, p - 1);
        quicksort(numarray, p + 1, hi);
    }

}

/*******************************************************************
 * @name partition
 * @brief This function figures out the holes and fills holes by coming together
 * @param an array and the low and high of the sort
 * @retval an int of the value that goes one less
*******************************************************************/
template <class T>
int Quick<T>::partition (T* numarray, int lo, int hi)
{
    T temp;
    int i, j;
    int pivot = numarray[hi];
    i = lo - 1;

    for(j = lo; j < hi; j++)
    {
        if (numarray[j] < pivot){
            i++;
            temp = numarray[i];
            numarray[i] = numarray[j];
            numarray[j] = temp;
        }
    }
    temp = numarray[i+1];
    numarray[i+1] = numarray[hi];
    numarray[hi] = temp;
    return i + 1;

}

/*******************************************************************
 * @name Quick constructor
 * @brief This function takes in the array and sorts it from least to greatest
 * @param an array and the number of items in the list
 * @retval the array altered to a sorted array
*******************************************************************/
template <class T>
Quick<T>::Quick (T* nums, int N)
{
    //sort the array
    quicksort(nums, 0, N);
    

}

/*******************************************************************
 * @name print
 * @brief This function prints the items in the array
 * @param an array and the number of items in the list
 * @retval none
*******************************************************************/
template <class T>
void Quick<T>::print (T* numarray, int Number)
{
    int i;
    for(i = 1; i < Number; i++){
        cout << numarray[i] << endl;
    }
}

#endif