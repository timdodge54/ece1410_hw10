/*
Create a template class to bubble sort
*/

#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>
using namespace std;

template <class T>
class Bubble {
  public:
    Bubble (T* nums, int N);
    void print (T* numarray, int Number);

};
/*******************************************************************
 * @name Bubble constructor
 * @brief This function takes in the array and sorts it from least to greatest
 * @param an array and the number of items in the list
 * @retval the array altered to a sorted array
*******************************************************************/
template <class T>
Bubble<T>::Bubble (T* nums, int N)
{
    T temp;
    int i;
    int swapped;

    //sort the array
    swapped = 1;
    while(swapped != 0){
        swapped = 0;
        for(i = 1; i < N; i++){
            if(nums[i-1] > nums[i]){
    //swap the two items
                temp = nums[i-1];
                nums[i-1] = nums[i];
                nums[i] = temp;
                swapped = 1;
            }
        }
    }

}

/*******************************************************************
 * @name print
 * @brief This function prints the items in the array
 * @param an array and the number of items in the list
 * @retval none
*******************************************************************/
template <class T>
void Bubble<T>::print (T* numarray, int Number)
{
    int i;
    for(i = 0; i < Number; i++){
        cout << numarray[i] << endl;
    }
}

#endif