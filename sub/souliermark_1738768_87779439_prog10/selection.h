/*
Create a template class to Selection sort
*/

#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>
using namespace std;

template <class T>
class Selection {
  public:
    Selection (T* nums, int N);
    void print (T* numarray, int Number);

};
/*******************************************************************
 * @name Selection constructor
 * @brief This function takes in the array and sorts it from least to greatest
 * @param an array and the number of items in the list
 * @retval the array altered to a sorted array
*******************************************************************/
template <class T>
Selection<T>::Selection (T* nums, int N)
{
    T temp;
    int i, j, imin;
    int swapped;

    //sort the array
    swapped = 1;
    for(j = 0; j < N; j++){
        imin = j;
        for (i = j+1; i < N; i++){
            if(nums[i] < nums[imin]){
                imin = i;
            }
        }
        if (imin != j) {
            //swap the items
            temp = nums[imin];
            nums[imin] = nums[j];
            nums[j] = temp;
        }
    }

}

/*******************************************************************
 * @name print
 * @brief This function prints the items in the array
 * @param an array and the number of items in the list
 * @retval none
*******************************************************************/
template <class T>
void Selection<T>::print (T* numarray, int Number)
{
    int i;
    for(i = 0; i < Number; i++){
        cout << numarray[i] << endl;
    }
}

#endif