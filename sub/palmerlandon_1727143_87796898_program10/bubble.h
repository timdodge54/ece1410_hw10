#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>

using namespace std;
//initialize bubbleSort as a template class
template <class T>
class Bubble {
	private:
//bubble contains two public members:
//the constructor, which performs the sorting,
//as well as the print function to print the sorted list
	public:
		Bubble(T *, int);
		void print(T *, int);
};
//the class definitions are performed inline
template <class T>
//nums is the array passed in to be sorted
//N is the number of items in the array
Bubble<T>::Bubble(T*nums, int N)
{
	//a temporary variable is created to be used in the swapping operation
	T tempVal;
	//a bool will keep track if any elements have been swapped
	bool swapped = 1;
	//a while loop is used to perform the check operation
	//as well as the swap function element by element
	while (swapped == 1){
		//if no swap took place, swapped remains false
				swapped = 0;
		for(int i = 1; i <= N-1; i++)
		{
			//if the previous item in the list has a value larger than the 
			//next one, swap the two values
			if(nums[i-1] > nums[i])
			{
				tempVal = nums[i-1];
				nums[i-1] = nums[i];
				nums[i] = tempVal;
				//swapped is set to 1 to signify a swap took place
				swapped = 1;
			}
				
		}
		//as long as swapped is true, the loop will continue to check the rest
		//of the elements
	} 
}
//member function to print the sorted list
template <class T>
void Bubble<T>::print(T*nums, int N)
{
	//a simple for loop is used to walk through each element of the array
	//each element is printed with a space between each element
	for (int j = 0; j < N; j++)
	{
		cout << nums[j] << " ";
	}
}
#endif