#ifndef QUICK_H
#define QUICK_H
#include <iostream>
//quick is initialized as a template class
template <class T>
class Quick {
	private:
	//quick has two private members: quicksort and partition to perform the
	//sorting algorithm
	T quicksort(T*, int, int);
	T partition(T*, int, int);
	//quick has two public members: constructor and a function to print list
	public:
		Quick(T *, int);
		void print(T *, int);
};
//constructor is passed the array to be sorted as well as the size of the array
template <class T>
Quick<T>::Quick(T*nums, int N)
{
	//variables are created to keep track of the high value of a sub-array,
	//as well as the low value, and the pivot
	T lo, hi, p;
	lo = nums[N];
	hi = nums[0];
}
	//quicksort and partition are initialized to be used in the constructor
	
	//function definition of quicksort. Recurrsion is utilized to walk
	//through the list
template <class T>
T Quick<T>::quicksort(T*nums, int lo, int hi)
	{
		int p;
		if(nums[lo] < nums[hi])
		{
			//partition is called to create a sub-array to sort
			p = partition(nums, lo, hi);
			//quicksort is called again to sort low and high if needed
			quicksort(nums, lo, p-1);
			quicksort(nums, p+1, hi);
		}
		//the pivot is then returned
		return p;
	}
	//funciton definition of partition. 
template <class T>
T Quick<T>::partition(T*nums, int lo, int hi)
	{
		//variables to keep track of loop and temporary swap variables
		int j;
		T tempValOne, tempValTwo;
		//pivot is set to the index of high in the array
		T pivot = nums[hi];
		//i is given the index below low
		int i = lo-1;
		//loop through the sub-array from the low value to the high value
		for(j = lo; j <= hi; j++)
		{
			//if the index of j is less than the pivot,
			//perform the swap
			if (nums[j] < pivot)
			{
				//i is incremented, and the tempvals are used to perform
				//the swap
				i++;
				tempValOne = nums[i];
				nums[i] = nums[j];
				nums[j] = tempValOne;
			}
		}

		tempValTwo = nums[i+1];
		nums[i+1] = nums[hi];
		nums[hi] = tempValTwo;
		return i+1;
	}
//member function to print the sorted list
template <class T>
void Quick<T>::print(T*nums, int N)
{
	for (int k = 0; k < N; k++)
	{
		//a simple for loop is used to walk through each element of the array
		//each element is printed with a space between each element
		cout << nums[k] << " ";
	}
}

#endif