#ifndef INSERTION_H
#define INSERTION_H
#include <iostream>

using namespace std;
//initialzie Insertion as a template class
template <class T>
class Insertion {
	private:
	//insertion has two public members: constructor and function to print list
	public:
		Insertion(T *, int);
		void print(T *, int);
};
//the constructor is passed the array as well as the number of elements in the
//array to be sorted
template <class T>
Insertion<T>::Insertion(T*nums, int N)
{
	//a variable is created to count through the list
	int i = 1;
	//a variable is created to keep track of the current value of i
	int j;
	//a temporary variable is created to aid in swapping list elements
	T tempVal;
	//a variable is created to store the size of the array that is passed in
	int length = N;
	
	//while i is less than the size of the array
	while(i < length)
	{
		//j is given the current value of i
		j = i;
		//while j is greater than zero and the previous element is larger than
		//the current element
		while(j > 0 && nums[j-1] > nums[j])
		{
			//perform the swap between the current and previous elements
			tempVal = nums[j-1];
			nums[j-1] = nums[j];
			nums[j] = tempVal;
			//decrement j to step back one element
			j--;
		}
		//increment i to keep walking through the list
		i++;
	}
}
//member function to print the sorted list
template <class T>
void Insertion<T>::print(T*nums, int N)
{
	//a simple for loop is used to walk through each element of the array
	//each element is printed with a space between each element
	for (int k = 0; k < N; k++)
	{
		cout << nums[k] << " ";
	}
}

#endif