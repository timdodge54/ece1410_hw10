#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>

using namespace std;
//initialize Selection as a template class
template <class T>
class Selection {
	private:
	//selection has two public members: constructor and a print function
	public:
		Selection(T *, int);
		void print(T *, int);
};
//the constructor is passed the array to be sorted as well as the 
//number of elements in the array
template <class T>
Selection<T>::Selection(T*nums, int N)
{
	//a temporary varaible is created to assist in the swapping procedure
	T tempVal;
	//a variable is created to keep track of the lowest member of the array
	int iMin;
	//a for loop is utilized ot walk through each member of the array
	//the nested for loop walks through each element, looking to see
	//if the next element is smaller than the current element
	for(int j = 0; j < N-1; j++)
	{
		iMin = j;
		
		for (int i = j+1; i < N; i++)
		{
			if (nums[i] < nums[iMin])
			{
				//if the next element is smaller than the current element
				//the loop steps back to swap elements
				iMin = i;
			}
		}
		
		if(iMin != j)
		{
			//if iMin got moved, the swap is performed on the current value
			//and the next value in the array
			tempVal = nums[j];
			nums[j] = nums[iMin];
			nums[iMin] = tempVal;
		}
		
	}
	
}
//member function to print the sorted list
template <class T>
void Selection<T>::print(T*nums, int N)
{
	//a simple for loop is used to walk through each element of the array
	//each element is printed with a space between each element
	for (int k = 0; k < N; k++)
	{
		cout << nums[k] << " ";
	}
}
#endif