#ifndef SELECTION_H
#define SELECTION_H

#include <iostream>
#include <algorithm>

/******************************************************************************
* @name Selection
* @brief Template class to sort with selection method.
******************************************************************************/
template <class T>
class Selection {
public:
/******************************************************************************
* @name Selection
* @brief Constructor for Selection class that sorts array.
* @param array pointer to array to sort
* @param size size of the array
* @retval none
******************************************************************************/
    Selection(T *array, int size)
    {
        // Loop through array
        for (int j = 0; j < size - 1; j++) 
        {
            int min = j;
            // Loop through elements after min
            for (int i = j + 1; i < size; i++) 
            {
                // If value is min, set as min index
                if (array[i] < array[min])
                {
                    min = i;
                }
            }
            // Swap values if min was found
            if (min != j) 
            {
                std::swap(array[j], array[min]);
            }
        }
    }

/******************************************************************************
* @name print
* @brief Prints all the elements in an array separated by a space.
* @param array pointer to array to print
* @param size size of the array
* @retval none
******************************************************************************/
    void print(T *array, int size)
    {
        // Loop through each item in array
        for (T *temp = array; temp < array + size; temp++)
        {
            // Print item and space
            std::cout << *temp << " ";
        }
        std::cout << std::endl;
    }
};

#endif