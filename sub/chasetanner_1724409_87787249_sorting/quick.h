#ifndef QUICK_H
#define QUICK_H

#include <iostream>
#include <algorithm>

/******************************************************************************
* @name Quick
* @brief Template class to sort with quick method.
******************************************************************************/
template <class T>
class Quick {
public:
/******************************************************************************
* @name Quick
* @brief Constructor for Quick class that sorts array.
* @param array pointer to array to sort
* @param size size of the array
* @retval none
******************************************************************************/
    Quick(T *array, int size)
    {
        // Initialize variables
        int min = 0;
        int max = size - 1;
        // Call recursive helper
        quicksort(array, min, max);
    }

/******************************************************************************
* @name print
* @brief Prints all the elements in an array separated by a space.
* @param array pointer to array to print
* @param size size of the array
* @retval none
******************************************************************************/
    void print(T *array, int size)
    {
        // Loop through each item in array
        for (T *temp = array; temp < array + size; temp++)
        {
            // Print item and space
            std::cout << *temp << " ";
        }
        std::cout << std::endl;
    }
private:
/******************************************************************************
* @name quicksort
* @brief Recursive function that sorts array.
* @param array pointer to array to sort
* @param size size of the array
* @retval none
******************************************************************************/
    void quicksort(T *array, int min, int max)
    {
        // If min is less than max
        if (min < max)
        {
            // Get pivot element and partition array
            int p = partition(array, min, max);
            // Sort partitioned arrays recursively
            quicksort(array, min, p - 1 );
            quicksort(array, p + 1, max);
        }
    }

/******************************************************************************
* @name partition
* @brief Splits and orders array around a pivot element.
* @param array pointer to array to partition
* @param size size of the array
* @retval index of pivot element
******************************************************************************/
    int partition(T *array, int min, int max)
    {
        // Initialize pivot and hole
        T pivot = array[max];
        int i = min - 1;
        // Loop through array
        for (int j = min; j < max; j++)
        {
            // If index value is less than pivot
            if (array[j] < pivot)
            {
                // Incrment hole and swap it with index
                i++;
                std::swap(array[i], array[j]);
            }
        }
        // Swap hole with pivot and return index
        std::swap(array[i+1], array[max]);
        return i + 1;
    }
};

#endif