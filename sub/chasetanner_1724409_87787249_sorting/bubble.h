#ifndef BUBBLE_H
#define BUBBLE_H

#include <iostream>
#include <algorithm>

/******************************************************************************
* @name Bubble
* @brief Template class to sort with bubble method.
******************************************************************************/
template <class T>
class Bubble {
public:
/******************************************************************************
* @name Bubble
* @brief Constructor for Bubble class that sorts array.
* @param array pointer to array to sort
* @param size size of the array
* @retval none
******************************************************************************/
    Bubble(T *array, int size)
    {   
        // Declare swapped bool
        bool swapped;
        // Loop
        do
        {
            swapped = false;
            // Loop through the array
            for (int i = 1; i < size; i++)
            {
                // Swap adjacent elements if needed
                if (array[i-1] > array[i])
                {
                    std::swap(array[i-1], array[i]);
                    // Set swapped to true
                    swapped = true;
                }
            }
            size--;
        // End loop when not swapped
        } while (swapped);
    }

/******************************************************************************
* @name print
* @brief Prints all the elements in an array separated by a space.
* @param array pointer to array to print
* @param size size of the array
* @retval none
******************************************************************************/
    void print(T *array, int size)
    {
        // Loop through each item in array
        for (T *temp = array; temp < array + size; temp++)
        {
            // Print item and space
            std::cout << *temp << " ";
        }
        std::cout << std::endl;
    }
};

#endif