#ifndef INSERTION_H
#define INSERTION_H

#include <iostream>
#include <algorithm>

/******************************************************************************
* @name Insertion
* @brief Template class to sort with insertion method.
******************************************************************************/
template <class T>
class Insertion {
public:
/******************************************************************************
* @name Insertion
* @brief Constructor for Insertion class that sorts array.
* @param array pointer to array to sort
* @param size size of the array
* @retval none
******************************************************************************/
    Insertion(T *array, int size)
    {
        // Loop through array
        for (int i = 1; i < size; i++)
        {
            // Loop through values less than current
            for(int j = i; j > 0 && array[j-1] > array[j]; j--)
            {
                // Swap if out of order
                std::swap(array[j], array[j-1]);
            }
        }
    }

/******************************************************************************
* @name print
* @brief Prints all the elements in an array separated by a space.
* @param array pointer to array to print
* @param size size of the array
* @retval none
******************************************************************************/
    void print(T *array, int size)
    {
        // Loop through each item in array
        for (T *temp = array; temp < array + size; temp++)
        {
            // Print item and space
            std::cout << *temp << " ";
        }
        std::cout << std::endl;
    }
};

#endif