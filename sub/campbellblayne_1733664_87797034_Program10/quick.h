#ifndef QUICK_H
#define QUICK_H
using namespace std;
#include <iostream>

template <typename T>
class Quick {
public:
    Quick(T* arr, int n) {
        quicksort(arr, 0, n - 1);
    }

    void print(T* arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }

private:
    void quicksort(T* arr, int left, int right) {
        if (left < right) {
            int pivotIndex = partition(arr, left, right);
            quicksort(arr, left, pivotIndex - 1);
            quicksort(arr, pivotIndex + 1, right);
        }
    }

    int partition(T* arr, int left, int right) {
        T pivotValue = arr[right];
        int i = left - 1;
        for (int j = left; j < right; j++) {
            if (arr[j] <= pivotValue) {
                i++;
                swap(arr[i], arr[j]);
            }
        }
        swap(arr[i + 1], arr[right]);
        return i + 1;
    }
};

#endif 





