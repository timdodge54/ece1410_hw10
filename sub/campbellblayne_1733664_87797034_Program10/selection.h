#ifndef SELECTION_H
#define SELECTION_H
using namespace std;
#include <iostream>

template <typename T>
class Selection {
public:
    Selection(T* arr, int n) {
        selectionSort(arr, n);
    }

    void print(T* arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }

private:
    void selectionSort(T* arr, int n) {
        int i, j, min_idx;
        for (i = 0; i < n - 1; i++) {
            min_idx = i;
            for (j = i + 1; j < n; j++) {
                if (arr[j] < arr[min_idx]) {
                    min_idx = j;
                }
            }
            std::swap(arr[min_idx], arr[i]);
        }
    }
};

#endif 