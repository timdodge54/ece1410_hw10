#ifndef BUBBLE_H
#define BUBBLE_H

#include <iostream>
using namespace std;
template <typename T>
class Bubble {
public:
    Bubble(T* arr, int n) {
        bubbleSort(arr, n);
    }

    void print(T* arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }

private:
    void bubbleSort(T* arr, int n) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    std::swap(arr[j], arr[j + 1]);
                }
            }
        }
    }
};

#endif 