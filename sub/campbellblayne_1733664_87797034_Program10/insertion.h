#ifndef INSERTION_H
#define INSERTION_H

#include <iostream>
using namespace std;
template <typename T>
class Insertion {
public:
    Insertion(T* arr, int n) {
        insertionSort(arr, n);
    }

    void print(T* arr, int n) {
        for (int i = 0; i < n; i++) {
            std::cout << arr[i] << " ";
        }
        std::cout << std::endl;
    }

private:
    void insertionSort(T* arr, int n) {
        int i, j;
        T key;
        for (i = 1; i < n; i++) {
            key = arr[i];
            j = i - 1;
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = key;
        }
    }
};

#endif /* INSERTION_H */