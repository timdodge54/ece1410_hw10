#pragma once
#include <iostream>

/*********************************************************************
* @name		Selection
* @brief	A class that can sort an array of numbers using selection
*			sort and print the sorted array to the screen
*********************************************************************/

template <class T>
class Selection{
	public:
		//constructor that sorts the array
		Selection(T *a, int n);
		
		//function that prints the array
		void print(T *a, int n);
};

/*********************************************************************
* @name		Selection
* @brief	A constructor for the Selection class that sorts an array
*			using selection sort
* @param	pointer to the head of the array
* @param	number of elements in the array
* @retval	none
*********************************************************************/

template <class T>
Selection<T>::Selection(T *a, int n){
	int i, j;
	T *ai, *aj, *amin;
	T ph;
	
	//for each array element
	for (i=0, ai=a; i<n-1; i++, ai++){
		//set min to current element
		amin = ai;
		
		//for each array element after the current one
		for (j=i+1, aj=ai+1; j<n; j++, aj++){
			//if it's less than the min
			if (*aj < *amin){
				//set min to it
				amin = aj;
			}
		}
		
		//if the minimum has changed
		if (amin != ai){
			//swap current element with the min
			ph = *ai;
			*ai = *amin;
			*amin = ph;
		}
	}
}

/*********************************************************************
* @name		print
* @brief	prints a given array to the screen
* @param	pointer to the head of the array
* @param	number of elements in the array
* @retval	none
*********************************************************************/

template <class T>
void Selection<T>::print(T *a, int n){
	int i;
	
	//for each array element
	for (i=0; i<n; i++, a++){
		//print the element
		std::cout << *a << " ";
	}
}