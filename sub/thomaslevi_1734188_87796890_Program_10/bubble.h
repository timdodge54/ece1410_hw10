#pragma once
#include <iostream>

/*********************************************************************
* @name		Bubble
* @brief	A class that can sort an array of numbers using bubble
*			sort and print the sorted array to the screen
*********************************************************************/

template <class T>
class Bubble{
	public:
		//constructor that sorts the array
		Bubble(T *a, int n);
		
		//function that prints the array
		void print(T *a, int n);
};

/*********************************************************************
* @name		Bubble
* @brief	A constructor for the Bubble class that sorts an array
*			using bubble sort
* @param	pointer to the head of the array
* @param	number of elements in the array
* @retval	none
*********************************************************************/

template <class T>
Bubble<T>::Bubble(T *a, int n){
	char sw;
	int i;
	T *arr;
	T ph;
	
	//while swapped is false
	do {
		//set swapped to false
		sw = 0;
		
		//for each array element
		for (i=0, arr = a; i<n-1; i++, arr++){
			//if current element > next element
			if (*arr > *(arr+1)){
				//swap the current and next element
				ph = *arr;
				*arr = *(arr+1);
				*(arr+1) = ph;
				
				//set swapped to true
				sw = 1;
			}
		}
		
		//decrement the end inc
		n--;
	} while (sw);
}

/*********************************************************************
* @name		print
* @brief	prints a given array to the screen
* @param	pointer to the head of the array
* @param	number of elements in the array
* @retval	none
*********************************************************************/

template <class T>
void Bubble<T>::print(T *a, int n){
	int i;
	
	//for each array element
	for (i=0; i<n; i++, a++){
		//print the element
		std::cout << *a << " ";
	}
}