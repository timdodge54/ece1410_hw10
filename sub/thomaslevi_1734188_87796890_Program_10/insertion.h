#pragma once
#include <iostream>

/*********************************************************************
* @name		Insertion
* @brief	A class that can sort an array of numbers using insertion
*			sort and print the sorted array to the screen
*********************************************************************/

template <class T>
class Insertion{
	public:
		//constructor that sorts the array
		Insertion(T *a, int n);
		
		//function that prints the array
		void print(T *a, int n);
};

/*********************************************************************
* @name		Insertion
* @brief	A constructor for the Insertion class that sorts an array
*			using insertion sort
* @param	pointer to the head of the array
* @param	number of elements in the array
* @retval	none
*********************************************************************/

template <class T>
Insertion<T>::Insertion(T *a, int n){
	int i, j;
	T *ai, *aj;
	T ph;
	
	//for each array element
	for (i=1, ai=a+1; i<n; i++, ai++){		
		//for each elemnt to the left of the current one that's smaller
		//than the current one
		for (j=i, aj=ai; j>0 && *aj<*(aj-1); j--, aj--){
			//swap the current one with the one to the left
			ph = *aj;
			*aj = *(aj-1);
			*(aj-1) = ph;
		}
	}
}

/*********************************************************************
* @name		print
* @brief	prints a given array to the screen
* @param	pointer to the head of the array
* @param	number of elements in the array
* @retval	none
*********************************************************************/

template <class T>
void Insertion<T>::print(T *a, int n){
	int i;
	
	//for each array element
	for (i=0; i<n; i++, a++){
		//print the element
		std::cout << *a << " ";
	}
}