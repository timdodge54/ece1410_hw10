#pragma once
#include <iostream>

/*********************************************************************
* @name		Quick
* @brief	A class that can sort an array of numbers using quicksort
*			and print the sorted array to the screen
*********************************************************************/

template <class T>
class Quick{
	public:
		//constructor that calls quicksort
		Quick(T *a, int n);
		
		//function that prints the array
		void print(T *a, int n);
	private:
		//partition function used by quicksort
		T* partition(T *bot, T *top);
		
		//quicksort funciton
		void quicksort(T *bot, T *top);
};

/*********************************************************************
* @name		Quick
* @brief	A constructor for the Quick class that calls quicksort
* @param	pointer to the head of the array
* @param	number of elements in the array
* @retval	none
*********************************************************************/

template <class T>
Quick<T>::Quick(T *a, int n){
	T *bot, *top;
	
	//set bottom and top values
	bot = a;
	top = a + n - 1;
	
	quicksort(bot, top);
}

/*********************************************************************
* @name		quicksort
* @brief	sorts an array using the quick sort algorithm
* @param	pointer to bottom of array
* @param	pointer to top of array
* @retval	none
*********************************************************************/

template <class T>
void Quick<T>::quicksort(T *bot, T *top){
	T *pv;
	
	//if bottom is less than top
	if (bot<top){
		//create a new partition pivot point
		pv = partition(bot, top);
		
		//repeat for lower partition
		quicksort(bot, pv-1);
		
		//repeat for upper partition
		quicksort(pv+1, top);
	}
}

/*********************************************************************
* @name		partition
* @brief	creates an upper and lower partition of an array, and
*			returns a pointer to the pivot point
* @param	pointer to bottom of array
* @param	pointer to top of array
* @retval	pointer to pivot point
*********************************************************************/

template <class T>
T* Quick<T>::partition(T *bot, T *top){
	T *i;
	T ph;
	
	//for each element of the current range
	for (i=bot; i<top; i++){
		//if the current element is less than the pivot point
		if (*i < *top){
			//swap it with the low element
			ph = *i;
			*i = *bot;
			*bot = ph;
			
			//increment the low pointer
			bot++;
		}
	}
	
	//swap the pivot value with the new bottom value
	ph = *top;
	*top = *bot;
	*bot = ph;
	
	//return the new bottom pointer
	return bot;
}

/*********************************************************************
* @name		print
* @brief	prints a given array to the screen
* @param	pointer to the head of the array
* @param	number of elements in the array
* @retval	none
*********************************************************************/

template <class T>
void Quick<T>::print(T *a, int n){
	int i;
	
	//for each array element
	for (i=0; i<n; i++, a++){
		//print the element
		std::cout << *a << " ";
	}
}