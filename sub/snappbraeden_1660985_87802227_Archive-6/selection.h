/******************************************************************************
 @name selection.h
 @brief house the selection template class
 @param none
 @retval none
 *****************************************************************************/
#ifndef selection_h
#define selection_h
#include <iostream>
#include <iostream>
#include <string>
#include <cstdbool>
#include <iomanip>
#include <chrono>
#include <random>
#include <fstream>
using namespace std;

template <typename T>
class Selection{
    public:
    Selection(T *nums, T s);
    void print(T *nums, T s);
};
/******************************************************************************
 @name Selection
 @brief constructor
 @param T *nums, int s
 @retval none
 *****************************************************************************/
template <typename T>
Selection<T>::Selection(T *nums, T s){
    int i, j, iMin;
    T value; //delcare variables

    for (j = 0; j < s-1; j++) { //for loop to move around the array
        iMin = j; //set variable values
        for (i = j+1; i < s; i++) { //for loop to check
            if (nums[i] < nums[iMin]) { //if statement to compare
                iMin = i; //if true stay the same
            }
        }
        if (iMin != j) { //if not true
            value = nums[j]; //store temporary value
            nums[j] = nums[iMin]; //switch
            nums[iMin] = value; //relocate temporary stored variable
        }
    }
};
/******************************************************************************
 @name selection
 @brief print the sorted array
 @param T *nums, int s
 @retval none
 *****************************************************************************/
template <typename T>
void Selection<T>::print(T *nums, T s){
    //for loop to print array
    int i;
    for (i = 0; i < s; i++){
        cout << nums[i] << " ";
    }
};





/**
 for (j = 0; j < n-1; j++) {
 iMin = j;
 for (i = j+1; i < n; i++) {
 if (a[i] < a[iMin]) {
 iMin = i;
 }
 }
 if (iMin != j) {
 swap(a[j], a[iMin]);
 }
 }
 
 */

#endif /* selection_hpp */
