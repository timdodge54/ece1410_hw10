/******************************************************************************
 @name quick.h
 @brief house the quick template class
 @param none
 @retval none
 *****************************************************************************/
#ifndef quick_h
#define quick_h
#include <iostream>
#include <iostream>
#include <string>
#include <cstdbool>
#include <iomanip>
#include <chrono>
#include <random>
#include <fstream>
#include <algorithm>
using namespace std;

template <typename T>
class Quick{
    public:
    Quick(T *nums, T q);
    void sort(T *nums, int lo, int hi);
    int partition(T *nums, int lo, int hi);
    void print(T *nums, T q);
};
/******************************************************************************
 @name quick.h
 @brief house the quick template class
 @param none
 @retval none
 *****************************************************************************/
template <typename T>
Quick<T>::Quick(T *nums, T q){

    sort(nums, 0, q-1);
};
/******************************************************************************
 @name quick.h
 @brief house the quick template class
 @param none
 @retval none
 *****************************************************************************/
template <typename T>
int Quick<T>::partition(T *nums, int lo, int hi) {
    T pivot = nums[hi];
    int i = lo - 1;
    for (int j = lo; j < hi; j++) {
        if (nums[j] < pivot) {
            i++;
            swap(nums[i], nums[j]);
        }
    }
    swap(nums[i + 1], nums[hi]);
    return i + 1;
}
/******************************************************************************
 @name quick.h
 @brief house the quick template class
 @param none
 @retval none
 *****************************************************************************/
template <typename T>
void Quick<T>::sort(T *nums, int lo, int hi) {
    if (lo < hi) {
        int p = partition(nums, lo, hi);
        sort(nums, lo, p - 1);
        sort(nums, p + 1, hi);
    }
}

/******************************************************************************
 @name quick.h
 @brief house the quick template class
 @param none
 @retval none
 *****************************************************************************/
template <typename T>
void Quick<T>::print(T *nums, T q){
    //for loop to print array
    int i;
    for (i = 0; i < q; i++){
        cout << nums[i] << " ";
    }
};


//a is the array
//lo are indexes for the first array
//hi is an index for the last of the array
//p makes the loop so should be an int
//in quicksort make sure that we incriment lo and hi

/*
 partition
 keep track of when we keep track of values/indexes
 
 
 */

/*
 algorithm quicksort(A, lo, hi) is
 if lo < hi then
 p = partition(A, lo, hi)
 quicksort(A, lo, p - 1 )
 quicksort(A, p + 1, hi)
 
 
 
 algorithm partition(A, lo, hi) is
 pivot = A[hi]
 i = lo - 1
 for j = lo to hi - 1 do
    if A[j] < pivot then
        i = i + 1
        swap A[i] with A[j]
 swap A[i + 1] with A[hi]
 return i + 1
 */
#endif /* quick_hpp */
