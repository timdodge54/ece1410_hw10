/******************************************************************************
 @name insertion.h
 @brief house the insertion template class
 @param none
 @retval none
 *****************************************************************************/
#ifndef insertion_h
#define insertion_h
#include <iostream>
#include <iostream>
#include <string>
#include <cstdbool>
#include <iomanip>
#include <chrono>
#include <random>
#include <fstream>
using namespace std;

template <typename T>
class Insertion{
    public:
        Insertion(T *nums, T ins);
        void print(T *nums, T ins);
};
/******************************************************************************
 @name insertion
 @brief constructor
 @param T *nums, int ins
 @retval none
 *****************************************************************************/
template <typename T>
Insertion<T>::Insertion(T *nums, T ins){
    int i, j;
    T value; //declare variables
    i = 0; //set values
    j = 0; //set values
    while(i < ins){ //while loop to move around the array
        j = i; //reset j value
        while(j > 0 && nums[j-1] > nums[j]){ //while loop to compare
            value = nums[j-1]; //store temporary location
            nums[j-1] = nums[j]; //switch
            nums[j] = value; //relocate temporary stored varible
            j--; //move variable
        }
        i++; //move variable
    }
};
/******************************************************************************
 @name print
 @brief print array
 @param T *nums, int ins
 @retval none
 *****************************************************************************/
template <typename T>
void Insertion<T>::print(T *nums, T ins){
    //for loop to print the sorted array
    int i;
    for (i = 0; i < ins; i++){
        cout << nums[i] << " ";
    }
};

/*i = 1
while i < length(A)
j = i
while j > 0 and A[j-1] > A[j]
swap A[j] and A[j-1]
j = j - 1
end while
i = i + 1
end while*/
#endif 
