/******************************************************************************
 @name bubble.h
 @brief house the template bubble class
 @param none
 @retval none
 *****************************************************************************/
#ifndef bubble_h
#define bubble_h
#include <iostream>
#include <string>
#include <cstdbool>
#include <iomanip>
#include <chrono>
#include <random>
#include <fstream>
using namespace std;

template <typename T>
class Bubble {
    public:
    Bubble(T *nums, T b);
    void print(T *nums, T b);
};
/******************************************************************************
 @name Bubble
 @brief constructor
 @param T *nums, int b
 @retval none
 *****************************************************************************/
template <typename T>
Bubble<T>::Bubble(T *nums, T b){
    int i, n;
    T j;    //declare variables
    n = b; //set n = b
    bool swap; //create a bool variable
    swap = 1; //make it true
    while(swap != false){ //whie bool vaule isnt false
        swap = 0; //set equal to false
        for(i = 1; i < b; i++){ //move throughout the array
            if(nums[i-1] > nums[i]){ //check to values
                swap = 1; //make it true
                j = nums[i-1]; //store one variable
                nums[i-1] = nums[i]; //switch
                nums[i] = j; //locate stored value
            }
        }
    }
};
/******************************************************************************
 @name print
 @brief print the sorted array
 @param T *nums, int b
 @retval none
 *****************************************************************************/
template <typename T>
void Bubble<T>::print(T *nums, T b){
    //for loop to print array
    int i;
    for (i = 0; i < b; i++){
        cout << nums[i] << " ";
    }
};

/*procedure bubbleSort( A : list of sortable items )
n = length(A)
repeat
swapped = false
for i = 1 to n-1 inclusive do
if A[i-1] > A[i] then
swap( A[i-1], A[i] )
swapped = true
end if
end for
until not swapped
end procedure*/
#endif /* bubble_hpp */
