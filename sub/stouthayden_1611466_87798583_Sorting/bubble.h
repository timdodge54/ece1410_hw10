#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>
#include <ostream>

using namespace std;

template <class T>
class Bubble {
	public:
		Bubble(T *, int);
		void print(T*, int);
};

/******************************************************************************
*	@name		Bubble::Bubble
*	@brief		uses bubble sort to create a sorted list of type T
*	@param		pointer to a type T array, integer displaying size of the array
*	@retval		N/A
******************************************************************************/
template <class T>
Bubble<T>::Bubble(T* A, int n)
{
	int i, j;
	T temp;
	//loop through the whole array
	for(i = 0; i < n-1; i++)
	{
		//loop until j reaches i
		for (j = n-1; j >= i+1; j--)
		{
			//if the previous element is greater then swap the two elements
			if(A[j-1] > A[j])
			{
				temp = A[j];
				A[j] = A[j-1];
				A[j-1] = temp;
			}
		}
	}
}

/******************************************************************************
*	@name		Bubble::print
*	@brief		prints the values of a type T array to the screen
*	@param		pointer to a type T array, integer displaying size of the array
*	@retval		none
******************************************************************************/
template <class T>
void Bubble<T>::print(T* A, int n)
{
	//Loop through the entire array and print each element separated by spaces
	for(int i = 0; i<n; i++)
	{
		cout << A[i] << " ";
	}
	//print an endline character
	cout << endl;
}

#endif