#ifndef QUICK_H
#define QUICK_H
#include <iostream>
#include <ostream>

using namespace std;

template <class T>
class Quick {
	public:
		Quick(T *, int);
		void print(T*, int);
};

template <class T>
int partition(T* A, int lo, int hi)
{
	//set the pivot to index hi and let i = low - 1
	T pivot = A[hi];
	T temp;
	int i = lo-1;
	int j = 0;
	//loop from lo to hi minus 1
	for(j=lo; j <= hi-1; j++)
	{
		//if the j index is less than pivot
		// then increase iterator i by 1 and swap the i and j indexes
		if(A[j] < pivot)
		{
			++i;
			temp = A[i];
			A[i] = A[j];
			A[j] = temp;
		}
	}
	//swap the i+1 and hi indexes and return i+1
	temp = A[i+1];
	A[i+1] = A[hi];
	A[hi] = temp;
	return i+1;
}

template <class T>
void quicksort(T* A, int lo, int hi)
{
	if(lo < hi)
	{
		//call partition function and set return value equal to p
		int p = partition(A, lo, hi);
		//use recursion and call quicksort function with hi = p-1
		quicksort(A, lo, p-1);
		//use recursion and call quicksort function with lo = p+1
		quicksort(A, p+1, hi);
	}
}

/******************************************************************************
*	@name		Quick::Quick
*	@brief		uses quick sort to create a sorted list of type T
*	@param		pointer to a type T array, integer displaying size of the array
*	@retval		N/A
******************************************************************************/
template <class T>
Quick<T>::Quick(T* A, int n)
{
	//calls quicksort with low starting at the beginning of the array
	//and high starting at the end of the array.
	quicksort(A, 0, n-1);
}

/******************************************************************************
*	@name		Quick::print
*	@brief		prints the values of a type T array to the screen
*	@param		pointer to a type T array, integer displaying size of the array
*	@retval		none
******************************************************************************/
template <class T>
void Quick<T>::print(T* A, int n)
{
	//Loop through the entire array and print each element separated by spaces
	for(int i = 0; i<n; i++)
	{
		cout << A[i] << " ";
	}
	//print an endline character
	cout << endl;
}

#endif