#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>
#include <ostream>

using namespace std;

template <class T>
class Selection {
	public:
		Selection(T *, int);
		void print(T*, int);
};

/******************************************************************************
*	@name		Selection::Selection
*	@brief		uses selection sort to create a sorted list of type T
*	@param		pointer to a type T array, integer displaying size of the array
*	@retval		N/A
******************************************************************************/
template <class T>
Selection<T>::Selection(T* A, int n)
{
	int i, j;
	T temp;
	//loop through the whole array
	for(i = 0; i < n-1; i++)
	{
		//set the min j index equal to i
		int jMin = i;
		//loop from i+1 to the end of the array
		for (j = i+1; j < n; j++)
		{
			//if the j index is less than the jMin index
			//then jMin = j
			if(A[j] < A[jMin])
			{
				jMin = j;
			}
		}
		//if jMin doesn't equal i then swap the ith index with the jMin index
		if(jMin != i)
		{
			temp = A[i];
			A[i] = A[jMin];
			A[jMin] = temp;
		}
	}
}

/******************************************************************************
*	@name		Selection::print
*	@brief		prints the values of a type T array to the screen
*	@param		pointer to a type T array, integer displaying size of the array
*	@retval		none
******************************************************************************/
template <class T>
void Selection<T>::print(T* A, int n)
{
	//Loop through the entire array and print each element separated by spaces
	for(int i = 0; i<n; i++)
	{
		cout << A[i] << " ";
	}
	//print an endline character
	cout << endl;
}

#endif