#ifndef INSERTION_H
#define INSERTION_H
#include <iostream>
#include <ostream>

using namespace std;

template <class T>
class Insertion {
	public:
		Insertion(T *, int);
		void print(T*, int);
};

/******************************************************************************
*	@name		Insertion::Insertion
*	@brief		uses insertion sort to create a sorted list of type T
*	@param		pointer to a type T array, integer displaying size of the array
*	@retval		N/A
******************************************************************************/
template <class T>
Insertion<T>::Insertion(T* A, int n)
{
	int i = 1;
	int j = 0;
	T temp;
	//loop n number of times where n = size of the array
	while (i < n)
	{
		j = i;
		//loop while j is greater than 0 and the index j-1 > index j
		while (j > 0 && A[j-1] > A[j])
		{
			//swap index j with index j-1 and set j = to j-1
			temp = A[j];
			A[j] = A[j-1];
			A[j-1] = temp;
			j = j-1;
		}
		i++;
	}
}

/******************************************************************************
*	@name		Insertion::print
*	@brief		prints the values of a type T array to the screen
*	@param		pointer to a type T array, integer displaying size of the array
*	@retval		none
******************************************************************************/
template <class T>
void Insertion<T>::print(T* A, int n)
{
	//Loop through the entire array and print each element separated by spaces
	for(int i = 0; i<n; i++)
	{
		cout << A[i] << " ";
	}
	//print an endline character
	cout << endl;
}

#endif