
#include <iostream> 
#ifndef INSERTION
#define INSERTION
#define N 100000

using namespace std; 
 
 /*************************************************************************
*	@name	Insertion Class
*	@brief  Class definition for Insertion
*	@param	none
*	@retval	none
*************************************************************************/
template <typename T> //template of typename T
class Insertion
{	
	int i, j;
	int n; //declare ints
	T length[N];
	T swap, swapping; 
	//declare variables of type T
	
	public:
	Insertion(T * , int i);
	void print(T *, int n);
	//public functions
};

/*************************************************************************
*	@name	Insertion Constructor
*	@brief  Constructor for class Insertion
*	@param	T length, int i
*	@retval	none
*************************************************************************/
template <typename T>
Insertion<T>::Insertion(T *length, int i)
{	
i = 1; //initialize i
	while (i < N)
		//loop while i is less than N
		{
		j = i;

		while (j > 0 && length[j-1] > length[j])
			{
		//loop while j is greater than 0, 
		//and length of j minus 1 is greater
		//than the length of j
			swap = length[j];
			swapping = length[j-1];
			length[j] = swapping;
			length[j-1] = swap;
			//swap length of j with length of j minus 1
			j = j - 1;
			//decrement j
			}
		i = i + 1;
		//increment i
		}
}
/*************************************************************************
*	@name	Insertion Print
*	@brief  Defines the print function to print sorted list
*	@param	T length, int n
*	@retval	none
*************************************************************************/
template <typename T>
void Insertion<T>::print(T *length, int n)
{
	n = length[N];
//n equals totallength of N
	for(i = 0; i < N; i++)
		//for loop to iterate through entire list
	{
		cout << length[i] << " ";
		//print statement
	}
}
#endif