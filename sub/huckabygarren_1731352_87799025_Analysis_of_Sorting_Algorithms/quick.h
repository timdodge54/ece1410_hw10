
#include <iostream> 
#ifndef QUICK
#define QUICK
#define N 100000

using namespace std; 
 
/*************************************************************************
*	@name	Quick Class
*	@brief  Class definition for Quick
*	@param	none
*	@retval	none
*************************************************************************/
template <typename T> //template of typename T
class Quick
{	
	private:
	int i, p, lo, hi, n, swap, swapping;
	//declare ints
	T length[N];
	T pivot;
	//declare variables of type T
	void quicksort(T *, int, int);
	int partition(T *, int, int);
	//declare private functions
	
	public:
	Quick(T *, int);
	void print(T *, int);
	//declare public functions

};
/*************************************************************************
*	@name	Quick Constructor
*	@brief  Constructor for class Quick
*	@param	T length int i
*	@retval	none
*************************************************************************/
template <typename T>
Quick<T>::Quick(T *length, int i)
{
	quicksort(length, 0, N - 1);
	//call quicksort
}
/*************************************************************************
*	@name	Quick Constructor
*	@brief  quicksort function for class quick
*	@param	T length int lo int hi
*	@retval	none
*************************************************************************/
template <typename T>
void Quick<T>::quicksort(T *length, int lo, int hi)
{
	if (lo < hi)
	{
		//if lo is less than hi
	p = partition(length, lo, hi);
	//call partition
	quicksort(length, lo, p - 1 );
	//call quicksort below pivot
	quicksort(length, p + 1, hi);
	//call quicksort above pivot
	}
}	
	/*************************************************************************
*	@name	partition function
*	@brief  partition function for class quick
*	@param	T length, int lo, int hi
*	@retval	int i + 1
*************************************************************************/
template <typename T>
int Quick<T>::partition(T *length, int lo, int hi)
{
	T pivot = length[hi];
	//pivot equals length at hi
	int i = (lo - 1);
	int j;
	//define ints
		for (j = lo; j < hi; j++)
		{
	//increment j
			if (length[j] < pivot)
			{
	//if length at j is less than pivot
			i = i + 1;
			swap = length[i]; 
			swapping = length[j];
			length[i] = swapping;
			length[j] = swap;
	//swap length i with length j
			}
		
		}
		swap = length[i + 1];
		swapping = length[hi];
		length[i+1] = swapping;
		length[hi] = swap;
	//swap length at i plus 1 with length at hi
		return i+1;
	//return i + 1
}
/*************************************************************************
*	@name	Quick print
*	@brief  Defines the print function to print sorted values of Quick
*	@param	T length, int n
*	@retval	none
*************************************************************************/
template <typename T>
void Quick<T>::print(T *length, int n)
{
	n = length[N];
	//n equals length at N
	for(i = 0; i < N; i++){
		//increment i
		cout << length[i] << " ";
			//print array
	}
}
#endif