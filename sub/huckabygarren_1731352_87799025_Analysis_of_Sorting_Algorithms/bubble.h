#include <iostream> 
#ifndef BUBBLE
#define BUBBLE
#define N 100000
 
using namespace std; 
 
 /*************************************************************************
*	@name	Bubble Class
*	@brief  Class definition for Bubble
*	@param	none
*	@retval	none
*************************************************************************/
template <typename T> //template of typename T
class Bubble
{	
	int i;
	int n;
	//declare ints
	T length[N];
	T swap, swapping;
	//declare variables of type T
	bool swapped;
	//bool swapped
	
	public:
	Bubble(T *, int);
	void print(T *, int);
	//declare public functions
};

/*************************************************************************
*	@name	Bubble Constructor
*	@brief  Constructor for class Bubble
*	@param	T length, int i
*	@retval	none
*************************************************************************/
template <typename T>
Bubble<T>::Bubble(T *length, int i)
{
swapped = true;
//swapped equals true
	n = length[N];
	while(swapped == true)
	{
	//while swapped equals true
		swapped = false;
		//change swapped to false
		for (i = 1; i < N; i++)
		{
			//increment i
			if (length[i-1] > length[i])
			{
			swap = length[i-1];
			swapping = length[i];
			length[i-1] = swapping;
			length[i] = swap;
			swapped = true;
			//swap length at i minus 1 with length of i
			//swapped equals true
			}
			
		}
	}
}

/*************************************************************************
*	@name	Bubble Print
*	@brief  Defines the print function to print sorted values of Bubble
*	@param	T length, int n
*	@retval	none
*************************************************************************/
template <typename T>
void Bubble<T>::print(T *length, int n)
{
	n = length[N];
	//n equals length at N
	for(i = 0; i < N; i++)
	{
	//increment i
		cout << length[i] << " ";
	//print sorted array
	}	
}
#endif