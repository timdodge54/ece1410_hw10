

#include <iostream> 
#ifndef SELECTION
#define SELECTION
#define N 100000

using namespace std; 
 
 /*************************************************************************
*	@name	Selection Class
*	@brief  Class definition for Selection
*	@param	none
*	@retval	none
*************************************************************************/
template <typename T> //template of typename T
class Selection
{
	int i, j;
	int iMin;
	int n;
	//declare ints
	T length[N];
	T swap, swapping;
	//declare variables of type T
	public:
	Selection(T *, int);
	void print(T *, int);
	//declare public functions
};
/*************************************************************************
*	@name	Selection Constructor
*	@brief  Constructor for class Selection
*	@param	T length int i
*	@retval	none
*************************************************************************/
template <typename T>
Selection<T>::Selection(T *length, int i)
{
	
	for (j = 0; j < N; j++) 
	{
	//for loop increment j
		iMin = j;
		//iMin equals j
		for (i = j+1; i < N; i++) 
		{
			//for loop to increment I
			if (length[i] < length[iMin]) 
			{
			iMin = i;
			}
//if length at i is less than length at iMin
		}
			if (iMin != j) 
			{
			swap = length[j];
			swapping = length[iMin];
			length[j] = swapping;
			length[iMin] = swap;
//swap length at j with length at iMin
			}
	}
}

/*************************************************************************
*	@name	Selection Print
*	@brief  Defines the print function to print list of sorted variables
*	@param	T length, int i
*	@retval	none
*************************************************************************/
template <typename T>
void Selection<T>::print(T *length, int n)
{
	n = length[N];
//n equals length at N
	for(i = 0; i < N; i++)
	{
		//increment i
		cout << length[i] << " ";
		//print array
	}
}


#endif
