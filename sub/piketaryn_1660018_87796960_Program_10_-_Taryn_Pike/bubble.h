#ifndef BUBBLE_H
#define BUBBLE_H

template <typename T>
class Bubble {
public:
/*********************************************************************
* @name Bubble
* @brief Bubble sorts a given array with size n
* @param T* arr: pointer to the array to be sorted, int n: size of array
* @retval none
*********************************************************************/
    Bubble(T* arr, int n) { // begin
        for (int i = 0; i < n-1; i++) { // loop through all values
            for (int j = 0; j < n-i-1; j++) {
                if (arr[j] > arr[j+1]) {// if the element is greater than next
                    T temp = arr[j];
                    arr[j] = arr[j+1]; // swap the elements
                    arr[j+1] = temp;
                }
            }
        }
    } // end
/*********************************************************************
* @name print
* @brief Prints the numbers in the array
* @param T* arr: pointer to the array to be sorted, int n: size of array
* @retval none
*********************************************************************/
    void print(T* arr, int n) { // begin
        for (int i = 0; i < n; i++) { // loop through all the array
            std::cout << arr[i] << " "; // print value
        }
        std::cout << std::endl;
    } // end
}; 
#endif