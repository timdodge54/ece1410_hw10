#ifndef SELECTION_H
#define SELECTION_H

template <typename T>
class Selection {
public:
/*********************************************************************
* @name Selection
* @brief Constructor for Selection class
* @param T* arr: pointer to the array to be sorted, int n: size of array
* @retval None
*********************************************************************/
    Selection(T* arr, int n) { // begin
        for (int i = 0; i < n-1; i++) { // loop through the array
            int min_index = i; // // assume i-th element is the smallest
            for (int j = i+1; j < n; j++) {
			// loop through the remaining elements
                if (arr[j] < arr[min_index]) {
                    min_index = j;
					// if j is smaller than i then update the min index
                }
            }
            if (min_index != i) { // if min index is different than i
                T temp = arr[i];
                arr[i] = arr[min_index]; // swap
                arr[min_index] = temp;
            }
        }
    } // end
	/*********************************************************************
* @name print
* @brief Prints the numbers in the array
* @param T* arr: pointer to the array to be sorted, int n: size of array
* @retval none
*********************************************************************/
    void print(T* arr, int n) {
        for (int i = 0; i < n; i++) { // loop through all the array
            std::cout << arr[i] << " "; // print value
        }
        std::cout << std::endl;
    }
};
#endif