#ifndef INSERTION_H
#define INSERTION_H


template <typename T>
class Insertion {
public:
/*********************************************************************
* @name Insertion
    * @brief Constructor for the Insertion class
    * @param T* arr: pointer to the array to be sorted, int n: size of array
    * @retval none
*********************************************************************/
    Insertion(T* arr, int n) { // begin
        for (int i = 1; i < n; i++) { 
		// loop through the array starting at index 1
            T key = arr[i]; // save current element as the key
            int j = i-1;
            while (j >= 0 && arr[j] > key) { // while element before is >
                arr[j+1] = arr[j]; // shift the larger element one to right
                j--;
            }
            arr[j+1] = key; // insert correct element into correct spot
        }
    } // end
	/*********************************************************************
* @name print
* @brief Prints the numbers in the array
* @param T* arr: pointer to the array to be sorted, int n: size of array
* @retval none
*********************************************************************/
    void print(T* arr, int n) {
        for (int i = 0; i < n; i++) { // loop through all the array
            std::cout << arr[i] << " "; // print value
        }
        std::cout << std::endl;
    }
};
#endif