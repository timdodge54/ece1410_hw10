#ifndef QUICK_H
#define QUICK_H

#include <iostream>

template<typename T>
class Quick {
private:
    T* arr;
    int n;
/*********************************************************************
* @name quickSort
* @brief Sorts the array using the quick sort algorithm
* @param int start, int end
* @retval none
*********************************************************************/
    void quickSort(int start, int end) { // begin
        if (start < end) { // Find pivot index
            int pivotIndex = partition(start, end);
            quickSort(start, pivotIndex - 1);
			// Recursively sort elements before and after the pivot index
            quickSort(pivotIndex + 1, end);
        }
    } // end
/*********************************************************************
* @name partition
* @brief Partitions the array and returns the pivot index
* @param int start, int end
* @retval int pivotIndex
*********************************************************************/
    int partition(int start, int end) { // begin
	// Choose the pivot element as the last element of the subarray
        T pivot = arr[end]; 
        int pivotIndex = start;
        for (int i = start; i < end; i++) { // loop through the subarray
            if (arr[i] < pivot) {
				// If current element is less than pivot element, swap it with element at pivot index
                std::swap(arr[i], arr[pivotIndex]);
                pivotIndex++;
            }
        }
        std::swap(arr[pivotIndex], arr[end]);
        return pivotIndex;
    } // end

public:
/*********************************************************************
* @name Quick
* @brief Constructor for quick
* @param T* arr: pointer to the array to be sorted, int n: size of array
* @retval none
*********************************************************************/
    Quick(T* arr, int n) : arr(arr), n(n) { // begin
        quickSort(0, n - 1); // call the quicksort
    } // end
/*********************************************************************
* @name print
* @brief Prints the numbers in the array
* @param T* arr: pointer to the array to be sorted, int n: size of array
* @retval none
*********************************************************************/
    void print(T* arr, int n) { // begin
        for (int i = 0; i < n; i++) { // loop through all the array
            std::cout << arr[i] << " "; // print the value
        }
    }
}; // end

#endif 
