
#include <iostream>
using namespace std;


template <class T>
class Quick {
private:
public:
	Quick(T* list, int n)
	{
		Quicksort(list, 0, n - 1);
	}

	void Quicksort(T* list, int lo, int hi)
	{
		if (lo < hi)
		{
			int p = partition(list, lo, hi);
			Quicksort(list, lo, p - 1);
			Quicksort(list, p + 1, hi);
		}
	}

	int partition(T* list, int lo, int hi)
	{
		int a = lo, b = lo;
		T temp;
		for (; b < hi; b++)
		{
			if (list[b] < list[hi])
			{
				temp = list[b];
				list[b] = list[a];
				list[a] = temp;
				a++;
			}
		}
		temp = list[a];
		list[a] = list[hi];
		list[hi] = temp;
		return a;
	}
	void print(T* list, int n)
	{
		int i;
		for (i = 0; i < n; i++)
		{
			cout << list[i] << " ";
		}
		cout << endl;
	}
};
