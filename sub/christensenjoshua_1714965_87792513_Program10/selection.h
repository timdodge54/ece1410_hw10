
#include <iostream>
using namespace std;


template <class T>
class Selection {
private:
public:
	Selection(T* list, int n)
	{
		int a, b, min;
		T temp;
		for (a = 0; a < n - 1 ;a++)
		{
			for (min = a, b = a + 1; b < n; b++)
			{
				if (list[b] < list[min])
				{
					min = b;
				}
			}
			temp = list[min];
			list[min] = list[a];
			list[a] = temp;
		}
	}
	void print(T* list, int n)
	{
		int i;
		for (i = 0; i < n; i++)
		{
			cout << list[i] << " ";
		}
		cout << endl;
	}
};
