
#include <iostream>
using namespace std;


template <class T>
class Insertion {
private:
public:
	Insertion(T* list, int n)
	{
		int a, b;
		T temp;
		for (a = 1; a < n; a++)
		{
			for (b = a; b > 0 && list[b-1] > list[b]; b--)
			{
				temp = list[b];
				list[b] = list[b - 1];
				list[b - 1] = temp;
			}
		}
	}
	void print(T* list, int n)
	{
		int i;
		for (i = 0; i < n; i++)
		{
			cout << list[i] << " ";
		}
		cout << endl;
	}
};
