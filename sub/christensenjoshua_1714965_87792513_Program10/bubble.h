
#include <iostream>
using namespace std;


template <class T>
class Bubble {
private:
public:
	Bubble(T* list, int n)
	{
		int a, b, end = n - 1;
		T temp = (T)0;
		for (;end > 0;end--)
		{
			for (a = 0, b = 1; b <= end; a++, b++)
			{
				if (list[a] > list[b])
				{
					temp = list[a];
					list[a] = list[b];
					list[b] = temp;
				}
			}
		}
	}
	void print(T* list, int n)
	{
		int i;
		for (i = 0; i < n; i++)
		{
			cout << list[i] << " ";
		}
		cout << endl;
	}
};
