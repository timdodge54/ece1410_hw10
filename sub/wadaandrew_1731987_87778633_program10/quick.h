#pragma once
#include <iostream>
template <class T>
class Quick {
	public:
		/********************************************************************* 
		* @name quick
		* @brief Sorts list using quick sort
		* @param pointer to array of class T, # of elements
		* @retval  none
		*********************************************************************/ 
		Quick(T *list, int n) {
			if (n > 0) {
				int p = partition(list, 0, n - 1);
				Quick(list, 0, p - 1);
				Quick(list, p+1, n-1);
			}
		};

		/********************************************************************* 
		* @name quick
		* @brief Sorts list using quick sort
		* @param pointer to array of class T
		* @retval  none
		*********************************************************************/ 
		Quick(T *list, int lo, int hi) {
			if (lo<hi) {
				int p = partition(list, lo, hi);
				Quick(list, lo, p - 1);
				Quick(list, p+1, hi);
			}
		};

		/********************************************************************* 
		* @name print
		* @brief prints list
		* @param pointer to start of list, length of list
		* @retval  none
		*********************************************************************/ 
		void print(T* list, int n) {
			for (int i = 0; i < n; i++) {
				std::cout << list[i] << " ";
				if (i % 10 == 0) std::cout << std::endl;
			}
		};
	private:
		/********************************************************************* 
		* @name part
		* @brief divides list into 2 partitions
		* @param array, hi value, low value
		* @retval  none
		*********************************************************************/ 
		int partition(T* list, int lo, int hi) {
			T pivot = list[hi];
			int i = lo - 1;
			for (int j = lo; j < hi; j++) {
				if (list[j] < pivot) {
					i = i + 1;
					T temp = list[j];
					list[j] = list[i];
					list[i] = temp;
				}
			}
			T temp = list[i + 1];
			list[i + 1] = list[hi];
			list[hi] = temp;
			return i + 1;
		};
};