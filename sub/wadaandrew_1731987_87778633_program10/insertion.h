#pragma once
#include <iostream>
template <class T>
class Insertion {
	public:
		/********************************************************************* 
		* @name Bubble
		* @brief Sorts list using bubble sort
		* @param pointer to array of class T
		* @retval  none
		*********************************************************************/ 
		Insertion(T *list, int n) {
			int i = 1;
			while (i < n) {
				int j = i;
				while ((j > 0) & (list[j - 1] > list[j])) {
					//swap variables
					T temp = list[j - 1];
					list[j - 1] = list[j];
					list[j] = temp;
					j--;
				}
				i=i+1;
			}
		};

		/********************************************************************* 
		* @name print
		* @brief prints list
		* @param pointer to start of list, length of list
		* @retval  none
		*********************************************************************/ 
		void print(T* list, int n) {
			for (int i = 0; i < n; i++) {
				std::cout << list[i] << " ";
				if (i % 10 == 0) std::cout << std::endl;
			}
		};
	
};