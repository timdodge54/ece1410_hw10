#pragma once
#include <iostream>
template <class T>
class Selection {
	public:
		/********************************************************************* 
		* @name Bubble
		* @brief Sorts list using bubble sort
		* @param pointer to array of class T
		* @retval  none
		*********************************************************************/ 
		Selection(T *a, int n) {
			int iMin = 0;
			for (int j = 0; j < n-1; j++) {
				iMin = j;
				for (int i = j+1; i < n; i++) {
					if (a[i] < a[iMin]) {
						iMin = i;
					}
				}
				if (iMin != j) {
					//swap variables
					T temp = a[iMin];
					a[iMin] = a[j];
					a[j] = temp;
				}
			}

		};

		/********************************************************************* 
		* @name print
		* @brief prints list
		* @param pointer to start of list, length of list
		* @retval  none
		*********************************************************************/ 
		void print(T* list, int n) {
			for (int i = 0; i < n; i++) {
				std::cout << list[i] << " ";
				if (i % 10 == 0) std::cout << std::endl;
			}
		};
	
};