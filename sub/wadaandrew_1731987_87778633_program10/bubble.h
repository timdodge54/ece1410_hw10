#pragma once
#include <iostream>
template <class T>
class Bubble {
	public:
		/********************************************************************* 
		* @name Bubble
		* @brief Sorts list using bubble sort
		* @param pointer to array of class T
		* @retval  none
		*********************************************************************/ 
		Bubble(T *list, int n) {
			int swapped = 0;
			do {
				swapped = 0;
				for (int i = 1; i < n; i++) {
					if (list[i - 1] > list[i]) {
						//swap variables
						T temp = list[i - 1];
						list[i - 1] = list[i];
						list[i] = temp;
						swapped = 1;
					}
				}
			} while (swapped == 1);

		};

		/********************************************************************* 
		* @name print
		* @brief prints list
		* @param pointer to start of list, length of list
		* @retval  none
		*********************************************************************/ 
		void print(T* list, int n) {
			for (int i = 0; i < n; i++) {
				std::cout << list[i] << " ";
				if (i % 10 == 0) std::cout << std::endl;
			}
		};
	
};