#include <iostream>

#pragma once

using namespace std;

template <class T>
class Bubble{
private:
    T *temp;
    int x;
    int swap;
public:
    Bubble(T *, int);
    void print(T *, int);
};

/********************************************************************
 * @name Bubble
 * @brief function constructs and sorts the array of elements using
 *        the bubble sort method.
 * @param *array
 * @param size
 * @retval none
 *******************************************************************/
template <class T>
Bubble<T>::Bubble(T *array, int size) {
    temp = array;
    x = size;
    do {
        swap = 0;   //set flag to 0
        for (int i = 1; i < x; i++) {
            //if first word comes after second word
            if (temp[i - 1] >temp[i]) {
                T hold = temp[i - 1];  //swap
                temp[i - 1] = temp[i];  //swap
                temp[i] = hold;    //swap
                swap = 1;   //set flag to 1
            }
        }
    } while (swap == 1);
}

/********************************************************************
 * @name print
 * @brief function prints the sorted array
 * @param *C
 * @param y
 * @retval none
 *******************************************************************/
template <class T>
void Bubble<T>::print(T *C, int y){
    for(int i = 0; i < y; i++){
        cout<<C[i]<< " ";   //print sorted words
    }
}