#include <iostream>

#pragma once

using namespace std;

template <typename T>
class Selection {
private:
    T *temp;
    int x;
public:
    Selection(T *, int);
    void print(T *, int);
};

/********************************************************************
 * @name Selection
 * @brief function constructs and sorts the array of elements using
 *        the selection sort method.
 * @param *array
 * @param size
 * @retval none
 *******************************************************************/
template <class T>
Selection<T>::Selection(T *array, int size) {

    temp = array;   //creates changeable variable
    x = size;       //creates changeable variable
    int i, j, min;

    for (i = 0; i < x-1; i++) {
        min = i;    //walks through the array one index at a time
        for (j = i+1; j < x; j++) {
            if (temp[j] < temp[min]) {  //compares each item stored
                min = j;    //assigns the new minimum
            }
        }
        T hold = temp[i];   //swap
        temp[i] = temp[min];    //swap
        temp[min] = hold;   //swap
    }
}

/********************************************************************
 * @name print
 * @brief function prints the sorted array
 * @param *C
 * @param y
 * @retval none
 *******************************************************************/
template <class T>
void Selection<T>::print(T *C, int y) {
    for (int i = 0; i < y; i++) {
        cout << C[i] << " ";   //print sorted words
    }
}