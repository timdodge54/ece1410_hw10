#include <iostream>

#pragma once

using namespace std;

template <typename T>
class Insertion {
private:
    T *temp;
    int x;
public:
    Insertion(T *array, int size);
    void print(T *, int);
};

/********************************************************************
 * @name Insertion
 * @brief function constructs and sorts the array of elements using
 *        the insertion sort method.
 * @param *array
 * @param size
 * @retval none
 *******************************************************************/
template <class T>
Insertion<T>::Insertion(T *array, int size) {
    temp = array;
    x = size;
    int i = 1, j;

    while (i < x) { //allows the entire array to be searched
        j = i;
        while (j > 0 && temp[j-1] > temp[j]) {  //compares the current
                                                //position of j with the
                                                //index before it. Builds
                                                //array in place
            T hold = temp[j];   //swap
            temp[j] = temp[j-1];    //swap
            temp[j-1] = hold;   //swap
            j--;    //moves j backwards through the array
        }
        i++;    //moves i up through the array
    }
}

/********************************************************************
 * @name print
 * @brief function prints the sorted array
 * @param *C
 * @param y
 * @retval none
 *******************************************************************/
template <class T>
void Insertion<T>::print(T *C, int y) {
    for (int i = 0; i < y; i++) {
        cout << C[i] << " ";   //print sorted words
    }
}