#include <iostream>

#pragma once

using namespace std;

template<typename T>
class Quick {
private:
    T* temp;
    T hold;
public:
    Quick(T*, int);
    void print(T*, int);
    void quicksort(T*, int, int);
    int partition(T*, int, int);
};

/********************************************************************
 * @name Quick
 * @brief function constructs and calls to the function that will
 *        start the quicksort method of sorting.
 * @param *array
 * @param size
 * @retval none
 *******************************************************************/
template <class T>
Quick<T>::Quick(T *array, int size){
    quicksort(array, 0, size-1);
}

/********************************************************************
 * @name quicksort
 * @brief function picks a pivot point, then calls to another function
 *        to sort the lower elements in the array and the higher
 *        elements of the array (independently)
 * @param *array
 * @param low
 * @param high
 * @retval none
 *******************************************************************/
template <class T>
void Quick<T>::quicksort(T* array, int low, int high) {
    if(low < high) {
        int pivot = partition(array, low, high);
        quicksort(array, low, pivot - 1);   //sorts the low numbers
        quicksort(array, pivot + 1, high);  //sorts the high numbers
    }
}

/********************************************************************
 * @name partition
 * @brief function
 * @param *array
 * @param size
 * @retval none
 *******************************************************************/
template <class T>
int Quick<T>::partition(T* array, int low, int high) {
    temp = array;
    T pivot = temp[high];
    int i = low - 1;    //initializes i to 0
    for(int j=low; j<=high-1; j++) {
        if(temp[j] < pivot) {   //moves all elements lower than the pivot
            i++;    //moves to gap
            hold = temp[i]; //swap
            temp[i] = temp[j];  //swap
            temp[j] = hold; //swap
        }
    }
    //places the pivot in the gap
    hold = temp[i+1];   //swap
    temp[i+1] = temp[high]; //swap
    temp[high] = hold;  //swap
    return i+1;
}

/********************************************************************
 * @name print
 * @brief function prints the sorted array
 * @param *C
 * @param y
 * @retval none
 *******************************************************************/
template <class T>
void Quick<T>::print(T *C, int y) {
    for (int i = 0; i < y; i++) {
        cout << C[i] << " ";   //print sorted words
    }
}
