#include <iostream>
using namespace std;

template <class T>
class Selection {
public:
	Selection(T* head, int n);
	void print(T* head, int n);
};

template <class T>
Selection<T>::Selection(T* head, int n) {
	int i, j;
	for (j = 0; j < n - 1; j++) {
		int iMin = j;
		for (i = j + 1; i < n; i++) {
			if (head[i] < head[iMin]) {
				iMin = i;
			}
		}
		if (iMin != j) {
			T temp = head[j];
			head[j] = head[iMin];
			head[iMin] = temp;
		}
	}
}

template <class T>
void Selection<T>::print(T* head, int n) {
	for (int i = 0; i < n; i++) {
		cout << head[i] << " ";
	}
}