#include <iostream>
using namespace std;

template <class T>
class Quick {
public:
	Quick(T* head, int n);
	Quick(T* head, int lo, int hi);
	void print(T* head, int n);
	int partition(T* head, int lo, int hi);
};

template <class T>
Quick<T>::Quick(T* A, int n) {
	int hi = n;
	int lo = 0;
	if (lo < hi)
	{
		int p = partition(A,lo, hi);
		Quick(A, lo, p - 1);
		Quick(A, p + 1, hi);
	}
}

template <class T>
Quick<T>::Quick(T* head, int lo, int hi) {
	if (lo < hi)
	{
		int p = partition(head, lo, hi);
		Quick(head, lo, p - 1);
		Quick(head, p + 1, hi);
	}
}

template <class T>
void Quick<T>::print(T* head, int n) {
	for (int i = 0; i < n; i++) {
		cout << head[i] << " ";
	}
}

template <class T>
int Quick<T>::partition(T* A, int lo, int hi) {
	T pivot = A[hi];
	int i = lo - 1;
	for (int j = lo; j < hi - 1; j++) {
		if (A[j] < pivot) {
			i++;
			T temp = A[i];
			A[i] = A[j];
			A[j] = temp;
		}
	}
	T templol = A[i + 1];
	A[i + 1] = A[hi];
	A[hi] = templol;
	return i + 1;
}