#include <iostream>
using namespace std;

template <class T>
class Bubble {
public:
	Bubble(T* head, int n);
	void print(T* head, int n);
};
template <class T>
Bubble<T>::Bubble(T* head, int n) {
	int i, j;
	for (i = 0; i < n - 1; i++) {
		for (j = 0; j < n - i - 1; j++) {
			if (head[j] > head[j + 1]) {
				T temp = head[j];
				head[j] = head[j + 1];
				head[j + 1] = temp;
			}
		}
	}
}

template <class T>
void Bubble<T>::print(T* head, int n) {
	for (int i = 0; i < n; i++) {
		cout << head[i] << " ";
	}
}