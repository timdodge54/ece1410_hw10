#include <iostream>
using namespace std;

template <class T>
class Insertion {
public:
	Insertion(T* head, int n);
	void print(T* head, int n);
};

template <class T>
Insertion<T>::Insertion(T* head, int n) {
	int i = 1;
	int j;
	while (i < n) {
		j = i;
		while (j > 0 && head[j - 1] > head[j]) {
			T temp = head[j];
			head[j] = head[j - 1];
			head[j - 1] = temp;
			j--;
		}
		i++;
	}
}

template <class T>
void Insertion<T>::print(T* head, int n) {
	for (int i = 0; i < n; i++) {
		cout << head[i] << " ";
	}
}