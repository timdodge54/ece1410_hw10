#ifndef SELECTION_H
#define SELECTION_H

#include<algorithm>
#include <iostream>

using namespace std;
template <class T>
class Selection{
	public:
	Selection (T* number, int n);
	void print(T* number, int n);
	
};


/**
@name	Selection
@brief	sorts a bunch of numbers
@param	number and n
@retval	none
*/
template <class T>
Selection<T>::Selection (T* number, int n)
{
	int j, iMin, i;
	// goes through the list and finds the smallest number
	for(j=0; j<n-1; j++)
	{
		iMin = j;
		for(i= j + 1; i<n; i++)
		{
			if(number[i] <number[iMin])
			{
				iMin = i;
			}
		}
		if(iMin!=j)
		{
			swap(number[j], number[iMin]);
			
		}
	}
	
	
}

/**
@name	print
@brief	prints a sorted list of numbers
@param	number and n
@retval	none
*/
template <class T>
void Selection<T>::print(T*number, int n)
{
	int spot;
	// go through the array and print all of it
	for(spot = 0; spot < n; spot++)
	{
		cout<<number[spot]<<" ";
		
	}
}

#endif