#ifndef QUICK_H
#define QUICK_H

#include <algorithm>

#include <iostream>

using namespace std;

template <class T>
class Quick
{
	public:
	Quick (T* A, int amount); 
	void quicksort(T * A, int lo, int hi);
	int partition(T*A, int lo, int hi);
	void print (T* A, int n);
	
	
};

/**
@name	Quick
@brief	sorts a bunch of numbers
@param	A and amount
@retval	none
*/
template <class T>
Quick<T>::Quick (T* A, int amount)
{
	// call quicksort
	quicksort(A, 0, amount-1);
	
}



/**
@name	Quick
@brief	sorts a bunch of numbers
@param	A and lo and hi
@retval	none
*/
template <class T>
void Quick<T>::quicksort (T*A, int lo, int hi)
{
	int p;
	if (lo<hi)
	{
		// find the pivot and find the pivots in those two divided places
		p = partition(A, lo, hi);
		quicksort(A, lo, p-1);
		quicksort(A, p+1, hi);	
	}
	
}



/**
@name	partition
@brief	sorts a bunch of numbers
@param	A and lo and hi
@retval	an int 
*/
template <class T>
int Quick<T>::partition (T*A, int lo, int hi)
{
	int i, j;
	T pivot;
	// make the pivot spot and check some stuff
	pivot = A[hi];
	i = lo - 1;
	for(j= lo; j<hi; j++)
	{
		if (A[j]<pivot)
		{
			i++;
			swap(A[i], A[j]);
			
		}
	}
		swap(A[i+1], A[hi]);
	return i+1;
}
/**
@name	print
@brief	prints a sorted list of numbers
@param	number and n
@retval	none
*/
template <class T>
void Quick<T>::print(T*number, int n)
{
	int spot;
	// go through the array and print all of it
	for(spot = 0; spot < n; spot++)
	{
		cout<<number[spot]<<" ";
		
	}
}

#endif