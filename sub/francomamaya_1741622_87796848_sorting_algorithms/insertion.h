#ifndef INSERTION_H
#define INSERTION_H 
#include <algorithm>

#include <iostream>

using namespace std;

template <class T>
class Insertion{
	public:
	Insertion (T* A, int amount); 
	void print(T* A, int amount);
	
	
};
/**
@name	Insertion
@brief	sorts a bunch of numbers
@param	A and amount
@retval	none
*/
template <class T>
Insertion<T>::Insertion (T* A, int amount)
{
	int i, j;
	i=1;
	// go through checking the next value and inserting it in the sorted spot
	while (i<amount)
	{
		j=i;
		while(j>0 && A[j-1] >A[j])
		{
			swap(A[j], A[j-1]);
			j--;
		}
		i++;
	}
	
}

/**
@name	print
@brief	prints a sorted list of numbers
@param	number and n
@retval	none
*/
template <class T>
void Insertion<T>::print(T*number, int n)
{
	int spot;
	// go through the array and print all of it
	for(spot = 0; spot < n; spot++)
	{
		cout<<number[spot]<<" ";
		
	}
}

#endif