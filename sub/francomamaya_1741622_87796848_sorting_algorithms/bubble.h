#ifndef BUBBLE_H
#define BUBBLE_H 

#include <algorithm>
#include <iostream>

using namespace std;

template <class T>
class Bubble{
	public:
	Bubble(T* number, int n); 
	void print (T*number, int n);
	
};

/**
@name	Bubble
@brief	sorts a bunch of numbers
@param	nums and n
@retval	none
*/
template <class T>
Bubble<T>::Bubble(T* number, int n)
{
	int i;
	int swapped = 1;
	int temp = n;
	// compares neighbors and swaps if needed the largest bubbles to the back
	while(swapped)
	{
		swapped = 0;
		for (i=1; i<temp; i++)
		{
			if(number[i-1]>number[i])
			{
				swap(number[i-1], number[i]);
				swapped = 1;
			
			}
		}
		temp--;
	}
	
}
/**
@name	print
@brief	prints a sorted list of numbers
@param	number and n
@retval	none
*/
template <class T>
void Bubble<T>::print(T*number, int n)
{
	int spot;
	// go through the array and print all of it
	for(spot = 0; spot < n; spot++)
	{
		cout<<number[spot]<<" ";
		
	}
}


#endif