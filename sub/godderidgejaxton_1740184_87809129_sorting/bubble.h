#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>

template <typename t>
class Bubble{
	public:
		Bubble(t*, int);
		void print(t*, int);
};

/**
@name: bubble
@brief: sorts using bubble method
@param: arr, n
@retval: none
**/

template <typename t>
Bubble<t>::Bubble(t* arr, int n)
{
	int l;
	int i;
	
	for(l = 0; l < n-1; l++)
	{
		
		for(i = 0; i < n - l - 1; i++)
		{	
			if(arr[i-1] > arr[i])
			{
				std::swap(arr[i-1], arr[i]);
			}	
		}
	}
}	

template <typename t>
void Bubble<t>::print(t* arr, int n)
{
	for(int i = 0; i < n; i++)
	{
        std::cout << arr[i] << " ";
    }
}
#endif