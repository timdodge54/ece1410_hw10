#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>

template <typename t>
class Selection{
	public:
		Selection(t*, int);
		void print(t*, int);
};

/**
@name: selection
@brief: sorts using selection method
@param: arr, n
@retval: none
**/

template <typename t>
Selection<t>::Selection(t* arr, int n)
{
	int i;
	int j;
	int iMin;
	
	for(j = 0; j < n-1; j++)//loop
	{
		iMin = j;
		
		for(i = j + 1; i < n; i++)//loop
		{
			if(arr[i] < arr[iMin])//checks if i is less than the min
			{
				iMin = i;//sets i to the min
			}
		}
		
		if(iMin != j)
		{
			std::swap(arr[j], arr[iMin]);//swap function
		}	
		
	}
}	

/**
@name: print
@brief: prints sorted numbers
@param: arr, n
@retval: none
**/

template <typename t>
void Selection<t>::print(t* arr, int n)
{
	for(int i=0; i<n; i++){
        std::cout << arr[i] << " ";//loop to print array
    }
}

#endif