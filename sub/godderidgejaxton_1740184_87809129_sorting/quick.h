#ifndef QUICK_H
#define QUICK_H
#include <iostream>

template <typename t>
class Quick{
	public:
		Quick(t*, int );
		void print(t*, int);
};

/**
@name: partition
@brief: sets pivot number and sorts
@param: arr, low, high
@retval: none
**/

template <typename t>
int partition(t* arr, int low, int high)
{
	int j;
	
	t pivot = arr[high];//sets high number to pivot
	int i = (low - 1);
		
	for (j = low; j <= high - 1; j++)//loop
	{
		if (arr[j] < pivot)//checks if j is < pivot
		{
			i = i + 1;
			std::swap(arr[i], arr[j]);//swap function
		}	
	}	
	std::swap(arr[i + 1], arr[high]);//swap function
	return (i + 1);
}

/**
@name: quickhelp
@brief: sets high and low
@param: arr, low, high
@retval: none
**/

template <typename t>
void Quickhelp(t* arr, int low, int high)
{
	if(low < high)
	{
		int q = partition(arr, low, high);//sets q to partition
		
		Quickhelp(arr, low, q - 1);//calls quickhelp function
		Quickhelp(arr, q + 1, high);
	}	
}	
	
/**
@name: quick
@brief: calls quickhelp
@param: arr, n
@retval: none
**/

template <typename t>
Quick<t>::Quick(t* arr, int n)
{
	Quickhelp(arr, 0, n-1);//calls quickhelp function
}	

template <typename t>
void Quick<t>::print(t* arr, int n)
{
	for(int i=0; i<n; i++){
        std::cout << arr[i] << " ";//loop to print array
    }
}

#endif