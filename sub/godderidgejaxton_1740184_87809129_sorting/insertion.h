#ifndef INSERTION_H
#define INSERTION_H
#include <iostream>

template <typename t>
class Insertion{
	public:
		Insertion(t*, int);
		void print(t*, int);
};

/**
@name: insertion
@brief: sorts using insertion method
@param: arr, n
@retval: none
**/

template <typename t>
Insertion<t>::Insertion(t* arr, int n)
{
	int i;
	int j;
	
	i = 1;//set i to 1
	
	while(i < n)//loop
	{
		j = i;
		while(j > 0 && arr[j - 1] > arr[j])//loop 
		{
			std::swap(arr[j], arr[j-1]);//swap function
			j = j - 1;
		}
		
		i = i + 1;
		
	}	
}	

/**
@name: print
@brief: prints sorted numbers
@param: arr, n
@retval: none
**/

template <typename t>
void Insertion<t>::print(t* arr, int n)
{
	for(int i=0; i<n; i++){
        std::cout << arr[i] << " ";//loop to print array
    }
}

#endif