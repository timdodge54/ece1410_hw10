#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>

using namespace std;

template <class T>
class Bubble {
	private:
		bool swap = true;
		T temp;
		T* arr;
	public:
		Bubble(T*, int);
		void print(T*, int);
};

/*********************************************************************
* @name Bubble
* @brief Constructor, sorts contents of an array with Bubble sort algorithm
* @param T* and int
* @retval none
*********************************************************************/
template<class T>
Bubble<T>::Bubble(T* arr, int size)
{
	//keeps track of variable swap, loops while swap = true
	while (swap == true)
	{
		swap = false;
		//loops through the contents of array
		for (int i = 1; i < size; i++)
		{
			//compares each element, if previous > next, swaps them
			if (arr[i - 1] > arr[i])
			{
				//swaps adjacent elements of the array
				temp = arr[i-1];
				arr[i-1] = arr[i];
				arr[i] = temp;
				//if swapped, sets swap to true
				swap = true;
			}
		}
	}
}

/*********************************************************************
* @name print
* @brief Prints the contents of the sorted array
* @param T* and int
* @retval none
*********************************************************************/
template<class T>
void Bubble<T>::print(T* arr, int size)
{
	//loops through array, prints each element separated by a space
	for (int i = 0; i < size; i++)
	{
		cout << arr[i] << " ";
	}
}

#endif