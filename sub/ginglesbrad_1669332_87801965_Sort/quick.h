#ifndef QUICK_H
#define QUICK_H
#include <iostream>
#include <algorithm>

using namespace std;

template <class T>
class Quick {
	private:
		T* arr;
	public:
		Quick(T*, int);
		void print(T*, int);
		void quicksort(T*, int, int);
		int partition(T*, int, int);
};

/*********************************************************************
* @name Quick
* @brief Constructor, sorts contents of an array with quicksort algorithm
* @param T* and int
* @retval none
*********************************************************************/
template<class T>
Quick<T>::Quick(T* arr, int size)
{
	//calling quicksort algorithm
	quicksort(arr, 0, size - 1);
}

/*********************************************************************
* @name print
* @brief Prints the contents of the sorted array
* @param T* and int
* @retval none
*********************************************************************/
template<class T>
void Quick<T>::print(T* arr, int size)
{
	//loops through the array, prints each element separated by a space
	for (int i = 0; i < size; i++)
	{
		cout << arr[i] << " ";
	}
}

/*********************************************************************
* @name quicksort
* @brief sorts the contents of a set of numbers in an array
* @param T*, int, int
* @retval none
*********************************************************************/
template<class T>
void Quick<T>::quicksort(T*arr, int lo, int hi)
{
	int p;
	//walks in from the outside elements of the array
	if (lo < hi)
	{
		//if not on the same element, find a partition
		//and call quicksort on each new sub-array
		p = partition(arr, lo, hi);
		quicksort(arr, lo, p - 1);
		quicksort(arr, p + 1, hi);
	}
}

/*********************************************************************
* @name partition
* @brief finds a point to partition the array into sub-arrays
* @param T*, int, int
* @retval int
*********************************************************************/
template<class T>
int Quick<T>::partition(T* arr, int lo, int hi)
{
	//pivot starts at last element of the array
	T pivot = arr[hi];
	int i = lo - 1;
	//loops through array, finds a new pivot by comparing elements
	for (int j = lo; j <= hi - 1; j++)
	{
		if (arr[j] < pivot)
		{
			//increments i before doing anything with it
			i++;
			//swaps element i with element j
			swap(arr[i], arr[j]);
		}
	}
	//swaps element i + 1 with element hi
	swap(arr[i + 1], arr[hi]);
	return i + 1;
}

#endif