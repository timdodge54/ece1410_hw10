#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>
#include <algorithm>

using namespace std;

template <class T>
class Selection {
	private:
        T* arr;
	public:
		Selection(T*, int);
		void print(T*, int);
};

/*********************************************************************
* @name Selection
* @brief Constructor, sorts contents of an array with Selection sort algorithm
* @param T* and int
* @retval none
*********************************************************************/
template<class T>
Selection<T>::Selection(T* arr, int size)
{
	//outer for loop, loops through array
    for (int j = 0; j < size - 1; j++)
    {
        int iMin = j;
		//inner for loop
        for (int i = j + 1; i < size; i++)
        {
			//compares the elements in the array and finds next
			//unsorted number and assigns it to iMin
            if (arr[i] < arr[iMin])
            {
                iMin = i;
            }
        }
		//Swaps iMin and j elements of the array
        if (iMin != j)
        {
            swap(arr[j], arr[iMin]);
        }
    }
}

/*********************************************************************
* @name print
* @brief Prints the contents of the sorted array
* @param T* and int
* @retval none
*********************************************************************/
template<class T>
void Selection<T>::print(T* arr, int size)
{
	//loops through array, prints every element separated by a space
    for (int i = 0; i < size; i++)
    {
        cout << arr[i] << " ";
    }
}

#endif