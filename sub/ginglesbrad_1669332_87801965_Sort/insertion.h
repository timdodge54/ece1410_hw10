#ifndef INSERTION_H
#define INSERTION_H
#include <iostream>
#include <algorithm>

using namespace std;

template <class T>
class Insertion {
	private:
		T* arr;
	public:
		Insertion(T*, int);
		void print(T*, int);
};

/*********************************************************************
* @name Insertion
* @brief Constructor, sorts contents of an array with Insertion sort algorithm
* @param T* and int
* @retval none
*********************************************************************/
template<class T>
Insertion<T>::Insertion(T* arr, int size)
{
	int i = 1;
	//loops through array until i = size
	while (i < size)
	{
		int j = i;
		//sorts array by comparing each element side by side
		while (j > 0 && arr[j - 1] > arr[j])
		{
			//if previous element is greater than next element, swap them
			swap(arr[j], arr[j - 1]);
			j--;
		}
		i++;
	}
}

/*********************************************************************
* @name print
* @brief Prints the contents of the sorted array
* @param T* and int
* @retval none
*********************************************************************/
template<class T>
void Insertion<T>::print(T* arr, int size)
{
	//loops through array, prints each element separated by a space
	for (int i = 0; i < size; i++)
	{
		cout << arr[i] << " ";
	}
}

#endif