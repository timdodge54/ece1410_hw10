#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>

using namespace std;

template <typename T>
class Selection
{
	public:
	
	/**
	@name Selection
	@brief template class for Selection algorithm
	@param T nums[]: any type array to be sorted
	@param int N: number of values in array
	@retval NA
	*/		
	Selection(T nums[], int N)	
	{
		int i, j, smallest;
		T	placeholder;
		for (i=0; i < N-1; i++)			//go through one fewer each pass
		{
			smallest=i;					//smallest is new first
			//j= i-1;
			
			for(j=i+1 ; j<N ; j++)		//go through nums
			{
				if(nums[j] < nums[smallest])	//find smallest
				{
					smallest = j;				//grab new smallest
				}
				placeholder = nums[i];			//bring smallest to front
				nums[i] = nums[smallest];
				nums[smallest] = placeholder;
			}
		}
	}	
	
	/**
	@name print
	@brief go through array and print members
	@param T nums[]: any type array to be sorted
	@param int N: number of values in array
	@retval none
	*/	
	void print(T nums[], int N)		//print list
		{
		int k;						
		for (k=0; k<N; ++k)			//go through each member of array
		{
			cout << " " << 	nums[k];	//print current member
		}
		cout << "\n";
		}

};

#endif