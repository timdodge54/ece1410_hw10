#ifndef INSERTION_H
#define INSERTION_H
#include <iostream>
using namespace std;

template <typename T>
class Insertion
{
	public:
	
	/**
	@name Insertion
	@brief template class for insertion algorithm
	@param T nums[]: any type array to be sorted
	@param int N: number of values in array
	@retval NA
	*/		
	Insertion(T nums[], int N)
	{
		int i, j;
		T next, placeholder;
		for (i=1; i<N; i++)		//increment one fewer each time
		{
			j=i-1;				//scoot member is one less than sorting mem
			next = nums[i];		//next member to sort
								//loop until next is the largets value
			for (; j>=0 && nums[j] > next; j--)	
			{					//scoot each member over
				placeholder=nums[j+1];
				nums[j+1]=nums[j];
			}
			nums[j+1]=next;
		}
		
	}
		/**
	@name print
	@brief go through array and print members
	@param T nums[]: any type array to be sorted
	@param int N: number of values in array
	@retval none
	*/	
	void print(T nums[], int N)		//print list
		{
		int k;						
		for (k=0; k<N; ++k)			//go through each member of array
		{
			cout << " " << 	nums[k];	//print current member
		}
		cout << "\n";
		}
};



#endif