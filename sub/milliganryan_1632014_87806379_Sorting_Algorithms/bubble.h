#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>

using namespace std;

template <typename T>
class Bubble
{
	private:
		//T nums[N];
	public:
	
		/**
		@name Bubble
		@brief template class for the Bubble algorithm
		@param T nums[]: any type array to be sorted
		@param int N: number of values in array
		@retval NA
		*/		
		Bubble (T nums[], int N)
		{
		int j, i, placeholder;
		for (j = 0; j < N; ++j)
		{
			for (i=0; i < N - j; ++i)
			{
				if(nums[i] > nums[i+1])
				{
					int placeholder = nums[i];
					nums[i] = nums[i+1];
					nums[i+1] = placeholder;
				}
			}
		}
		}
		
	/**
	@name print
	@brief go through array and print members
	@param T nums[]: any type array to be sorted
	@param int N: number of values in array
	@retval none
	*/	
	void print(T nums[], int N)		//print list
	{
		int k;						
		for (k=0; k<N; ++k)			//go through each member of array
		{
			cout << " " << 	nums[k];	//print current member
		}
		cout << "\n";
	}
};

#endif