#ifndef QUICK_H
#define QUICK_H
#include <iostream>

using namespace std;

template <typename T>
class Quick
{
public:

	/**
	@name quicksort
	@brief recursively divides and sorts array
	@param T nums[]: any type array to be sorted
	@param int high: highest placed member in partition
	@param int low:	lowest placed member in partition
	@retval none
	*/		
	void quicksort(T nums[], int high, int low)
		{
			if(low < high)		//if low member is before high member still
			{							//when they're =, then sorting is done
				//cout << "15" << '\n';
				int p;
				p = partition(nums, high, low); //split array into two
				quicksort(nums, p-1, low);		//quicksort lower half
				quicksort(nums, high, p+1);		//quicksort upper half
			}
		}
		
	/**
	@name Partition
	@brief creates pivot points and sorts around them
	@param T nums[]: any type array to be sorted
	@param int high: highest placed member in partition
	@param int low:	lowest placed member in partition
	@retval int: the partion for quicksort to use
	*/				
	int partition(T nums[], int high, int low)
		{
			T pivot=nums[high];				//set pivot to high end of array
			T placeholder;			
			int i=low-1;						//high was low
			int j;		
			//cout << "P"<<pivot <<'\t'<<"H"<< high << '\t';
			
			for (j=low; j < high; j++)	
			{
				if (nums[j] <= pivot)			//if member is < pivot
				{
					i++;				
					placeholder=nums[i];		//grab onto low number
					nums[i]=nums[j];
					nums[j]=placeholder;
				}
			}
			placeholder=nums[i+1];				//swap the hole
			nums[i+1]=nums[high];
			nums[high]=placeholder;
			return i+1;		
			}
			
	/**
	@name Quick
	@brief template class for quick sort algorithm
	@param T nums[]: any type array to be sorted
	@param int N: number of values in array
	@retval NA
	*/		
	Quick(T nums[], int N)						//constructor
		{
			int high, low;						
			low=0;								//pass in correct N and array
			high=N-1;
			quicksort(nums, high, low);
		}
		
	/**
	@name print
	@brief go through array and print members
	@param T nums[]: any type array to be sorted
	@param int N: number of values in array
	@retval none
	*/	
	void print(T nums[], int N)		//print list
		{
		int k;						
		for (k=0; k<N; ++k)			//go through each member of array
		{
			cout << " " << 	nums[k];	//print current member
		}
		cout << "\n";
		}
};

#endif