#pragma once
#include <random>
#include <chrono>
#include <iostream>
using namespace std;

template <class T>
class Insertion
{
private:

public:
	Insertion(T* nums, int n);
	void print(T* nums, int n);
};

template <class T>
Insertion<T>::Insertion(T* nums2, int number)
{
	int n = number;
	T temp;
	int i = 1, j = 0;
	while (i < n)
	{
		j = i;
		while ((j > 0) && (nums2[j - 1] > nums2[j]))
		{
			temp = nums2[j];
			nums2[j] = nums2[j - 1];
			nums2[j - 1] = temp;
			j -= 1;
		}
		i += 1;
	}
}

template <class T>
void Insertion<T>::print(T* nums3, int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << nums3[i] << " ";
	}
}