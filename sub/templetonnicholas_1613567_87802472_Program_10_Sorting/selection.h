#pragma once
#include <random>
#include <chrono>
#include <iostream>
using namespace std;

template <class T>
class Selection
{
private:

public:
	Selection(T* nums, int n);
	void print(T* nums, int n);
};

template <class T>
Selection<T>::Selection(T* nums2, int number)
{
	int n = number;
	int iMin = 0;
	T temp;
	
	for (int j = 0; j < n - 1; j++)
	{
		iMin = j;
		for (int i = j+1; i < n; i++)
		{
			if (nums2[i] < nums2[iMin])
			{
				iMin = i;
			}
		}
		if (iMin != j)
		{
			temp = nums2[iMin];
			nums2[iMin] = nums2[j];
			nums2[j] = temp;
		}
	}
}

template <class T>
void Selection<T>::print(T* nums3, int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << nums3[i] << " ";
	}
}
