#pragma once
#include <iostream>
using namespace std;

template <class T>
class Quick
{
private:
public:
	Quick(T* array, int n);
	void quicksort(T* array, int low, int hi);
	int partition(T*array, int low, int hi);
	void print(T* nums, int n);
};

template <class T>
Quick<T>::Quick(T* array, int n)
{
	int low = 0, hi = n - 1;
	quicksort(array, low, hi);
}

template <class T>
void Quick<T>::quicksort(T* array, int low, int hi)
{
	int p = 0;
	if (low < hi)
	{
		p = partition(array, low, hi);
		quicksort(array, low, p - 1);
		quicksort(array, p + 1, hi);
	}
}

template <class T>
int Quick<T>::partition(T* array, int low, int hi)
{
	T pivot, temp;
	pivot = array[hi];
	int i = low - 1;
	for (int j = low; j < hi; j++)
	{
		if (array[j] < pivot)
		{
			i += 1;
			temp = array[j];
			array[j] = array[i];
			array[i] = temp;
		}
	}
	temp = array[i +1];
	array[i+1] = array[hi];
	array[hi] = temp;
	return (i + 1);
}

template <class T>
void Quick<T>::print(T* nums3, int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << nums3[i] << " ";
	}
}
