/**
@file bubble.h
@brief Header file for Bubble
*/

#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>

using namespace std;

/**
@brief Class for bubble
@tparam T Data to be sorted
*/

template <typename T>
class Bubble {
public:

    /**
    @brief Constructor for Bubble class
    @param Pointer to array T
    @param n elements in array
    */

    Bubble(T* arr, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    T temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    /**
    @brief Prints sorted array
    */

    void print(T* arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }
};


#endif