/**
@file insertion.h
@brief Header file for Insertion
*/

#ifndef INSERTION_H
#define INSERTION_H
#include <iostream>

using namespace std;

/**
@brief Class for insertion 
@tparam T Data to be sorted
*/

template <typename T>
class Insertion {
public:

    /**
    @brief Constructor for Insertion class
    @param Pointer to array of type T
    @param n Number of elements in array
    */


    Insertion(T* arr, int n) {
        int i, j;
        T key;
        for (i = 1; i < n; i++) {
            key = arr[i];
            j = i - 1;
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    }

    /**
    @brief Prints the sorted array.
    */

    void print(T* arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }
};


#endif