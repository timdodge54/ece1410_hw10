/**
@file quick.h
@brief Header file for Quick
*/

#ifndef QUICK_H
#define QUICK_H
#include <iostream>

using namespace std;

/**
@brief Class for quick 
@tparam T Data to be sorted
*/


template <typename T>
class Quick {
public:

    /*
    @brief Constructor for Quick 
    @param Pointer to array of type T
    @param n number of elements in rray
    */

    Quick(T* arr, int n) {
        quicksort(arr, 0, n - 1);
    }

    /**
    @brief Sorts the array using quick sort
    */

    void quicksort(T* arr, int low, int high) {
        if (low < high) {
            int pi = partition(arr, low, high);
            quicksort(arr, low, pi - 1);
            quicksort(arr, pi + 1, high);
        }
    }

    /**
    @brief Partitions array around pivot element.
    @return Index of pivot element 
    */

    int partition(T* arr, int low, int high) {
        T pivot = arr[high];
        int i = low - 1;
        for (int j = low; j <= high - 1; j++) {
            if (arr[j] < pivot) {
                i++;
                T temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
        T temp = arr[i + 1];
        arr[i + 1] = arr[high];
        arr[high] = temp;
        return i + 1;
    }

    /**
    @brief Prints sorted array.
    */

    void print(T* arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }
};


#endif