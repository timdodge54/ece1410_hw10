/**
@file selection.h
@brief Header file for Selection
*/

#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>

using namespace std;


/**

@brief Class for selection 
@tparam T Data to be sorted
*/

template <typename T>
class Selection {
public:

    /*
    @brief Constructor for Selection class
    @param Pointer to array of T
    @param n Number of elements in array
    */

    Selection(T* arr, int n) {
        int i, j, min_idx;
        for (i = 0; i < n - 1; i++) {
            min_idx = i;
            for (j = i + 1; j < n; j++) {
                if (arr[j] < arr[min_idx]) {
                    min_idx = j;
                }
            }
            T temp = arr[min_idx];
            arr[min_idx] = arr[i];
            arr[i] = temp;
        }
    }

    /**
    @brief Prints the sorted array
    */

    void print(T* arr, int n) {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
    }
};


#endif