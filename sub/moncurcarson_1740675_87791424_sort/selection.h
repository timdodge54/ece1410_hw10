/*********************************************************************
* @name selection.h
* @brief implements the selection alg
* @param none
* @retval  none
********************************************************************/
#include<iostream>
using namespace std;

template<typename T>
class Selection {
    private:
        T *arr;
        int n;

    public:
        Selection(T *arr, int n) {
            this->arr = arr;
            this->n = n;
            selection();
        }

        void selection() {
            int minIndex;
            for(int i=0; i<n-1; i++) {
                minIndex = i;
                for(int j=i+1; j<n; j++) {
                    if(arr[j] < arr[minIndex])
                        minIndex = j;
                }
                if(minIndex != i)
                    swap(arr[i], arr[minIndex]);
            }
        }

        void print(T *, int n) {
            for(int i=0; i<n; i++)
                cout << arr[i] << " ";
            cout << endl;
        }
};
