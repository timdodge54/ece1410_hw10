/*********************************************************************
* @name quick.h
* @brief implements the quicksort alg
* @param none
* @retval  none
********************************************************************/
#include<iostream>
using namespace std;

template<typename T>
class Quick {
    private:
        T *arr;
        int n;

    public:
        Quick(T *arr, int n) {
            this->arr = arr;
            this->n = n;
            quick(0, n-1);
        }

        int partition(int low, int high) {
            T pivot = arr[high];
            int i = low-1;

            for(int j=low; j<high; j++) {
                if(arr[j] <= pivot) {
                    i++;
                    swap(arr[i], arr[j]);
                }
            }
            swap(arr[i+1], arr[high]);
            return i+1;
        }

        void quick(int low, int high) {
            if(low < high) {
                int pivotIndex = partition(low, high);
                quick(low, pivotIndex-1);
                quick(pivotIndex+1, high);
            }
        }

        void print(T *, int n) {
            for(int i=0; i<n; i++)
                cout << arr[i] << " ";
            cout << endl;
        }
};