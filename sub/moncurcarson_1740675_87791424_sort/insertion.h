/*********************************************************************
* @name insertion.h
* @brief implements the insertion algorithm
* @param none
* @retval  none
********************************************************************/
#include<iostream>
using namespace std;

template<typename T>
class Insertion {
    private:
        T *arr;
        int n;

    public:
        Insertion(T *arr, int n) {
            this->arr = arr;
            this->n = n;
            insertion();
        }

        void insertion() {
            T key;
            int j;
            for(int i=1; i<n; i++) {
                key = arr[i];
                j = i-1;

                while(j>=0 && arr[j]>key) {
                    arr[j+1] = arr[j];
                    j--;
                }
                arr[j+1] = key;
            }
        }

        void print(T *, int n) {
            for(int i=0; i<n; i++)
                cout << arr[i] << " ";
            cout << endl;
        }
};