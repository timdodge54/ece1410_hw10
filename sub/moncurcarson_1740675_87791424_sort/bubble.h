/*********************************************************************
* @name bubble.h
* @brief Implements the BubbleSort algorithm
* @param none
* @retval  none
********************************************************************/
#include<iostream>
using namespace std;

template<typename T>
class Bubble {
    private:
        T *arr;
        int n;

    public:
        Bubble(T *arr, int n) {
            this->arr = arr;
            this->n = n;
            bubble();
        }

        void bubble() {
            for(int i=0; i<n-1; i++) {
                for(int j=0; j<n-i-1; j++) {
                    if(arr[j] > arr[j+1])
                        swap(arr[j], arr[j+1]);
                }
            }
        }

        void print(T *, int n) {
            for(int i=0; i<n; i++)
                cout << arr[i] << " ";
            cout << endl;
        }
};
