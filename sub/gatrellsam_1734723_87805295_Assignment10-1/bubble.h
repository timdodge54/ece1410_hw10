#include <iostream>

using namespace std;

#include <iostream>
using namespace std;
template <class T>
class Bubble {
		public:
			Bubble(T * A, int length);
			void print(T * A, int length);
};

template <class T>
Bubble<T>::Bubble(T * A, int length)
{
	for (int i = 1; i <= length - 1; i++)
    {
        for (int j = length; j >= i + 1; j--)
        {
            if (A[j] < A[j - 1])
            {
                swap(A[j], A[j - 1]);
            }
        }
    }
};


template <class T>
void Bubble<T>::print(T * A, int length)
{
	for (int i=0; i<length; i++)
	{
		cout<<A[i]<<"   ";
	}
};