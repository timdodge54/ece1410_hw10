#include <iostream>

using namespace std;

template <class T>
class Insertion{
	public:
		Insertion(T * A, int length);
		void print(T * A, int length);
};
		
template <class T>
Insertion<T>::Insertion (T * A, int length)
{
	int i;
	int j;
	int temp;
	for(i=0; i<length; i++)
	{
		j=i;
		while(j>0 && (A[j-1]>A[j]))
		{
			temp = A[j];
			A[j] = A[j-1];
			A[j-1] = temp;
			j--;
		}
		
	}
	
};

template <class T>
void Insertion<T>::print(T * A, int length)
{
	for(int i=0; i<length; i++)
	{
		cout<<A[i]<<" ";
	}
	
};