#include <iostream>
#define N 50000
using namespace std;

template <class T>
class Selection{
	public:
		Selection(T * A, int length);
		void print(T * A, int length);
		
};

template <class T>
Selection<T>::Selection (T * A, int length)
{int iMin;
for (int j = 0; j < N-1; j++) {
iMin = j;
for (int i = j+1; i < N; i++) {
if (A[i] < A[iMin]) {
iMin = i;
}
}
if (iMin != j) {
swap(A[j], A[iMin]);
}
}
};

template <class T>
void Selection<T>::print(T * A, int length)
{for (int i=0; i<length; i++)
	{
		cout<<A[i]<<" ";
	}
	
};
