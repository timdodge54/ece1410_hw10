#ifndef DEFIN_Q
#define DEFIN_Q

#include <iostream>
#include <string>

using namespace std;

template <class T>
class Quick {
public:

	/*********************************************************************
	* @name Quick
	* @brief sorts array with Quick algorithm
	* @param pointer to first element in array, length of array
	* @retval  none
	*********************************************************************/
	Quick(T* list, int len) {
		quicksort(list, 0, len - 1, len);
	};

	/*********************************************************************
	* @name print
	* @brief prints elements in array
	* @param pointer to first element in array, length of array
	* @retval  none
	*********************************************************************/
	void print(T* list, int len) {
		

		for (int i = 0; i < len; i++) {
			cout << list[i] << " ";
		}
		cout << endl;
	};
private:
	void quicksort(T* A, int lo, int hi, int len) {
		if (lo < hi) {

			int p = partition(A, lo, hi, len);
			quicksort(A, lo, p - 1, len);
			quicksort(A, p + 1, hi, len);
		}
	}
	int partition(T* A, int lo, int hi, int len) {
		T pivot = A[lo];
		int low = lo;
		int high = hi;
		bool a = true;
		while (low != high) {
			if (A[high] <= pivot && a == true) {
				T temp1 = A[low];
				A[low] = A[high];
				A[high] = temp1;
				low++;
				a = false;
			}
			else if (A[high] > pivot && a == true) {
				high--;
			}
			else if (A[low] >= pivot && a == false) {
				T temp1 = A[low];
				A[low] = A[high];
				A[high] = temp1;
				high--;
				a = true;
			}
			else if (A[low] < pivot && a == false) {
				low++;
			}

		}
		return low;
	}
};

#endif
