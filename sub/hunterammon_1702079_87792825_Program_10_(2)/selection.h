#ifndef DEFIN_S
#define DEFIN_S


#include <iostream>
#include <string>

using namespace std;

template <class T>
class Selection {
public:

	/*********************************************************************
	* @name Selection
	* @brief sorts array with selection algorithm
	* @param pointer to first element in array, length of array
	* @retval  none
	*********************************************************************/
	Selection(T* list, int len) {
		for (int j = 0; j < len - 1; j++) {
			int iMin = j;
			for (int i = j + 1; i < len; i++) {
				if (list[i] < list[iMin]) {
					iMin = i;
				}
			}
			if (iMin != j) {
				T temp = list[j];
				list[j] = list[iMin];
				list[iMin] = temp;
			}
		}
	};

	/*********************************************************************
	* @name print
	* @brief prints elements in array
	* @param pointer to first element in array, length of array
	* @retval  none
	*********************************************************************/
	void print(T* list, int len) {
		for (int i = 0; i < len; i++) {
			cout << list[i] << " ";
		}
	};
};

#endif


