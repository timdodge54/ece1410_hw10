#ifndef DEFIN_I
#define DEFIN_I


#include <iostream>
#include <string>

using namespace std;

template <class T>
class Insertion {
public:

	/*********************************************************************
	* @name Insertion
	* @brief sorts array with Insertion algorithm
	* @param pointer to first element in array, length of array
	* @retval  none
	*********************************************************************/
	Insertion(T* list, int len) {
		int i = 1;
		while (i < len){
			int j = i;
		while (j>0 && list[j-1] > list[j]) {
			T temp = list[j];
			list[j] = list[j-1];
			list[j-1] = temp;
			j = j - 1;
		}
		i = i + 1;
		}
	};

	/*********************************************************************
	* @name print
	* @brief prints elements in array
	* @param pointer to first element in array, length of array
	* @retval  none
	*********************************************************************/
	void print(T* list, int len) {
		for (int i = 0; i < len; i++) {
			cout << list[i] << " ";
		}
	};
};

#endif


