#ifndef DEFIN_B
#define DEFIN_B


#include <iostream>
#include <string>

using namespace std;

template <class T>
class Bubble {
public:

	/*********************************************************************
	* @name Bubble
	* @brief sorts array with Bubble algorithm
	* @param pointer to first element in array, length of array
	* @retval  none
	*********************************************************************/
	Bubble(T* list, int len) {
		bool good = false;
		while (good == false) {
			good = true;
			for (int i = 0; i < len-1; i++) {
				if (list[i] > list[i + 1]) {
					T temp = list[i];
					list[i] = list[i + 1];
					list[i + 1] = temp;
					good = false;
				}
			}
		}
		
	};

	/*********************************************************************
	* @name print
	* @brief prints elements in array
	* @param pointer to first element in array, length of array
	* @retval  none
	*********************************************************************/
	void print(T* l, int ln) {
		for (int i = 0; i < ln; i++) {
			cout << l[i] << " ";
		}
	};
private:
	T* list;
	int len;
	
};

#endif


