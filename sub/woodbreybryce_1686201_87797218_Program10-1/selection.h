#include <iostream>

using namespace std;

template <class T>
class Selection
{
	public:
		Selection(T *head, int n);
		void print(T *head, int n);
};

/*********************************************************************
* @name Selection
* @brief Constructor which sorts through the data
* @param an array and integer of the number of elements in the array
* @retval none
*********************************************************************/

template <class T>
Selection<T>::Selection(T *head, int n)
{
	int i, j, iMin;
	
	for (j = 0; j < n-1; j++) 
	{
		//sets iMin
		iMin = j;
		for (i = j+1; i < n; i++) 
		{
			//change iMin
			if (head[i] < head[iMin]) 
			{
            iMin = i;
			}
        }
		
		//swap if iMin != j
		if (iMin != j) 
		{
			T temp;
		
			temp = head[j];
			head[j] = head[iMin];
			head[iMin] = temp;
		}
	}
}

/*********************************************************************
* @name print
* @brief prints data from the array
* @param array of type T and integer of the size of the array
* @retval none
*********************************************************************/

template <class T>
void Selection<T>::print(T *head, int n)
{
	int i;
	//print the array
	for (i = 0; i < n; i++)
	{
		cout << head[i] << " ";
	}
	cout << endl;
}