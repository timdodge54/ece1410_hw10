#include <iostream>

using namespace std;

template <class T>
class Insertion
{
	public:
		Insertion(T *head, int n);
		void print(T *head, int n);
};

/*********************************************************************
* @name Insertion
* @brief Constructor which sorts through the data
* @param an array and integer of the number of elements in the array
* @retval none
*********************************************************************/

template <class T>
Insertion<T>::Insertion(T *head, int n)
{
	int i, j;
	T temp;
	
	i = 1;
	//loop through array
	while (i < n)
	{
		j = i;
		while (j > 0 && head[j-1] > head[j])
		{
			//swap
			temp = head[j];
			head[j] = head[j-1];
			head[j-1] = temp;
			//decrement j
			j--;
		}
		//increment i
		i++;
	}
}

/*********************************************************************
* @name print
* @brief prints data from the array
* @param array of type T and integer of the size of the array
* @retval none
*********************************************************************/

template <class T>
void Insertion<T>::print(T *head, int n)
{
	int i;
	//print array
	for (i = 0; i < n; i++)
	{
		cout << head[i] << " ";
	}
	cout << endl;
}