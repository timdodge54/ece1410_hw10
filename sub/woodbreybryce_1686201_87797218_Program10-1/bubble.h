#include <iostream>

using namespace std;

template <class T>
class Bubble
{
	private:
		
	public:
		Bubble(T *head, int n);
		void print(T *head, int n);
};

/*********************************************************************
* @name Bubble
* @brief Constructor which sorts through the data
* @param an array and integer of the number of elements in the array
* @retval none
*********************************************************************/

template <class T>
Bubble<T>::Bubble(T *head, int n)
{
	int swapped, i;
	T temp;
	
	do
	{
		//set swap to 0
		swapped = 0;
		//loop through array
		for (i = 1; i <= n-1; i++)
		{
			if (head[i-1] > head[i])
			{
				//swap
				temp = head[i];
				head[i] = head[i-1];
				head[i-1] = temp;
				//set swap to 1
				swapped = 1;
			}
		}
	} while (swapped == 1);
}

/*********************************************************************
* @name print
* @brief prints data from the array
* @param array of type T and integer of the size of the array
* @retval none
*********************************************************************/

template <class T>
void Bubble<T>::print(T *head, int n)
{
	int i;
	//loop around and print array
	for (i = 0; i < n; i++)
	{
		cout << head[i] << " ";
	}
	cout << endl;
}