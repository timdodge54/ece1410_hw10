#include <iostream>

using namespace std;

template <class T>
class Quick
{
	public:
		Quick(T *head, int n);
		void print(T *head, int n);
		void quicksort(T *head, int lo, int hi);
		int partition(T *head, int lo, int hi);
};

/*********************************************************************
* @name Quick
* @brief Constructor which begins the process to sort through the data
* @param an array and integer of the number of elements in the array
* @retval none
*********************************************************************/

template <class T>
Quick<T>::Quick(T *head, int n)
{
	//sort through starting with a call of the quicksort func
	quicksort(head, 0, n-1);
}

/*********************************************************************
* @name print
* @brief Prints the array
* @param array and integer of the size of the array
* @retval none
*********************************************************************/

template <class T>
void Quick<T>::print(T *head, int n)
{
	int i;
	//print the array
	for (i = 0; i < n; i++)
	{
		cout << head[i] << " ";
	}
	cout << endl;
}

/*********************************************************************
* @name quicksort
* @brief recursively calls itself to access sub arrays to sort data
* @param an array and integer of the number of elements in the array
* @retval none
*********************************************************************/

template <class T>
void Quick<T>::quicksort(T *head, int lo, int hi)
{
	if (lo < hi)
	{
		//set p
		int p = partition(head, lo, hi);
		//recursively call quicksort with different sections of the array
		quicksort(head, lo, p-1);
		quicksort(head, p+1, hi);
	}
}

/*********************************************************************
* @name partition
* @brief swap data around a pivot point with small on left and big on right
* @param array of type T and integer of the size of the array
* @retval none
*********************************************************************/

template <class T>
int Quick<T>::partition(T *head, int lo, int hi)
{
	T pivot = head[hi];
	int i = lo - 1;
	
	//go through sub array
	for (int j = lo; j < hi; j++)
	{
		if (head[j] < pivot)
		{
			//increment i
			i++;
			//swap
			T temp = head[i];
			head[i] = head[j];
			head[j] = temp;
		}
	}
	
	//swap
	T temp = head[i + 1];
	head[i+1] = head[hi];
	head[hi] = temp;
	
	return i + 1;
}