#ifndef INSERTION_H
#define INSERTION_H

#include <iostream>

using namespace std;

template <class T>
class Insertion
{
public:
	Insertion(T* nums, int N);
	void print(T* nums, int N);
};

/*********************************************************************
* @name Insertion
* @brief Constructor for the Insertion class performing the 
*		 insertion sort algorithm
* @param Values for T* nums, and int N
* @retval none
*********************************************************************/
template <class T>
Insertion<T>::Insertion(T* nums, int N)
{
	int i, j;
	
	i = 1;
	while (i < N)
	{
		j = i;
		//swapping values in array
		while (j > 0 && nums[j - 1] > nums[j])
		{
			swap (nums[j], nums[j - 1]);
			j = j - 1;
		}
		
		i = i + 1;
	}
}

/*********************************************************************
* @name print
* @brief Prints number values to screen
* @param Values for T* nums, and int N
* @retval none
*********************************************************************/
template <class T>
void Insertion<T>::print(T* nums, int N)
{
	int i;

	//loop to print values to screen
	for (i = 0; i < N; i++)
	{
		cout << " " << nums[i];
	}
}	

#endif