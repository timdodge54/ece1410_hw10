#ifndef SELECTION_H
#define SELECTION_H

#include <iostream>

using namespace std;

template <class T>
class Selection
{
public:
	Selection(T* nums, int N);
	void print(T* nums, int N);
};

/*********************************************************************
* @name Selection
* @brief Constructor for the Selection class performing the 
*		 selection sort algorithm
* @param Values for T* nums, and int N
* @retval none
*********************************************************************/
template <class T>
Selection<T>::Selection(T* nums, int N)
{
	int i, j;
	
	for (i = 0; i < N-1; i++)
	{
		int jMin = i;
		for (j = i+1; j < N; j++)
		{
			if(nums[j] < nums[jMin])
			{
				jMin = j;
			}
		}
		
		//swapping values in array
		if (jMin != i)
		{
			swap(nums[i], nums[jMin]);
		}
	}
}

/*********************************************************************
* @name print
* @brief Prints number values to screen
* @param Values for T* nums, and int N
* @retval none
*********************************************************************/
template <class T>
void Selection<T>::print(T* nums, int N)
{
	int i;

	//loop to print values to screen
	for (i = 0; i < N; i++)
	{
		cout << " " << nums[i];
	}
}
#endif