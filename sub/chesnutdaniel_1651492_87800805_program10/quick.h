#ifndef QUICK_H
#define QUICK_H

#include <iostream>

using namespace std;

template <class T>
class Quick
{
public:
	Quick(T* nums, int N);
	void quicksort(T* nums, int lo, int hi);
	int partition(T* nums, int lo, int hi);
	void print(T* nums, int N);
};

/*********************************************************************
* @name Quick
* @brief Constructor for the Quick class calling the quicksort 
*		 function to perform the quick sort algorithm
* @param Values for T* nums, and int N
* @retval none
*********************************************************************/
template <class T>
Quick<T>::Quick(T* nums, int N)
{
	//calling quicksort algorithm, setting lo and hi
	quicksort(nums, 0, N-1);
}

/*********************************************************************
* @name quicksort
* @brief function partitions, then utilizies recursion of the
*		 quick sort algorithm
* @param Values for T* nums, and int N
* @retval none
*********************************************************************/
template <class T>
void Quick<T>::quicksort(T* nums, int lo, int hi)
{
	int p;
	
	//run until lo is equal to hi
	if(lo < hi)
	{
		//call partition, set return value to p
		p = partition(nums, lo, hi);
		//recursively call quicksort function
		quicksort(nums, lo, p - 1);
		quicksort(nums, p + 1, hi);
	}
}

/*********************************************************************
* @name partition
* @brief function to perform the overall quick sort algorithm
* @param Values for T* nums, and int N
* @retval int value
*********************************************************************/
template <class T>
int Quick<T>::partition(T* nums, int lo, int hi)
{
	int i, j;
	T pivot;
	
	//setting pivot
	pivot = nums[hi];
	
	i = lo - 1;
	
	//loop until j is equal to hi
	for (j = lo; j < hi; j++)
	{
		//if statment around pivot to swap values
		if(nums[j] < pivot)
		{
			i = i + 1;
			swap(nums[i], nums[j]);
		}
	}
	//final swap
	swap(nums[i + 1], nums[hi]);
	
	return i + 1;
}

/*********************************************************************
* @name print
* @brief Prints number values to screen
* @param Values for T* nums, and int N
* @retval none
*********************************************************************/
template <class T>
void Quick<T>::print(T* nums, int N)
{
	int i;

	//loop to print values to screen
	for (i = 0; i < N; i++)
	{
		cout << " " << nums[i];
	}	
}	
#endif