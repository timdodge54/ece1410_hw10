#ifndef BUBBLE_H
#define BUBBLE_H

#include <iostream>

using namespace std;

template <class T>
class Bubble
{
public:
	Bubble(T* nums, int N);
	void print(T* nums, int N);
};

/*********************************************************************
* @name Bubble
* @brief Constructor for the Bubble class performing the 
*		 bubble sort algorithm
* @param Values for T* nums, and int N
* @retval none
*********************************************************************/
template <class T>
Bubble<T>::Bubble(T* nums, int N)
{
	int i, j;
	T temp;

	//bubble sort algorithm
	for (i = 1; i <= N; i++)
	{
		for (j = 0; j < N - i; j++)
		{
			//swapping values
			if (nums[j] > nums[j + 1])
			{
				temp = nums[j];
				nums[j] = nums[j + 1];
				nums[j + 1] = temp;
			}
		}
	}
}

/*********************************************************************
* @name print
* @brief Prints number values to screen
* @param Values for T* nums, and int N
* @retval none
*********************************************************************/
template <class T>
void Bubble<T>::print(T* nums, int N)
{
	int i;

	//loop to print values to screen
	for (i = 0; i < N; i++)
	{
		cout << " " << nums[i];
	}
}

#endif