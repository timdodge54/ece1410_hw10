#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>
template<typename T>
class Bubble {
public:
    Bubble(T* arr, int size) {
        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    T temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        print(arr, size);
    }
    void print(T* arr, int size) {
        for (int i = 0; i < size; i++) {
            std::cout << arr[i] << " ";
        }
        std::cout << std::endl;
    }
};
#endif BUBBLE_H