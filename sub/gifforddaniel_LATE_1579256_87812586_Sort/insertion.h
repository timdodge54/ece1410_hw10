#ifndef INSERTION_H
#define INSERTION_H
#include <iostream>
template<typename T>
class Insertion {
public:
    Insertion(T* arr, int size) {
        for (int i = 1; i < size; i++) {
            T key = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = key;
        }
        print(arr, size);
    }
    void print(T* arr, int size) {
        for (int i = 0; i < size; i++) {
            std::cout << arr[i] << " ";
        }
        std::cout << std::endl;
    }
};
#endif INSERTION_H