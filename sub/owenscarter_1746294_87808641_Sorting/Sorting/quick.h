#pragma once
#include <algorithm>
#include <iostream>

using namespace std;

template <class T>class Quick {
private:
/*************
@name partiton
@brief breaks the selection of the array in half
@param head of array, begining, end
@retval partiton location
**************/
	template <class T> int partition(T* a,int start, int end) {
//place pivot at the begining
        T pivot = a[start];
        T count = 0;
//find the size of the selection
        for (int i = start + 1; i <= end; i++) {
            if (a[i] <= pivot)
                count++;
        }
//swap the begining and the end values
        int pivotIndex = start + count;
        swap(a[pivotIndex], a[start]);
//move inward and swap where needed
        int i = start, j = end;

        while (i < pivotIndex && j > pivotIndex) {
            while (a[i] <= pivot) {
                i++;
            }
            while (a[j] > pivot) {
                j--;
            }
            if (i < pivotIndex && j > pivotIndex) {
                swap(a[i++], a[j--]);
            }
        }
        return pivotIndex;
	}
public:
/*************
@name quicksort
@brief recursively sorts the array
@param head of array, start and end of selection
@retval none
**************/
    template <class T> void Quicksort(T* a, int start, int end) {

        if (start >= end) {
           return;
        }
//split array in half
        int p = partition(a, start, end);
//sort top half
        Quicksort(a, start, p - 1);
//sort bottom half
        Quicksort(a, p + 1, end);
    }

/*************
@name contructor
@brief sorts an array
@param head of array, size of array
@retval none
**************/
    template <class T> Quick(T* a, int n) {
        int start = 0;
        int end = n - 1;
        Quicksort(a, start, end);
    }
/*************
@name print
@brief prints the sorted array
@param head of array, size of array
@retval none
**************/
    template <class T> void print(T* a, int n) {
        for (int i = 0; i < n - 1; i++) {
            cout << a[i] << "\n";
        }
    }
};
