#pragma once
#include <algorithm>
#include <iostream>

using namespace std;

template <class T>class Selection {
private:
	long swaps;
public:
	template <class T> Selection(T* a, int n) {
		swaps = 0;
		for (int j = 0; j < n - 1; j++) {
			int imin = j;
			for (int i = j + 1; i < n; i++) {
				if (a[i] < a[imin]) {
					imin = i;
				}
			}
			if (imin != j) {
				swap(a[j], a[imin]);
				swaps++;
			}
		}
	}
	template <class T> void print(T* a, int n) {
		for (int i = 0; i < n - 1; i++) {
			cout << a[i] << "\n";
		}
		cout << swaps << " operations performed";
	}
};