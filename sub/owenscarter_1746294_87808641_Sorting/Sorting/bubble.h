#pragma once
#include <algorithm>
#include <iostream>

using namespace std;

template <class T>class Bubble {
private:
    long swaps;

public:
    template <class T> Bubble(T *a, int n)
    {
        swaps = 0;
        for (int i = 0; i < n - 1; i++)
            for (int j = n - 1; i < j; j--)
                if (a[j] < a[j - 1]) {
                    swap(a[j], a[j - 1]);
                    swaps++;
                }
    }
    template <class T> void print(T* a, int n) {
        for (int i = 0; i < n - 1; i++) {
            cout << a[i] << "\n";
       }
        cout << swaps << " operations performed";
    }
};