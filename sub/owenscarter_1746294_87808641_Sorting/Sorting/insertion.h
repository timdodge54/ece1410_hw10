#pragma once
#include <algorithm>
#include <iostream>

using namespace std;
template <class T> class Insertion {
private:
	long swaps;

public:
	template <class T> Insertion(T* a, int n) {
		swaps = 0;
		for (int i = 0; i < n - 1; i++) {
			int j = i;
			while ((j > 0) && (a[j - 1] > a[j])) {
				swap(a[j], a[j - 1]);
				swaps++;
				j = j - 1;
			}
		}
	}
	template <class T> void print(T* a, int n) {
		for (int i = 0; i < n - 1; i++) {
			cout << a[i] << "\n";
		}
		cout << swaps << " operations performed";
	}
};
