#ifndef SELECTION
#define SELECTION

#include <iostream>
using namespace std;

template <class T>
class Selection {
public:
    Selection(T* nums, int N);
    void print(T* nums, int N);
};



/***********************************************
* @name		Selection
* @brief    constructor for class Selection
* @param	pointer to array of element T, int length of array
* @retval	none
***********************************************/

template<class T>
inline Selection<T>::Selection(T* nums, int N)
{
    //declare variables
    T temp;

    //loop through array
    for (int j = 0; j < N - 1; j++) {

        //track iMin
        int iMin = j;

        //compare 2 items in array
        for (int i = j + 1; i < N; i++) {
            if (nums[i] < nums[iMin]) {
                iMin = i;
            }
        }

        //swap numbers
        if (iMin != j) {
            temp = nums[j];
            nums[j] = nums[iMin];
            nums[iMin] = temp;
        }
    }
}



/***********************************************
* @name		print
* @brief    print sorted array
* @param	pointer to array of element T, int length of array
* @retval	none
***********************************************/

template<class T>
inline void Selection<T>::print(T* nums, int N)
{
    //loop N times
    for (int i = 0; i < N; i++) {
        //print each item in array
        cout << nums[i] << " ";
    }
}

#endif