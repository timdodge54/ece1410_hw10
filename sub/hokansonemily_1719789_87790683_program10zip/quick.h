#ifndef QUICK
#define QUICK

#include <iostream>
using namespace std;

template <class T>
class Quick {
public:
    Quick(T* nums, int N);
    void quicksort(T* nums, int lo, int hi);
    int partition(T* nums, int lo, int hi);
    void print(T* nums, int N);
};



/***********************************************
* @name		Quick
* @brief    constructor for class Quick
* @param	pointer to array of element T, int length of array
* @retval	none
***********************************************/

template<class T>
inline Quick<T>::Quick(T* nums, int N)
{
    //declare variables
    int lo = 0;
    int hi = N - 1;

    //call quicksort function
    quicksort(nums, lo, hi);
}



/***********************************************
* @name		quicksort
* @brief    quick sort array
* @param	pointer to array of element T, int lo, int hi
* @retval	none
***********************************************/

template<class T>
inline void Quick<T>::quicksort(T* nums, int lo, int hi)
{
    //declare variables
    int p;
    
    //loop
    if (lo < hi) {
        p = partition(nums, lo, hi);
        quicksort(nums, lo, p - 1);
        quicksort(nums, p + 1, hi);
    }
}



/***********************************************
* @name		partition
* @brief    partion array for quick sort
* @param	pointer to array of element T, int lo, int hi
* @retval	none
***********************************************/

template<class T>
inline int Quick<T>::partition(T* nums, int lo, int hi)
{
    //declare variables
    T temp;
    T pivot = nums[hi];
    int i = lo - 1;

    //loop
    for (int j = lo; j <= hi - 1; j++) {
        if (nums[j] < pivot) {

            //increment i
            i++;

            //swap A[i] and A[j]
            temp = nums[i];
            nums[i] = nums[j];
            nums[j] = temp;
        }
    }

    //swap A[i+1] and A[hi]
    temp = nums[i + 1];
    nums[i + 1] = nums[hi];
    nums[hi] = temp;

    return i + 1;
}



/***********************************************
* @name		print
* @brief    print sorted array
* @param	pointer to array of element T, int length of array
* @retval	none
***********************************************/

template<class T>
inline void Quick<T>::print(T* nums, int N)
{
    //loop N times
    for (int i = 0; i < N; i++) {
        //print each item in array
        cout << nums[i] << " ";
    }
}

#endif