#ifndef BUBBLE
#define BUBBLE

#include <iostream>
using namespace std;

template <class T>
class Bubble {
public:
    Bubble(T* nums, int N);
    void print(T* nums, int N);
};



/***********************************************
* @name		Bubble
* @brief    constructor for class Bubble
* @param	pointer to array of element T, int length of array
* @retval	none
***********************************************/

template<class T>
inline Bubble<T>::Bubble(T* nums, int N)
{
    //declare variables
    int swapped;
    T temp;

    //repeat until swapped == false
    do {
        swapped = 0;

        //loop through array
        for (int i = 1; i <= N - 1; i++) {

            //swap numbers to correct order
            if (nums[i - 1] > nums[i]) {
                temp = nums[i - 1];
                nums[i - 1] = nums[i];
                nums[i] = temp;

                swapped = 1;
            }
        }
    } while (swapped);
}



/***********************************************
* @name		print
* @brief    print sorted array
* @param	pointer to array of element T, int length of array
* @retval	none
***********************************************/

template<class T>
inline void Bubble<T>::print(T* nums, int N)
{
    //loop N times
    for (int i = 0; i < N; i++) {
        //print each item in array
        cout << nums[i] << " ";
    }
}

#endif