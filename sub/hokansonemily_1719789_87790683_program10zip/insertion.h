#ifndef INSERTION
#define INSERTION

#include <iostream>
using namespace std;

template <class T>
class Insertion {
public:
    Insertion(T* nums, int N);
    void print(T* nums, int N);
};



/***********************************************
* @name		Insertion
* @brief    constructor for class Insertion
* @param	pointer to array of element T, int length of array
* @retval	none
***********************************************/

template<class T>
inline Insertion<T>::Insertion(T* nums, int N)
{
    //declare variables
    int i = 1;
    int j;
    T temp;

    //loop through arrray
    while (i < N) {
        j = i;

        //compare and swap
        while (j > 0 and nums[j - 1] > nums[j]) {
            temp = nums[j];
            nums[j] = nums[j - 1];
            nums[j - 1] = temp;

            //decrement j
            j--;
        }

        //increment i
        i++;
    }
}



/***********************************************
* @name		print
* @brief    print sorted array
* @param	pointer to array of element T, int length of array
* @retval	none
***********************************************/

template<class T>
inline void Insertion<T>::print(T* nums, int N)
{
    //loop N times
    for (int i = 0; i < N; i++) {
        //print each item in array
        cout << nums[i] << " ";
    }
}

#endif