#ifndef QUICK_H
#define QUICK_H
#include <iostream>
using namespace std;

template <class T>
class Quick {
private:
	int partition(T* a, int lo, int hi);
	void quicksort(T* a, int lo, int hi);
public:
	Quick(T* a, int n);
	void print(T* a, int n);
};

/*****************************************************************************
* @name 	Quick
* @brief	constructor that sorts array with quicksort algorithm
* @param	pointer to an array, number of elements in that array
* @retval	none
*****************************************************************************/

template<class T>
inline Quick<T>::Quick(T* a, int n)
{
	//another function is called here that operates recursively
	quicksort(a, 0, n-1);
}

/*****************************************************************************
* @name 	print
* @brief	prints an array to the screen
* @param	pointer to an array, number of elements in that array
* @retval	none
*****************************************************************************/

template<class T>
inline void Quick<T>::print(T* a, int n)
{
	//print each element with spaces in between
	for (int i = 0; i < n; i++) cout << a[i] << " ";
}

/*****************************************************************************
* @name 	partition
* @brief	partitions an array for the quicksort algorithm
* @param	pointer to an array, 2 ints representing endpoints of array to
*			partition
* @retval	index location of partition center
*****************************************************************************/

template<class T>
inline int Quick<T>::partition(T* a, int lo, int hi)
{
	T temp;
	T pivot = a[hi];//start pivot at right
	int i = lo - 1;//i starts at left
	for (int j = lo; j < hi; j++)
	{//loop through list, and if item is less than pivot, swap with i
		if (a[j] < pivot)
		{
			i++;
			temp = a[i];
			a[i] = a[j];
			a[j] = temp;
		}
	}//swap i and hi, return i as partition value
	i++;
	temp = a[i];
	a[i] = a[hi];
	a[hi] = temp;
	return i;
}

/*****************************************************************************
* @name 	quicksort
* @brief	uses recursion and quicksort algorithm to sort an array
* @param	pointer to an array, 2 ints representing endpoints of array to 
			sort
* @retval	none
*****************************************************************************/

template<class T>
inline void Quick<T>::quicksort(T* a, int lo, int hi)
{
	int p;
	if (lo < hi)//stop when we've sorted through whole list
	{
		p = partition(a, lo, hi);//partition, then recursively sort both sides
		quicksort(a, lo, p - 1);
		quicksort(a, p + 1, hi);
	}
}
#endif
