#ifndef INSERTION_H
#define INSERTION_H
#include <iostream>
using namespace std;

template <class T>
class Insertion {
private:
public:
	Insertion(T* a, int n);
	void print(T* a, int n);
};

/*****************************************************************************
* @name 	Insertion
* @brief	sorts array with insertion sort algorithm
* @param	pointer to an array, number of elements in that array
* @retval	none
*****************************************************************************/

template<class T>
inline Insertion<T>::Insertion(T* a, int n)
{
	T temp;
	for (int i = 0; i < n; i++)//i tracks how far we've sorted the list
	{//for each position in the list, loop through the rest of the list 
	 //swapping terms where needed
		for (int j = i; j > 0 && a[j] < a[j - 1]; j--)
		{
			temp = a[j];
			a[j] = a[j - 1];
			a[j - 1] = temp;
		}
	}
}

/*****************************************************************************
* @name 	print
* @brief	prints an array to the screen
* @param	pointer to an array, number of elements in that array
* @retval	none
*****************************************************************************/

template<class T>
inline void Insertion<T>::print(T* a, int n)
{
	//print each element with spaces in between
	for (int i = 0; i < n; i++) cout << a[i] << " ";
}
#endif
