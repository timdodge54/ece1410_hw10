#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>
using namespace std;

template <class T>
class Selection {
private:
public:
	Selection(T* a, int n);
	void print(T* a, int n);
};

/*****************************************************************************
* @name 	Selection
* @brief	sorts array with selection sort algorithm
* @param	pointer to an array, number of elements in that array
* @retval	none
*****************************************************************************/

template<class T>
inline Selection<T>::Selection(T* a, int n)
{
	T temp;
	int iMin;
	//starting at the beginning of the array, find the smallest unsorted 
	//element and put it in the next unsorted position
	for (int j = 0; j < n; j++)//j tracks how far we've sorted
	{
		iMin = j;
		for (int i = j + 1; i < n; i++)
		{//look through rest of list to find smallest remaining element
			if (a[i] < a[iMin]) iMin = i;
		}
		if (iMin != j)
		{//swap smallest element with current sorting position
			temp = a[j];
			a[j] = a[iMin];
			a[iMin] = temp;
		}
	}
}

/*****************************************************************************
* @name 	print
* @brief	prints an array to the screen
* @param	pointer to an array, number of elements in that array
* @retval	none
*****************************************************************************/

template<class T>
inline void Selection<T>::print(T* a, int n)
{
	//print each element with spaces in between
	for (int i = 0; i < n; i++) cout << a[i] << " ";
}
#endif
