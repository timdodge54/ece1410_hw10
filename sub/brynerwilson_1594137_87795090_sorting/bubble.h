#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>
using namespace std;

template <class T>
class Bubble {
public:
	Bubble(T* a, int n);
	void print(T* a, int n);
};

/*****************************************************************************
* @name 	Bubble
* @brief	sorts array with bubble sort algorithm	
* @param	pointer to an array, number of elements in that array
* @retval	none
*****************************************************************************/

template<class T>
inline Bubble<T>::Bubble(T* a, int n)
{
	int swapped;
	T temp;
	do {
		swapped = 0;
		for (int i = 0; i < n - 1; i++)//step through list
		{
			if (a[i] > a[i + 1])//compare adjacent elements
			{
				//swap if needed
				temp = a[i];
				a[i] = a[i + 1];
				a[i + 1] = temp;
				swapped = 1;
			}
		}
	} while (swapped == 1);//repeat until full pass through list without swaps
}

/*****************************************************************************
* @name 	print
* @brief	prints an array to the screen
* @param	pointer to an array, number of elements in that array
* @retval	none
*****************************************************************************/

template<class T>
inline void Bubble<T>::print(T* a, int n)
{
	//print each element with spaces in between
	for (int i = 0; i < n; i++) cout << a[i] << " ";
}
#endif