#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>
template <class T>
class Selection
{
	public:
		Selection<T>(T* arr,int j);
		void print(T* arr,int j);
};

/********************************************************************* 
* @name Selection::Selection
* @brief  constructor for Selection that sorts via selection sorting
* @param array, and int
* @retval  none 
*********************************************************************/
template <class T>
Selection<T>::Selection(T* arr, int j)
{
	//getting temp placeholder
	T temp;
	//for the whole array
	for (int i = 0; i < j-1; i++)
	{
		int mIndex = i;
		//go through the array comparing index and current
        for (int k = i+1; k < j; k++) 
		{
			if (arr[k] < arr[mIndex]) 
			{
				mIndex = k;
            }
        }
		//if minimum index is not i, swap
        if (mIndex != i) 
		{
			//swap the selections
			temp = arr[mIndex];
			arr[mIndex] = arr[i];
			arr[i] = temp;
        }
    }
}

/********************************************************************* 
* @name Selection::print
* @brief  prints the array
* @param array, and int
* @retval  none 
*********************************************************************/
template <class T>
void Selection<T>::print(T* arr,int j)
{
	//setting variables
	int i;
	//printing sorted array
	for(i = 1; i < j-1; i++)
	{
		std::cout << arr[i] << " ";
	}
}
#endif