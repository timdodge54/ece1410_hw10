#ifndef QUICK_H
#define QUICK_H
#include <iostream>
template <class T>
class Quick
{
	public:
		Quick<T>(T* arr,int j);
		void print(T* arr,int j);
		void quicksort(T* arr, int l, int r);
		int partition(T* arr, int l, int r);
		void swap(T& a, T& b) 
		{
			//just to easily swap two elements
			T temp = a;
			a = b;
			b = temp;
		}
};

/********************************************************************* 
* @name Quick::Quick
* @brief  constructor for Quick that sorts via Quick sorting
* @param array, and int
* @retval  none 
*********************************************************************/
template <class T>
Quick<T>::Quick(T* arr, int j)
{
	quicksort(arr, 0, j - 1);
}
/********************************************************************* 
* @name Quick::quicksort
* @brief  recursively sorts the array via quick sorting
* @param array, and int
* @retval  none 
*********************************************************************/
template <class T>
void Quick<T>::quicksort(T* arr, int l, int r)
{
    if (l < r)
	{
        //partition the subarray
        int pIndex = partition(arr, l, r);
        
        //recursively sort the l and r subarrays
        quicksort(arr, l, pIndex - 1);
        quicksort(arr, pIndex + 1, r);
    }
}

/********************************************************************* 
* @name Quick::partition
* @brief  gets the partition so that you can use divide/conquer algo
* @param array, and int
* @retval  none 
*********************************************************************/
template <class T>
int Quick<T>::partition(T* arr, int l, int r)
{
    T pVal = arr[r];  // Choose the pivot value (last element in subarray)
    // initialize the store index to the leftmost element
	int getIndex = l;     
    
    // Iterate through the subarray from the l to the second-to-last element
    for (int i = l; i < r; i++)
	{
        if (arr[i] < pVal)
		{
            //if the current element is less than the pivot value
			//swap it with the element at the stored index
            swap(arr[i], arr[getIndex]);
            //increment the store index
			getIndex++;  
        }
    }
    
    //Swap the pivot value with the element at the stored index
    swap(arr[getIndex], arr[r]);
	//return the last index of the pivot value
    return getIndex;  
}

/********************************************************************* 
* @name Quick::print
* @brief  prints the array
* @param array, and int
* @retval  none 
*********************************************************************/
template <class T>
void Quick<T>::print(T* arr,int j)
{
	//setting variables
	int i;
	//printing sorted array
	for(i = 1; i < j-1; i++)
	{
		std::cout << arr[i] << " ";
	}
}
#endif