#ifndef BUBBLE_H
#define BUBBLE_H
#include <iostream>
template <class T>
class Bubble
{
	public:
		Bubble<T>(T* arr,int j);
		void print(T* arr,int j);
};

/********************************************************************* 
* @name Bubble::Bubble
* @brief  constructor for Bubble that sorts via bubble sorting
* @param array, and int
* @retval  none 
*********************************************************************/
template <class T>
Bubble<T>::Bubble(T* arr, int j)
{
	//setting variables
	int i, flag = 1;
	T temp;
	//while swapped
	while(flag)
	{
		flag = 0;
		//for loop going through the array
		for(i = 1; i < j-1; i++)
		{
			//comparing elements in adjacent arrays
			if(arr[i-1] > arr[i])
			{
				//swapping the elements
				temp = arr[i-1];
				arr[i-1] = arr[i];
				arr[i] = temp;
				//swapped = true
				flag = 1;
			}
		}
		j-=1;
	}
	
}

/********************************************************************* 
* @name Bubble::print
* @brief  prints the array
* @param array, and int
* @retval  none 
*********************************************************************/
template <class T>
void Bubble<T>::print(T* arr,int j)
{
	//setting variables
	int i;
	//printing sorted array
	for(i = 1; i < j-1; i++)
	{
		std::cout << arr[i] << " ";
	}
}
#endif