#ifndef INSERTION_H
#define INSERTION_H
#include <iostream>
template <class T>
class Insertion
{
	public:
		Insertion<T>(T* arr,int j);
		void print(T* arr,int j);
};

/********************************************************************* 
* @name Insertion::Insertion
* @brief  constructor for Insertion that sorts via Insertion sorting
* @param array, and int
* @retval  none 
*********************************************************************/
template <class T>
Insertion<T>::Insertion(T* arr, int j)
{
	for (int i = 1; i < j; i++) 
	{
		//creating temp placeholder
		T temp = arr[i];
		int k = i - 1;
		//while k is strictly greater than 0
		//and element k is less than temp
		while (k >= 0 && arr[k] > temp) 
		{
			//shift the elements
			arr[k + 1] = arr[k];
			k--;
		}
		//complete the cycle with k+1 = temp
        arr[k + 1] = temp;
    }
}

/********************************************************************* 
* @name Insertion::print
* @brief  prints the array
* @param array, and int
* @retval  none 
*********************************************************************/
template <class T>
void Insertion<T>::print(T* arr,int j)
{
	//setting variables
	int i;
	//printing sorted array
	for(i = 1; i < j-1; i++)
	{
		std::cout << arr[i] << " ";
	}
}
#endif