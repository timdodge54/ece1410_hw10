#ifndef BUBBLE
#define BUBBLE

#include <iostream>
using namespace std;

template <class T>
class Bubble {

public: 
	Bubble(T* h, int s);
	void print(T* h, int s);
};

template <class T>
Bubble<T>::Bubble(T* h, int s) {
	T* curr = h;
	T* next = h;
	next++;
	T temp;

	for (int i = s; i > 0; i--) {
		for (int j = 1; j < i; j++) {
			if (*curr > *next) {
				temp = *curr;
				*curr = *next;
				*next = temp;
			}
			curr++;
			next++;
		}
		curr = h;
		next = h;
		next++;
	}
}

template <class T>
void Bubble<T>::print(T* h, int s) {
	cout << "Sorted Array:" << endl;

	for (int i = 0; i < s; i++, h++) {
		cout << *h << ", ";
	}

	cout << endl;
}

#endif