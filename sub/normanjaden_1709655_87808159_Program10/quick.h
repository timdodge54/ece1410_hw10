#ifndef QUICK
#define QUICK

#include <iostream>
using namespace std;

template <class T>
class Quick {

public:
	Quick(T* h, int s);
	void quickSort(T* h, int start, int end);
	void print(T* h, int s);
	int partition(T* h, int start, int end);
};

template <class T>
Quick<T>::Quick(T* h, int s) {
	quickSort(h, 0, s - 1);

}

template <class T>
void Quick<T>::quickSort(T* h, int start, int end) {
	//start –> Starting index,  end --> Ending index
	if (start < end) {
		int pIndex = partition(h, start, end);
		quickSort(h, start, pIndex - 1);
		quickSort(h, pIndex + 1, end);
	}
}

template <class T>
void Quick<T>::print(T* h, int s) {
	cout << "Sorted Array: " << endl;

	for (int i = 0; i < s; i++) {
		cout << *h << ", ";
		h++;
	}

	cout << endl;
}

template <class T>
int Quick<T>::partition(T* h, int start, int end) {
	
	// Setting rightmost Index as pivot
	T* pivot = h + end;

	int i = (start - 1);  // Index of smaller element and indicates the 
						  // right position of pivot found so far
	for (int j = start; j <= end - 1; j++) {

		T* curr = h + j;
		if (*curr < *pivot) {
			i++;    // increment index of smaller element
			T* temp1 = h + i;
			T* temp2 = h + j;
			
			T temp = *temp1;
			*temp1 = *temp2;
			*temp2 = temp;
		}
	}
		
	T* temp1 = h + i + 1;
	T* temp2 = h + end;

	T temp = *temp1;
	*temp1 = *temp2;
	*temp2 = temp;

	return (i + 1);
}

#endif