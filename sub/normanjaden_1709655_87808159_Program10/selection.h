#ifndef SELECTION
#define SELECTION

#include <iostream>
using namespace std;

template <class T>
class Selection {

public:
	Selection(T* h, int s);
	void print(T* h, int s);
};

template <class T>
Selection<T>::Selection(T* h, int s) {
	T* min = h;
	T* minSpot = h;
	T* curr = h;

	for (int i = 0; i < s; i++) {
		for (int j = i; j < s; j++) {
			if (*min > *curr) {
				min = curr;
			}
			curr++;
		}
		curr = minSpot;

		T temp = *curr;
		*curr = *min;
		*min = temp;
		minSpot++;
		curr++;
		min = curr;;

	}
}

template <class T>
void Selection<T>::print(T* h, int s) {
	cout << "Sorted Array:" << endl;

	for (int i = 0; i < s; i++) {
		cout << *h << ", ";
		h++;
	}

	cout << endl;
}

#endif