#ifndef INSERTION
#define INSERTION

#include <iostream>
using namespace std;

template <class T>
class Insertion {

public:
	Insertion(T* h, int s);
	void print(T* h, int s);
};

template <class T>
Insertion<T>::Insertion(T* h, int s) {
	T* curr = h;
	T* currSpot;
	T* next = h;
	next++;
	T temp;

	for (int i = 0; i < s - 1; i++) {
		currSpot = curr;
		if (*curr > *next) {

			temp = *curr; 
			*curr = *next;
			*next = temp;

			currSpot = curr + 1;


			for (int j = 0; j < i; j++) {
				curr--;
				next--;
				if (*curr > *next) {
					temp = *curr;
					*curr = *next;
					*next = temp;
				}
				else {
					break;
				}
			}

			curr = currSpot;
			next = currSpot + 1;
		}
		else {
			curr++;
			next++;
		}
	}
}

template <class T>
void Insertion<T>::print(T* h, int s) {
	cout << "Sorted Array: " << endl;

	for (int i = 0; i < s; i++) {
		cout << *h << ", ";
		h++;
	}

	cout << endl;
}

#endif