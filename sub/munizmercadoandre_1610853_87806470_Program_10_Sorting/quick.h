/************************************************************
Andre Muniz
A02290559
4/13/2023
******************
Function: quick.h

Summary: produce output of number using quick sort method.


Compile: main.cpp used by instructor
******************
pseudocode
Begin
	define quick.h file
	include insertion.h
	include headers
	create class quick, with public variables
	create template for quicksort
	calculate the lo and hi in quick sort
	use int partion function to swap values with each other.
	call quick sort in template class quick
	create tmpalte function print, to print out correct values
End
*************************************************************/
// Begin
//define quick.h file
//include insertion.h
//include headers
#ifndef	QUICK_H
#define QUICK_H
#include "insertion.h"
#include <iostream>
template <typename T>

// 	create class quick, with public variables
class Quick {
public:
	Quick(T* arr, int n);
	void print(T* arr, int n);
	void QuickSort(T* arr, int lo, int hi);
	int Partition(T* arr, int lo, int hi);
};

/************************************************************
@name
@brief
@param
@retval
*************************************************************/
// create template for quick
template <typename T>
void Quick<T>:: QuickSort(T* arr, int lo, int hi)
{
	//	calculate the lo and hi in quick sort
	int p;

	if (lo < hi)
	{
		p = Partition(arr, lo, hi);
		QuickSort(arr, lo, p - 1);
		QuickSort(arr, p + 1, hi);
	}
}

/************************************************************
@name
@brief
@param
@retval
*************************************************************/
// 	use int partion function to swap values with each other.
template<typename T>
int Quick<T>::Partition(T* arr, int lo, int hi)
{
	int i, j;
	int pivot;
	T temp;

	pivot = arr[hi];
	i = lo - 1;

	for (j = lo; j < hi; j++)
	{
		if (arr[j] < pivot)
		{
			i = i + 1;
			temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
		}
		

	}
	temp = arr[i + 1];
	arr[i + 1] = arr[hi];
	arr[hi] = temp;

	return i + 1;
}

/************************************************************
@name
@brief
@param
@retval
*************************************************************/
//call quick sort in template class quick

template <typename T>
Quick<T>::Quick(T* arr, int n)
{
	QuickSort(arr, 0, n - 1);

}

/************************************************************
@name
@brief
@param
@retval
*************************************************************/
// 	create template function print, to print out correct values

template <typename T>
void Quick<T>::print(T* arr, int n) {

	int i;

	for (i = 0; i < n; i++) {
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
}
#endif
//End