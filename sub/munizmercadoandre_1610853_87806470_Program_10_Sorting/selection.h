/************************************************************
Andre Muniz
A02290559
4/13/2023
******************
Function: selection.h

Summary: produce output of number using selection method


Compile: main.cpp used by instructor
******************
pseudocode
Begin
	define selection.h file
	include bubble.h
	include headers
	create class selection, with public variables
	create template for selection
	calculate the array and do swap
	create template function print, to print out correct values
End
*************************************************************/
//Begin
//	define selection.h file
#ifndef SELECTION_H
#define SELECTION_H

//	include bubble.h
//	include headers

#include "bubble.h"
#include <iostream>

template <typename T>

// 	create class selection, with public variables
class Selection {
public:
	Selection(T*, int);
	void print(T*, int);

private:


};


/************************************************************
@name
@brief
@param
@retval
*************************************************************/
//	create template for selection

template <typename T>
Selection<T>::Selection(T* arr, int n)
{
	int i, j;
	int iMin;
	T temp;

	for (j = 0; j < n - 1; j++)
	{
		iMin = j;
		for (i = j + 1; i < n; i++)
		{
			if (arr[i] < arr[iMin]) //	calculate the array and do swap
			{
				iMin = i;
			}
		}
		if (iMin != j)
		{
			temp = arr[j];
			arr[j] = arr[iMin];
			arr[iMin] = temp;
		}
	}

}


/************************************************************
@name
@brief
@param
@retval
*************************************************************/
//	create template function print, to print out correct values
template <typename T>
void Selection<T>::print(T* arr, int n) {
	int i;

	for (i = 0; i < n; i++) {
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
}
#endif
//End