/************************************************************
Andre Muniz
A02290559
4/13/2023
******************
Function: insertion.h

Summary: produce output of number using insertion metho


Compile: main.cpp used by instructor
******************
pseudocode
Begin
	define insertion.h file
	include selection.h
	include headers
	create class insertion, with public variables
	create template for insertion
	calculate the array and do swap
	create template function print, to print out correct values
End
*************************************************************/
//Begin
//define insertion.h file
//include selection.h
//include headers

#ifndef INSERTION_H
#define INSERTION_H
#include "selection.h"
#include <iostream>
template <typename T>

//	create class insertion, with public variables
class Insertion {
public:
	Insertion(T *, int);
	void print(T *, int);

};

/************************************************************
@name
@brief
@param
@retval
*************************************************************/
//	create template for insertion
template <typename T>
Insertion<T>::Insertion(T* arr, int n)
{
	int i, j;
	T temp;
	i = 1;

	//	calculate the array and do swap
	while (i < n) 
	{
		j = i;
		while (j > 0 && arr[j - 1] > arr[j])
		{
			temp = arr[j - 1];
			arr[j - 1] = arr[j];
			arr[j] = temp;
			j = j - 1;
		}
		i = i + 1;
	}
}

/************************************************************
@name
@brief
@param
@retval
*************************************************************/
// create template function print, to print out correct values

template <typename T>
void Insertion<T>::print(T* arr, int n) {

	int i;

	for (i = 0; i < n; i++) {
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
}
#endif
//End