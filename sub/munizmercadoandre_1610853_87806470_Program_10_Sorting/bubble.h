/************************************************************
Andre Muniz
A02290559
4/13/2023
******************
Function: Bubble.h

Summary: 


Compile: main.cpp used by instructor
******************
pseudocode
Begin
	define bubble.h file
	include headers
	create class bubble, with public variables
	create template for bubble
	do calculation to sort the bubble
	swap values
	create template function print, to print out correct values
End
*************************************************************/
//Begin
//	define bubble.h file
#ifndef BUBBLE_H
#define BUBBLE_H

//	include headers
#include "bubble.h"

#include <iostream>


template <typename T>


//	create class bubble, with public variables
class Bubble {
public:
	Bubble(T*, int);
	void print(T*, int);

private:


};


/************************************************************
@name
@brief
@param
@retval
*************************************************************/
//	create template for bubble
//	do calculation to sort the bubble
template <typename T>
Bubble<T>::Bubble(T* arr, int n)
{
	int i, j, swapped;
	T temp;
	swapped = 1;

	//	swap values
	while (swapped == 1) {
		swapped = 0;
		for (i = 1; i < n; i++)
		{
			if (arr[i - 1] > arr[i])
			{
				temp = arr[i-1];
				arr[i-1] = arr[i];
				arr[i] = temp;
				swapped = 1;
			}
		}
	}


}


/************************************************************
@name
@brief
@param
@retval
*************************************************************/
// 	create template function print, to print out correct values
template <typename T>
void Bubble<T>::print(T* arr, int n) {
	int i;

	for (i = 0; i < n; i++) {
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
}
#endif
//End