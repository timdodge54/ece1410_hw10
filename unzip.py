import os
import zipfile

directory = input("Enter the directory path: ")
output = input("Enter the output directory path: ")

for filename in os.listdir(directory):
    if filename.endswith('.zip'):
        file_path = os.path.join(directory, filename)
        print(file_path)
        new_folder = os.path.join(output, os.path.splitext(filename)[0])
        print(new_folder)
        new_folder = new_folder.replace(" ", "_")
        os.mkdir(new_folder)
        with zipfile.ZipFile(file_path, 'r') as zip_ref:
            zip_ref.extractall(new_folder)
